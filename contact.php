<?php 
session_start();
include_once('includes/config.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['submit'])){

  $full_name=$_POST['full_name'];
  $email=$_POST['email'];
  $subject=$_POST['subject'];
  $message=$_POST['message'];
  $details=$_POST['lookingFor'];

  $secretKey = '6LeiFLEZAAAAAPhmVPm907CtOOBxRFwbIh1Cgy9R';
  $token = $_POST['g-token'];
  $ip = $_SERVER['REMOTE_ADDR'];

  $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$token."&remoteip=".$ip;
  $request = file_get_contents($url);
  $response = json_decode($request);

    require 'vendor/autoload.php';
    require 'phpmailer/credential-contact.php';

    $phpmail = new PHPMailer(true);

    // $mail->SMTPDebug = 4;                               // Enable verbose debug output

    $phpmail->isSMTP();                                      // Set mailer to use SMTP
    $phpmail->Host = SMTP_SERVER;  // Specify main and backup SMTP servers
    $phpmail->SMTPAuth = true;                               // Enable SMTP authentication
    $phpmail->Username = EMAIL;                 // SMTP username
    $phpmail->Password = PASS;                           // SMTP password
    $phpmail->SMTPSecure = ENCRYPTION;                            // Enable TLS encryption, `ssl` also accepted
    $phpmail->Port = SMTP_PORT;                                    // TCP port to connect to

    $phpmail->setFrom(EMAIL, 'Find Work Australia');
    $phpmail->addAddress(EMAIL);     // Add a recipient

    $phpmail->addReplyTo(EMAIL);
    $phpmail->isHTML(true);                                  // Set email format to HTML

    $phpmail->Subject = $subject;
    $phpmail->Body    = '<b>Hi Admin,</b>
        <br>
        <h4><b>A Visitor sent you a message. Here is the details.</b></h4>
        <br>
        <p><b>Full Name:</b> ' .$full_name.'</p>
        <p><b>Email Address:</b> ' .$email.'</p>
        <p><b>Details:</b> ' .$details.'</p>
        <p><b>Message:</b> <i>'.$message.'</i>'.$gtoken.'</p>
        <br>
        <h4><b>Please contact the visitor at your convenience!</b></h4>
        <br><br>
        <i><p>Thanks for your time,</p>
        <p>Find Work Australia Team</p></i>

        ';

  if($response->success){

    if(!$phpmail->send()) {
    $_SESSION['error']= 'Message could not be sent. Mailer Error: ' . $phpmail->ErrorInfo;
    } 
    else {
        $_SESSION['success']='Message has been sent. Someone will contact you shortly.';
    }

  }

  else{
    $_SESSION['error']='Captcha Validation Failed.';
  }

}


// if(isset($_POST) && isset($_POST["submit"]))
// {

//   $secretKey = '6LeiFLEZAAAAAPhmVPm907CtOOBxRFwbIh1Cgy9R';
//   $token = $_POST['g-token'];
//   $ip = $_SERVER['REMOTE_ADDR'];

//   $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$token."&remoteip=".$ip;
//   $request = file_get_contents($url);
//   $response = json_decode($request);

//   echo "<br><br><br><br>";
//   echo "<pre>";
//   print_r($response);
//   echo "</pre>";
// }


include_once('includes/header.php'); 

?>

<!DOCTYPE html>
<html>

<head>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css"> -->
<script src="https://www.google.com/recaptcha/api.js?render=6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK"></script>

  

</head>
 
<body>

    <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="img/Jobseeker-top.jpg" height="400" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <p>CONTACT US</p>
            <h2 style="font-family: 'Comfortaa', cursive;">Get In Touch</h2>
            <div>
              <a href="#contact-email" class="btn-get-started scrollto">EMAIL</a>
              <a href="#contact" class="btn-services scrollto">SUBSCRIBE</a>
            </div>
         </div>

      </div>
    </div>
   </div>

 <main id="main"> 
    <section id="about">
      <div class="container" id="jobseeker-step">
        <header class="section-header">
            <h3>Connecting Jobseekers with Employers</h3>
            <br>
            <h5 style="text-align:justify;font-family: 'Comfortaa', cursive;">We’re dedicated to connecting people as quickly and efficiently as possible.
            <br>
            If you need help with something, please let us know and we’ll get back to you as quickly as we can.</h5>

        </header>
        </div>
      </section>

  </main>

  <div class="col-md-12 login_back" id="contact-email">
    <div class="container"> 
      <div class="row">
        <div class="col-md-2">       
            <div class="container">

          </div>
        </div>
        <div class="col-md-8 login regist_head">
          <?php
                    if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Error! </h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Success!</h4>
                            ".$_SESSION['success']."
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
?> 
          <div class="row">
            <div class="col-md-2">
              
            </div>
            <div class="col-md-8">           
              <div class="row">



              <form action="#main" method="post" enctype="multipart/form-data" >
              <div class="col-md-12">
                <h4>Send us a Message</h4>
                <input type="hidden" id="g-token" name="g-token"/>
                <label class="reg_label">Full Name <span class="star">*</span></label>
                <input type="text" id="full_name" name="full_name"  class="reg_box">
 
                <label class="reg_label">Email <span class="star">*</span></label>
                <input type="text" id="email" name="email" class="reg_box">

                <label class="reg_label1">Subject <span class="star">*</span></label>
                <input type="text" id="subject" name="subject"  class="reg_box">

                <label class="reg_label1">What are you looking for? <span class="star">*</span></label>
                <select name="lookingFor" id="lookingFor" class="reg_box">
                  <option value=""></option>
                  <option value="I am a Jobseeker">I am a Jobseeker</option>
                  <option value="I am an Employer">I am an Employer</option>
                  <option value="I am an Agency">I am an Agency</option>
                </select>

                <label class="reg_label1">Message <span class="star">*</span></label>
             
                <textarea class="reg_box" rows="5" cols="5" name="message" id="message"></textarea>      
                <input type="submit" value="Submit" name="submit" class="login_btn">
              </div>
                </form>
              </div>
            </div>
          </div>     
        </div>
        </div>
      </div>
    </div>

    <div class="col-md-12  col-sm-12 contact_bottom">
      
  </div>
  <!-- </div> -->


  <!--==========================
      Contact Section
    ============================-->
<?php include_once('includes/pre-footer.php');?>

</body>


<script>
        grecaptcha.ready(function() {
          grecaptcha.execute('6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK', {action: 'submit'}).then(function(token) {
              document.getElementById("g-token").value = token;
          });
        });
</script>

</html>

<?php include_once('includes/footer.php');?>



<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
<script type="text/javascript">
  $(document).ready(function() {
  $('.collapse.in').prev('.panel-heading').addClass('active');
  $('#accordion, #bs-collapse')
    .on('show.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').addClass('active');
    })
    .on('hide.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').removeClass('active');
    });
});
</script>






      <!-- <script src="lib/jquery/jquery.min.js"></script> -->
  <!-- <script src="lib/jquery/jquery-migrate.min.js"></script> -->
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <!-- <script src="lib/owlcarousel/owl.carousel.min.js"></script> -->
  <!-- <script src="lib/isotope/isotope.pkgd.min.js"></script> -->
  <script src="lib/lightbox/js/lightbox.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  