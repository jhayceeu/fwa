<?php 
session_start();
// header("X-Frame-Options: DENY");
include_once('includes/config.php');
include_once('includes/header.php');
$job_keyword=$location=$sql_search=$age=$category="";

if(isset($_POST['search-backup']))
{
    $job_keyword=$_POST['job_keyword'];
    $location=$_POST['location'];
    $age=$_POST['age_braket'];
    $tag=$_POST['tag'];
  
  if(isset($_POST['checkAlltag']))
  {
    $location=implode(",",$_POST['checkAlltag']);
    
  }

     $_SESSION['key']=$job_keyword;
     $_SESSION['category']=$category;
     $_SESSION['tag']=$tag;
     $_SESSION['location']=$location;
     $_SESSION['age_braket']=$age;
    header('location:employer/home.php');
}

if(isset($_POST['search']))
{
  $tag=$_POST['tag'];
  if(isset($_POST['checkAll']))
  {
    $category=implode(",",$_POST['checkAll']);
    if($category!="")
    {
      $n_array=explode(',',$category);
    }
  }

  if(isset($_POST['checkAlltag']))
  {
    $location=implode(",",$_POST['checkAlltag']);
    if($location!="")
    {
      $n_array1=explode(',',$location);
    }
  }
  
   $job_keyword=$_POST['job_keyword'];
    if(isset($_POST['age_braket']))
    {
       $age=$_POST['age_braket'];
    }
    $extra="";

    
    if($job_keyword!="")
    {
      if($extra=="")
      {
        $extra=" where (no_qualification like '%".$job_keyword."%') ";
        
      }
      else
      {
        $extra=$extra." and (no_qualification like '%".$job_keyword."%') ";
      }
     
    }

    if($age!="")
    {
      if($extra=="")
      {
        $extra=" where (age_braket like '%".$age."%' )";
      }
      else
      {
        $extra=$extra." and (age_braket like '%".$age."%' )";
      }
     
    }

        if($tag!="")
        {
          if($extra=="")
          {
            $extra=" where (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
          }
          else
          { 

            $extra=$extra." and (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
          }

        }

          if($location!="")
        {


          if($extra=="")
          {
           
            $extra=" where (live_in in (".$location.")  )";
          }
          else
          {  
            $extra=$extra." and (live_in in (".$location.") ) ";
          }
        }



          $sql_search="select * from tbl_jobseeker ".$extra; 
          if($extra=="") 
          {
            $sql_search=$sql_search." where work_status!=2 limit 3";
          }
          else
          {
            $sql_search=$sql_search." and work_status!=2 limit 3";
          }
  
}
?>

<script>
  $("#search").submit( function() {
    $('body').scrollTo('#searchResult',{duration:2000});
    return false;
});
</script>

<style>
.emp_profile h1,h2,h3,h4,h5,h6{
    
    font-family: 'Comfortaa', cursive;
    font-weight: 600;
}
p {
  font-family: 'Comfortaa', cursive;
    font-weight: 600;
}

</style>


  <div class="col-md-12 emp_profile" class="banner">
    <div class="row">
      <img src="img/v3.jpg" height="700" width="100%">
        <div class="container">
           <div class="intro-info" style="position:absolute;top:190px;">
              <p>FIND WORK IN AUSTRALIA</p>
              <h2>Find or Get Found</h2>
              <div>
                <a href="jobseeker/register.php" class="btn-get-started scrollto">I AM LOOKING FOR A JOB</a>
                <a href="#about" class="btn-services scrollto">I WANT TO HIRE SOMEONE</a>
              </div>
              <h1 style="font-size: 12vw; text-align: : left;">Hired</h1>
           </div>
        </div>
    </div>
  </div>


  <main id="main"> 
    <section id="about">
      <div class="container">
        <header class="section-header">
            <h3>Find a job ready Candidate Today!</h3>
            <p>Handpick your job-ready candidate based on your needs at a seriously competitive rate.
            Search by tags, area, age and more and get a free preview of your candidates profiles.
            Only spend what you need with our incredibly affordable plans.</p>
            <div class="col-md-12" style="text-align: center;">
             <a href="#works" class="btn-services scrollto abt_btn">HOW DOES IT WORK?</a>
           </div>
        </header>
        <div class="col-md-12 search">
          <div class="col-md-12" style="margin-top: 50px;">
             <a class="btn-services scrollto abt_btn">FIND A CANDIDATE</a>
          </div>
    
          </div>
        </div>


<!-- ------------------------- -->
<div class="container">
           <div class="job-search">
              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

              </ul>
              <div class="tab-content bg-white p-4 rounded" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-job" role="tabpanel" aria-labelledby="pills-job-tab">
                  <form action="#searchResult" method="post">
                    <div class="row">
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      &nbsp;
                      <label class="reg_label">Industry</label>

                      <select id="primary_business_industry" name="job_keyword" class="search_box">
                      <option value="" selected="selected">Select</option>
                          <?php 
                            $result = mysqli_query($con,"SELECT DISTINCT industry_name,id from tbl_industry ORDER BY industry_name ASC");
                                while($row = mysqli_fetch_array($result))
                                      { $job_keyword= $row['id']  ?>
                                <option value="<?php if($job_keyword!="") echo $job_keyword; ?>"><?php echo $row['industry_name'];?></option>                                                    
                                    <?php }  ?> 
                                <option value="others">Others</option>
                      </select>
                      </select>
                      </div>
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                    
                      <label class="reg_label">Tag</label>
                      <input class="form-control" type="text" name="tag" placeholder="Tag" >

                      </div>  

          <!-- tag -->
              <div class="col-md-3">              
                  <label class="reg_label">Location</label>

                   <div class="selectbox1 selectwrap1">
                      <div class="selectbox-title1 form-control">
                        <span class="plzselect1">Location</span>          
                      </div>
                  <div class="selectbox-content1">
                    <div class="input-group">

                      <input class="form-control" type="text"  name="location" placeholder="Search" id="box1">
                    </div>
                      <ul class="ul-list1">
                      <?php
                      $sql_town="select * from tbl_towns";
                      $res_town=$con->query($sql_town);
                      while($row_town=$res_town->fetch_assoc())
                      {            
                      ?>
                                  <li style="padding-left: 10px;"><input class="checkbox-custom1" name="checkAlltag[]" type="checkbox" id="test1<?php echo $row_town['id']; ?>" value="<?php echo $row_town['id']; ?>" /><label for="test1<?php echo $row_town['id']; ?>" class="checkbox-custom-label checkbox"><?php echo $row_town['name']; ?></label></li>
                      <?php
                      }
                      ?>          
                      </ul>

                </div>
               
            </div>
              </div>
          <!-- </div> -->
   
                    <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      <label class="reg_label">Age</label>
                      &nbsp;
                      <select id="age_braket" name="age_braket"  class="form-control"  >
                        <option value="" selected="selected" disabled>Age</option>
                        <option value="18-25"<?php  if($age=="18-25"){ echo "selected"; }?>>18 - 25</option>
                        <option value="25-30"<?php  if($age=="25-30"){ echo "selected"; }?>>25 - 30</option>
                        <option value="35-40"<?php  if($age=="35-40"){ echo "selected"; }?>>35 - 40</option>
                        <option value="40-45"<?php  if($age=="40-50"){ echo "selected"; }?>>40 - 50</option>
                        <option value="50+"<?php  if($age=="50+"){ echo "selected"; }?>>50+</option>
                      </select>
                    </div>
          <!-- tag end -->
                   
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      <!-- <br> -->
                        <input type="submit"  name="search" class="btn-services scrollto search_btn" value="Search" style="border: none;">
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
</div>


<?php
 $sql_search;
if($sql_search!="")
{
    $res=$con->query($sql_search);


?>


<div class="site-section bg-light" id="searchResult">
      <div class="container">
        <div class="row justify-content-start text-left mb-5">
          <div class="col-md-9" data-aos="fade">
            <h2 class="font-weight-bold text-black">Search Results</h2>
          </div>
          <!-- <div class="col-md-3" data-aos="fade" data-aos-delay="200">
            <a href="#" class="btn btn-primary py-3 btn-block"><span class="h5">+</span> Post a Job</a>
          </div> -->
        </div>
        <?php 
        $i=0;
        while($row=$res->fetch_assoc())
        {
          $_SESSION['qry']=$sql_search;
            $loc="";
            $sql_location="select * from tbl_towns where id='".$row['live_in']."'";
            $res_location=$con->query($sql_location);
            if($row_location=$res_location->fetch_assoc())
            {
                $loc=$row_location['name'];

            }

            $job="";
            if($row['no_qualification']!="")
            {
            $sql_job="select * from tbl_industry where id in (".$row['no_qualification'].")";
            $res_job=$con->query($sql_job);
            while($row_job=$res_job->fetch_assoc())
            {
                if($job=="")
                {
                    $job=$row_job['industry_name'];
                }
                else
                {
                $job=$job."<br>".$row_job['industry_name'];
                }

            }
          }



        ?>

        <div class="row" data-aos="fade" id="result_div">
         <div class="col-md-12">

           <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">

              <div class="mb-4 mb-md-0 mr-5">
               <div class="job-post-item-header d-flex align-items-center">
                 <h2 class="mr-3 text-black h4"><?php echo $row['first_name']." ".$row['last_name']; ?></h2>
                 <?php
                 if($row['work_status']=="1")
                 {
                   ?>
               <img src="img/one-token.png"  height="30" width="30">
                  <?php
                }
              
                 else  
                 {
                   ?>
                     <img src="img/two-token.png"  height="30" width="30">
                
                 <?php
                 }
                 ?>

                 <?php
                 if($row['gov_support']=="Yes")
                 {
                   ?>
                   <label>
                  
                  <!-- &#9989; MAY COME WITH GOVERNMENT SUPPORT</label> -->
                 &nbsp;
                 <i class="fa fa-university" aria-hidden="true"></i>
                  MAY COME WITH GOVERNMENT SUPPORT

                 <?php
                 }
              
                 else 
                 {
                   ?>
                      <!-- <input type="checkbox" name="gov_app" readonly/> -->
                    <?php
                 }
                 ?>
              
               </div>
               
               <div><span class="fl-bigmug-line-big104"></span> <span><?php echo $loc; ?></span></div>
               <div class="job-post-item-body d-block d-md-flex">
                 <div class="mr-3"><span ></span> <a href="#"><?php echo $job; ?></a></div>
              
            
            
               </div>
               <div class="mr-3" style="margin-top:5px;"><span >Bio:</span> <a href="#"><?php echo $row['bio'];  ?></a></div> 
              <div class="mr-3"> 
           

               <?php
                 if($row['gender']=="male")
                 {
                   ?>
                   <i class="fa fa-male" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                  <?php
                 }
              
                 else 
                 {
                   ?>
                     <i class="fa fa-female" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                
                 <?php
                 }
                 ?>
              </div> 

               <?php 
                 if($row['visa_type']!="")
                 {
                     ?>
                  <div class="badge-wrap">
                
               
               
                
                 </div>
                </br> <span class="bg-primary text-white badge py-2 px-3"><?php echo $row['visa_type']; ?></span>
                 <?php 
                 }
                 ?>
              </div>

              <div class="ml-auto">
                <!-- <a href="#" class="btn btn-secondary rounded-circle btn-favorite text-gray-500"><span class="icon-heart"></span></a> -->
                <a href="employer/login.php" class="btn btn-primary py-2">View</a>
              </div>
           </div>

         </div>
        </div>
        <?php
        $i++;


        }
        ?>

      

      </div>
    </div> 


<?php
  }
    ?>


  </div>
</div>


<!-- --------------------------------------- -->


        <div class="container">
        <div class="col-md-12 working" id="works">
          <h2>How does it work?</h2>
        </div>
        <div class="row about-container" style="margin-top: 60px;"> 
          <div class="col-lg-1">         
          </div>
          <div class="col-lg-6 content wow fadeInUp work_para">
            <p>
              Jobseekers fill out their profile. It includes their age, location, how long they will be there, the industries they are interested in and more.
            </p>
            <div class="col-md-12" style="margin-top: 50px;">
              <div class="row">
                <a href="jobseeker/understanding.php" class="btn-services scrollto service_btn">SEE A JOBSEEKER PROFILE</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 background order-lg-2 order-1 wow fadeInUp">
            <img src="img/about-img.svg" class="img-fluid" alt="">
          </div>
           <!-- <div class="col-lg-2"></div> -->
        </div>

        <div class="row about-extra">
          <div class="col-lg-1">         
          </div>
          <div class="col-lg-4 wow fadeInUp">
            <img src="img/about-extra-1.svg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0 work_para">            
            <p>
              Employers run a search for suitable candidates and use tokens to unlock contact information such as email address and phone number.
            </p> 
            <div class="col-md-12" style="margin-top: 50px;">
              <div class="row">
                <a href="jobseeker/understanding.php" class="btn-services scrollto service_btn">SEE THIS IN ACTION</a>
              </div>
            </div>          
          </div>
        </div>

        <div class="row about-extra">
          <div class="col-lg-1"></div>
          
          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 work_para">           
            <p>
             Jobseekers receive an email or phone call to talk about the job! 
            </p> 
            <div class="col-md-12" style="margin-top: 5px;">
              <div class="row">
                <a href="jobseeker/register.php" class="btn-services scrollto service_btn">I'M A JOBSEEKER</a>
              </div>
            </div>  
            <div class="col-md-12" style="margin-top: 5px;">
              <div class="row">
                <a href="employer/register.php" class="btn-services scrollto service_btn">I'M AN EMPLOYER</a>
              </div>
            </div>            
          </div>
          <div class="col-lg-4 wow fadeInUp order-1 order-lg-2">
            <img src="img/about-extra-2.svg" class="img-fluid" alt="">
          </div>
        </div>
      </div>
    </section>
  </div>


<div class="col-md-12 middle_banner">
  <div class="container">
      <h3>Are you outside Australia or new to the country?</h3>
  </div>
</div>

<div class="col-md-12 banner2">
  <div class="row">
    <div class="col-md-12 banner_text">
      <div class="container">
        <h1>YES</h1>
          <h5>We can help.</h5>
          <p>If you are looking for work in Australia and you are currently out of the country, we can help.</p>
          <p>In order to be succesfully employed, you will need a valid email address. On arrival into Australia you will need an Australian phone number.<br>
          By creating your Jobseeker Profile, you will automatically get a great deal.</p>
          <a href="#about" class="btn-get-started scrollto banner_text_btn" style="margin-top:15px;">get my australian phone number</a>
          <br><br>
          <a href="benefits.php" class="btn-get-started scrollto banner_text_btn" style="margin-bottom:30px;">See all benefits</a>
      </div>
    </div>
  </div>
</div>


<!-- </div> -->
<div class="col-md-12 industries">
  <div class="container">
    <h3>Popular Industries</h3>
    <div class="row">
      <div class="col-md-3">
        <img src="img/icon1.png" style="height: 126px;width: 90px;">
        <h4>BARISTA | CAFE</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon2.png" style="height: 126px;width: 90px;">
         <h4>BAR | RESTAURANT</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon3.png" style="height: 126px;width: 90px;">
         <h4>FRUIT PICKING</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon4.png" style="height: 126px;width: 90px;">
         <h4>RETAIL</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon5.png" style="height: 126px;width: 90px;">
         <h4>CONSTRUCTION</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon6.png" style="height: 126px;width: 90px;">
         <h4>CUSTOMER | CALL CENTRE</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon7.png" style="height: 126px;width: 90px;">
         <h4>CLEANING</h4>
      </div>
      <div class="col-md-3">
        <img src="img/icon8.png" style="height: 126px;width: 90px;">
         <h4>FACTORY</h4>
      </div>
    </div>

  <!-- <div class="col-md-12 "> -->
      <!-- <div class="container"> -->

  </div>
</div>



<div class="col-md-12 banner3">
  <div class="row">   
      <div class="col-md-12 banner_text">
    <div class="container">
     

  <div class="industy_tabs panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="row">
        <div class="col-md-6">
          <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              CAFE | BARISTA 
              </a>
            </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <p>Working in the hospitality industry in Australia is great way to work and travel. Cafe’s are located in the key tourist locations and many have high and low seasons, so there are plenty of hiring opportunitues.</p>
                <p>You can work as a Barista without formal qualifications. We recommend you know the vocabularly extensively used in cafe’s and restaurants.</p>
              </div>
            </div>
          </div>
          <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo">
                BAR | RESTAURANT
              </a>
            </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <p>In Australia you need a RSA certificate to server alcohol. Gaining this certificate is easy and can be completed online.</p>

                <p>To get an idea of pay rates, please <a target="_blank" href="https://www.payscale.com/research/AU/Job=Bar_Attendant/Hourly_Rate">click here.</a></p>
              </div>
            </div>
          </div>
           <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo">
                FRUIT PICKING
              </a>
            </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                  <h5>When & where to go for fruit picking jobs Australia</h5>
                  <p>Before you are going to look for a fruit picking job, it would be useful to determine where you would like to go to. There are many possibilities of different fruit picking jobs as Australia is a huge country with numerous different landscapes and weather conditions.</p>
                  <h5>Queensland</h5>
                  <div class="row">
                    <p class="col-md-8">Queensland grows 1/3 of the nation’s fruit and vegetables. Farmers hire migrant workers, fruit & vegetables pickers and other (experienced) farmhands. Farm work varies across the Queensland region – the farmers grow sugar cane, beans, seasonal fruits and vegetables, also cattle, cotton and wool.</p>
                    <div class="col-md-4">
                      <img src="img/fruit1.png" height="auto" width="100%">
                    </div>
                  </div>
                  <p>In Tully they grow many bananas. Tully is located 180 kilometres south of Cairns and is known for being the wettest place in Australia, receiving an average of 4134 mm of rain per year. In the Tully area there are lots of beautiful beach towns such as Mission Beach and Bingal Bay. The Tully River is also famous among people who are crazy about rafting!</p>

                  <p>From Stanthorpe in the south to the Atherton Tablelands in the far North; more than 120 types of fruit and vegetable are picked and packed in the state every year all year round so new and motivated workers are needed to fill a variety of fruit picking and harvest jobs.</p>
                  <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Feb – Mar</td>
<td>Pears / Apples</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Rock Melon</td>
<td>St George</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Ginger</td>
<td>Sunshine Coast</td>
</tr>
<tr>
<td>Mar – Dec</td>
<td>Vegetables</td>
<td>Bundaberg</td>
</tr>
<tr>
<td>Apr – Jun</td>
<td>Citrus</td>
<td>Mundubbera</td>
</tr>
<tr>
<td>Apr – Oct</td>
<td>Various</td>
<td>Lockyer Valley</td>
</tr>
<tr>
<td>Apr – Nov</td>
<td>Beans</td>
<td>Mary Valley</td>
</tr>
<tr>
<td>Apr – Dec</td>
<td>Vegetables / Tomatoes</td>
<td>Bowen / Ayr</td>
</tr>
<tr>
<td>May – Oct</td>
<td>Broccoli</td>
<td>Toowoomba</td>
</tr>
<tr>
<td>May – Dec</td>
<td>Sugar Cane</td>
<td>Ayr / Ingham / Innisfail</td>
</tr>
<tr>
<td>Jul – Sep</td>
<td>Ginger</td>
<td>Sunshine Coast</td>
</tr>
<tr>
<td>Jul – Dec</td>
<td>Onions</td>
<td>Lockyer Valley</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Mangoes / Lychees /<br> Avocados / Bananas</td>
<td>Mareeba</td>
</tr>
<tr>
<td>Nov – Jan</td>
<td>Plums</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Nov – Jan</td>
<td>Cotton</td>
<td>Goodiwindi / St George</td>
</tr>
<tr>
<td>Oct – Jan</td>
<td>Peaches</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Nov – Mar</td>
<td>Cotton</td>
<td>Toowoomba / Milleran</td>
</tr>
<tr>
<td>Dec – Jan</td>
<td>Bananas / Sugar</td>
<td>Innisfail / Tully</td>
</tr>
<tr>
<td>Dec – Mar</td>
<td>Various</td>
<td>Stanthorpe</td>
</tr>
</tbody>
</table>
              <h5>New South Wales</h5>
                  <div class="row">
                    <p class="col-md-8">New South Wales is one of best places to find seasonal farm jobs in Australia. The main produce in New South Wales include sheep, cattle, pigs, hay, apples, cherries, pears, legumes, maize, nuts, wool, wheat, and rice. Farmhands, migrant workers, fruit pickers, and vegetable pickers may all find work across New South Wales in fields and on ranches. November to April is the busiest harvest period which peaks in February. However, the harvest differs per crop.</p>
                    <div class="col-md-4">
                      <img src="img/fruit2.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>For example – Griffith is a thriving agricultural and industrial city boasting the third largest population in the Riverina region and many well regarded wineries. Next to grape picking it’s also possible to do orange picking or onion picking at different times of the year. Griffith is located approximately 600 kilometres west of Sydney. This country town is well known for its food, wine and festivals.</p>
                  
                  <table>
                  <tbody>
                  <tr>
                  <td><strong>Harvest</strong></td>
                  <td><strong>Crop</strong></td>
                  <td><strong>Location</strong></td>
                  </tr>
                  <tr>
                  <td>Jan – Mar</td>
                  <td>Stonefruit</td>
                  <td>Young</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Prunes</td>
                  <td>Young</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Pears</td>
                  <td>Orange</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Grapes</td>
                  <td>Leeton / Hunter Valley</td>
                  </tr>
                  <tr>
                  <td>Feb – Apr</td>
                  <td>Apples</td>
                  <td>Orange</td>
                  </tr>
                  <tr>
                  <td>Mar – Apr</td>
                  <td>Grapes</td>
                  <td>Tumbarumba</td>
                  </tr>
                  <tr>
                  <td>Mar – May</td>
                  <td>Apples</td>
                  <td>Batlow</td>
                  </tr>
                  <tr>
                  <td>Mar – Jun</td>
                  <td>Cotton Picking</td>
                  <td>Narrabri / Mooree</td>
                  </tr>
                  <tr>
                  <td>Sep – Oct</td>
                  <td>Asparagus</td>
                  <td>Gundagai</td>
                  </tr>
                  <tr>
                  <td>Sep – Dec</td>
                  <td>Asparagus</td>
                  <td>Cowra</td>
                  </tr>
                  <tr>
                  <td>Sep – Apr</td>
                  <td>Oranges</td>
                  <td>Griffith</td>
                  </tr>
                  <tr>
                  <td>Nov – Dec</td>
                  <td>Cherries</td>
                  <td>Young / Orange</td>
                  </tr>
                  <tr>
                  <td>Nov – Apr</td>
                  <td>Oranges</td>
                  <td>Lecton</td>
                  </tr>
                  <tr>
                  <td>Dec – Jan</td>
                  <td>Onions</td>
                  <td>Griffith</td>
                  </tr>
                  <tr>
                  <td>Dec – Mar</td>
                  <td>Stone Fruit</td>
                  <td>Tumut / Batlow</td>
                  </tr>
                  <tr>
                  <td>Dec – Apr</td>
                  <td>Blueberries</td>
                  <td>Tumbarumba</td>
                  </tr>
                  </tbody>
                  </table>

                  <h5>Victoria</h5>
                  <div class="row">
                    <p class="col-md-8">November to April (peaking in February) is the main season for fruit and vegetable picking jobs in Victoria. There is plenty of work in the central northern areas around Shepparton. The Murray River area is also a great area to look for work; places such as Mildura and Swan Hill often require fruit picking / harvest workers. Main harvests in these areas include orchard fruits, tomatoes, tobacco, grapes and soft fruits.</p>
                    <div class="col-md-4">
                      <img src="img/fruit3.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>Fruit picking in Cobram – “Cobram is a large fruit growing area situated on the Murray River just 250 kilometres north of Melbourne. This region is well known for its irrigated fruit and dairy with large areas of stone fruit varieties. The small farm area expanded after WWII , when a significant number of Italian immigrants arrived and established themselves in the farming community.” –<a target="_blank" href="http://workstay.com.au/fruit-picking-cobram-victoria"> http://workstay.com.au/fruit-picking-cobram-victoria</a></p>
                    <p>Fruit picking Yarra Valley in Victoria – “This valley is about 40 kilometres north east of Melbourne. A large variety of cool climate crops have traditionally been grown in the Valley and there are more than 80 vineyards and wineries in the Valley with many different selections of wines.” –<a target="_blank" href="https://jobsearch.gov.au/"> https://jobsearch.gov.au/</a></p>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Tomatoes</td>
<td>Ardmona / Shepparton / Rochester</td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Pears / Peaches / Apples</td>
<td>Ardmona / Shepparton / Cobram</td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Tobacco</td>
<td>Ovens / King &amp; Kiewa Valleys</td>
</tr>
<tr>
<td>Feb – Mar</td>
<td>Grapes</td>
<td>Lake Boga / Swan Hill / Nyah West</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Ovens / King &amp; Kiewa Valleys</td>
</tr>
<tr>
<td>Mar – Apr</td>
<td>Apples</td>
<td>Buckland Valley / Stanley / Wandilong / Red Hill / Main Range</td>
</tr>
<tr>
<td>Sep – Nov</td>
<td>Asparagus</td>
<td>Dalmore</td>
</tr>
<tr>
<td>Oct – Dec</td>
<td>Strawberries</td>
<td>Silvan</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Cherries</td>
<td>Boweya / Glenrowan / Wagandary</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Cherries / Berries</td>
<td>Wandin / Silvan</td>
</tr>
<tr>
<td>Nov – Dec</td>
<td>Tomato Weeding</td>
<td>Echuca / Rochester</td>
</tr>
</tbody>
</table>
              <h5>Tasmania</h5>
                  <div class="row">
                    <p class="col-md-8">Tasmania is all about apples. It grows almost a fifth of the apples in Australia, approximately 55,000 tonnes every year. The main areas are in the Huon district South of Hobart. Tasmania has a relatively short fruit picking season, generally from December to the end of May, fruit picking and harvest jobs are available all around Tasmania. As well as picking apples there are also pears, stone fruits, hops, grapes, berries and much more.</p>
                    <div class="col-md-4">
                      <img src="img/fruit4.png" height="auto" width="100%">
                    </div>
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Feb</td>
<td>Scallop Splitting</td>
<td>Bicheno</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Huon / Tamar Valley</td>
</tr>
<tr>
<td>Mar – Apr</td>
<td>Hops</td>
<td>Scottsdale / New Norfolk / Devenport</td>
</tr>
<tr>
<td>Mar – May</td>
<td>Apples</td>
<td>Hunter Valley / Tasman Peninsula / West Tamar</td>
</tr>
<tr>
<td>Dec – Jan</td>
<td>Soft Fruit</td>
<td>Channel District / Huon / Kingborough / Derwent Valley</td>
</tr>
</tbody>
</table>
                 <h5>South Australia</h5>
                  <div class="row">
                    <p class="col-md-8">fruit and vegetable picking jobs all year around e.g. picking citrus and soft fruits such as raspberries and strawberries around the Riverland area.</p>
                    <div class="col-md-4">
                      <img src="img/fruit5.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>February to April work is available around The Barossa Valley picking and packing the grapes.</p>

<table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Mar</td>
<td>Dried Fruits</td>
<td>Riverland</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes &amp; Peaches</td>
<td>Riverland</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Southern Vales / Barossa Valley</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Apples &amp; Pears</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Feb – Aug</td>
<td>Brussel Sprouts</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Jun – Aug</td>
<td>Oranges</td>
<td>Riverland</td>
</tr>
<tr>
<td>Jun – Sep</td>
<td>Pruning</td>
<td>Riverland</td>
</tr>
<tr>
<td>Sep – Jan</td>
<td>Oranges</td>
<td>Riverland</td>
</tr>
<tr>
<td>Oct – Feb</td>
<td>Strawberries</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Dec – Feb</td>
<td>Apricots</td>
<td>Riverland</td>
</tr>
</tbody>
</table>
                  <h5>Western Australia</h5>
                  <div class="row">
                    <p class="col-md-8">Fruit picking and harvest jobs are available in the Southwest of the state. From October to June you can harvest grapes and orchard fruits. March to November there is also fishing and processing work available for crayfish, prawns and scallops between Fremantle and Carnarvon. From May to October head northeast, around Kununurra for harvest jobs and fruit picking.</p>
                    <div class="col-md-4">
                      <img src="img/fruit6.png" height="auto" width="100%">
                    </div>
                    
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Mar</td>
<td>Grapes</td>
<td>Margaret River / Swan Valley / Mt Barker</td>
</tr>
<tr>
<td>Mar – May</td>
<td>Apples &amp; Pears</td>
<td>Pemberton / Donnybrook / Manjimup</td>
</tr>
<tr>
<td>Mar – Oct</td>
<td>Prawning &amp; Scalloping</td>
<td>Carnavon</td>
</tr>
<tr>
<td>May – Sep</td>
<td>Zucchini &amp; Rock Melons</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Apr – Nov</td>
<td>Melons</td>
<td>Coorow</td>
</tr>
<tr>
<td>Jun – Dec</td>
<td>Melons &amp; Tomatoes</td>
<td>Carnavon</td>
</tr>
<tr>
<td>Jul – Aug</td>
<td>Bananas</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Jul – Dec</td>
<td>Wildflowers</td>
<td>Coorow / Muchea</td>
</tr>
<tr>
<td>Oct – Jan</td>
<td>Mangoes</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Nov – Jun</td>
<td>Rock Lobster</td>
<td>Fremantle / Kalbarri / Dongara / Geraldton / Broome</td>
</tr>
<tr>
<td>All Year</td>
<td>Fishing</td>
<td>Broome</td>
</tr>
</tbody>
</table>
               <h5>Northern Territory</h5>
                  <div class="row">
                    <p class="col-md-8">May to October picking melons around Katherine and Darwin is popular. From October to November head to Katherine and Darwin for mango picking and packing jobs as well as citrus and bananas which are grown all year around there.</p>
                    <div class="col-md-4">
                      <img src="img/fruit7.png" height="auto" width="100%">
                    </div>
                    
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Mar – Oct</td>
<td>Melons</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>Oct – Nov</td>
<td>Mangoes</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>All Year</td>
<td>Citrus / Bananas</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>All Year</td>
<td>Asian Vegetables &amp; Cutting Flowers</td>
<td>Darwin</td>
</tr>
</tbody>
</table>
<p>Information from:<a target="_blank" href="http://www.fruitpicking.org/fruit–picking–essentials/crops–calendar"> http://www.fruitpicking.org/fruit–picking–essentials/crops–calendar</a></p>

              </div>
            </div>
          </div>
           <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                RETAIL
              </a>
            </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <p>Every part of Australia needs people to work in retail.</p>

                <p>Shop work can be physical, such as if you work in a warehouse and require long hours. Don’t forget to put in your profile if you have any physical issues that may be important.</p>

                <p><a target="_blank" href="https://www.payscale.com/research/US/Skill=Retail/Hourly_Rate">Click here</a> to see the Award Rates for retail in Australia.</p>
              </div>
            </div>
          </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              CONSTRUCTION
            </a>
          </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
             Building and Construction award rates can be found by clicking <a target="_blank" href="http://awardviewer.fwo.gov.au/award/show/MA000020#P551_59778">here.</a>
            </div>
          </div>
        </div>
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="heading6">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
              CUSTOMER SERVICE
            </a>
          </h4>
          </div>
          <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Customer Service covers everything from retail to call centres.</p>

                <p>If you are good with people, this could be a great option to get you working quicking with room to move into adminstration or management.</p>
            </div>
          </div>
        </div>

        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapseThree">
              CLEANING
            </a>
          </h4>
          </div>
          <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Cleaning can be a rewarding job that could include domestic or commercial work.</p>

              <p>Experience will definately pay, so if you have any, put it in your profile!</p>

              <p><a target="_blank" href="https://www.payscale.com/research/US/Skill=Cleaning/Hourly_Rate">Click here</a> to get an idea of the wages you could be paid.</p>
            </div>
          </div>
        </div>
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseThree">
              FACTORY
            </a>
          </h4>
          </div>
          <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Factory work may not sound glamourous, but you can be assured that the training will be excellent and all legal conditions will be met.</p>

              <p><a target="_blank" href="https://www.payscale.com/research/US/Job=Factory_Worker/Hourly_Rate">Click here</a> to see a pay estimate for this industry.</p>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </div>


        </div>
      </div>  
  </div>
</div>


<style type="text/css">

.industy_tabs {
  margin-top:130px;
}
.industy_tabs h4{
    margin: 0px 0px;
}
.industy_tabs .panel-title a{
  font-size: 13px;
    font-weight: 600;
     color: #fff;
}
.industy_tabs .panel-body {
    background-color: #0c0c0c69;
    border-color: #8696b7;
    padding: 10px 15px;
    text-align: left;
        color: #eee;
    font-size: 14px;
}
.panel-default>.panel-heading {
  color: #fff;
  background-color: #0c0c0c69;
    border-color: #8696b7;
  padding: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
  text-align: left;
}

.panel-default>.panel-heading a:after {
  /*content: "";*/
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
  /*background-color: #eee;*/
}

.panel-default>.panel-heading a[aria-expanded="true"]:after {
  content: "\2212";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-default>.panel-heading a[aria-expanded="false"]:after {
  content: "\002b";
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}
</style>

    <!--==========================
      Contact Section
    ============================-->
<?php include_once('includes/pre-footer.php');?>

  </main>



<style>
 .checkbox-custom1 + .checkbox-custom-label:before {
    content: '';
    background: white;
    display: inline-block;
    vertical-align: middle;
    width: 18px;
    height: 18px;
    padding: 0px;
    text-align: center;
    border: 1px solid #fbc52d;
    border-radius: 0px;
    margin-bottom: 4px;
    margin-right: 10px;
}
.ul-list1 li{
  list-style: none;
}
.selectwrap1 {
    position: relative;
    float: left;
    width: 100%;
}
.selectbox-content1 .input-group {
    padding: 5px;
}

input[type=checkbox], input[type=radio] {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
}
.selectbox { width:100%; display:inline-block;}
.selectbox-title.form-control { padding-left: 16px;}
.selectbox-content { border:1px solid #d5d5d5; max-height:205px; overflow:auto; display:none; border-top: none;}
.selectbox-content ul { list-style:none; margin:0px; padding:0px; cursor: pointer;  position: relative; }
.selectbox-content ul li { padding-top:10px; width:100%; display:inline-block;}
.selectbox-content ul li:last-child { border-bottom:none;}
.selectbox-content ul li:first-child { border-top:none;}
.selectbox-content .input-group {padding: 5px;}
.loadbutton {
    padding: 10px;
}
.loadbutton .btn-primary {
    padding-top: 8px;
    padding-bottom: 8px;
}
.ul-list label {
    padding-left: 20px;
}
#box {
    height: 34px;
}
/***** INPUT FORM STYLES *****/
.form-horizontal .control-label {
  text-align: left; }

.control-label-pad .control-label {
  padding-top: 0px; }

.form-control {
  border-radius: 0;
  border: 1px solid #d5d5d5;
  background-color: white;
  height: 38px;
  color: #555555;
  -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0);
  box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0); }
  .form-control:focus {
    border-color: #fbc52d;
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(251, 197, 45, 0.6);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(251, 197, 45, 0.6); }
    .form-control {
    border-radius: 0;
    border: 1px solid #d5d5d5;
    background-color: white;
    height: 38px;
    color: #555555;
    -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0);
    box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0);
}

.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
  background-color: #dddddd; }
  .form-control[disabled]:focus, .form-control[readonly]:focus, fieldset[disabled] .form-control:focus {
    border: none;
    box-shadow: none; }

.input-group-addon {
  border-radius: 0;
  padding: 6px 12px;
  font-size: 14px;
  font-weight: normal;
  line-height: 1;
  color: #091d3a;
  background-color: #e6e6e6;
  border: 1px solid #d5d5d5; }

.form-group .control-label {
  color: #535353; }

/*select style*/
.selectwrap {
  position: relative;
  float: left;
  width: 100%; }
  .selectwrap:after {
    content: "\f0d7";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    text-align: center;
    line-height: 36px;
    position: absolute;
    width: 26px;
    height: 36px;
    background: white;
    right: 1px;
    top: 1px;
    pointer-events: none; }

/***** CUSTOM CHECKBOX STYLES *****/
.custom-checkbox-label .checkbox-pad {
  padding-top: 7px; }

.custom-checkbox label {
  font-weight: normal; }

.checkbox-custom {
  opacity: 0;
  position: absolute;
  display: inline-block;
  vertical-align: middle;
  margin: 0px;
  cursor: pointer; }

.checkbox-custom-label {
  display: inline-block;
  vertical-align: middle;
  margin: 0px;
  cursor: pointer;
  position: relative; }

.checkbox-custom + .checkbox-custom-label:before {
  content: '';
  background: white;
  display: inline-block;
  vertical-align: middle;
  width: 18px;
  height: 18px;
  padding: 0px;
  text-align: center;
  border: 1px solid #fbc52d;
  border-radius: 0px;
  margin-bottom: 4px;
  margin-right: 10px; }
.checkbox-custom:checked + .checkbox-custom-label:before {
  content: "\f00c";
  font-family: 'FontAwesome';
  color: #fbc52d;
  font-size: 12px;
  font-weight: 100;
  line-height: 5px;
  padding-top: 6px; }
.checkbox-custom:focus + .checkbox-custom-label {
  outline: 0px solid #dddddd; }
   .selectbox1 { width:100%; display:inline-block;}
.selectbox-title1.form-control { padding-left: 16px;}
.selectbox-content1 { border:1px solid #d5d5d5; max-height:205px; overflow:auto; display:none; border-top: none;}
.selectbox-content1 ul { list-style:none; margin:0px; padding:0px; cursor: pointer;  position: relative; }
.selectbox-content1 ul li { padding-top:10px; width:100%; display:inline-block;}
.selectbox-content1 ul li:last-child { border-bottom:none;}
.selectbox-content1 ul li:first-child { border-top:none;}
.selectbox-content1 .input-group {padding: 5px;}
.loadbutton1 {
    padding: 10px;
}
.loadbutton1 .btn-primary {
    padding-top: 8px;
    padding-bottom: 8px;
}
.ul-list1 label {
    padding-left: 20px;
}
#box1 {
    height: 34px;
}
/***** INPUT FORM STYLES *****/
.form-horizontal1 .control-label {
  text-align: left; }

.control-label-pad1 .control-label {
  padding-top: 0px; }

.form-control {
  border-radius: 0;
  border: 1px solid #d5d5d5;
  background-color: white;
  height: 38px;
  color: #555555;
  -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0);
  box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0); }
  .form-control:focus {
    border-color: #fbc52d;
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(251, 197, 45, 0.6);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(251, 197, 45, 0.6); }

.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
  background-color: #dddddd; }
  .form-control[disabled]:focus, .form-control[readonly]:focus, fieldset[disabled] .form-control:focus {
    border: none;
    box-shadow: none; }

.input-group-addon {
  border-radius: 0;
  padding: 6px 12px;
  font-size: 14px;
  font-weight: normal;
  line-height: 1;
  color: #091d3a;
  background-color: #e6e6e6;
  border: 1px solid #d5d5d5; }

.form-group .control-label {
  color: #535353; }

/*select style*/
.selectwrap1 {
  position: relative;
  float: left;
  width: 100%; }
  .selectwrap1:after {
    content: "\f0d7";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    text-align: center;
    line-height: 36px;
    position: absolute;
    width: 26px;
    height: 36px;
    background: white;
    right: 1px;
    top: 1px;
    pointer-events: none; }

/***** CUSTOM CHECKBOX STYLES *****/
.custom-checkbox-label1 .checkbox-pad {
  padding-top: 7px; }

.custom-checkbox1 label {
  font-weight: normal; }

.checkbox-custom1 {
  opacity: 0;
  position: absolute;
  display: inline-block;
  vertical-align: middle;
  margin: 0px;
  cursor: pointer; }

.checkbox-custom-label1 {
  display: inline-block;
  vertical-align: middle;
  margin: 0px;
  cursor: pointer;
  position: relative; }

.checkbox-custom1 + .checkbox-custom-label:before {
  content: '';
  background: white;
  display: inline-block;
  vertical-align: middle;
  width: 18px;
  height: 18px;
  padding: 0px;
  text-align: center;
  border: 1px solid #fbc52d;
  border-radius: 0px;
  margin-bottom: 4px;
  margin-right: 10px; }
.checkbox-custom1:checked + .checkbox-custom-label:before {
  content: "\f00c";
  font-family: 'FontAwesome';
  color: #fbc52d;
  font-size: 12px;
  font-weight: 100;
  line-height: 5px;
  padding-top: 6px; }
.checkbox-custom1:focus + .checkbox-custom-label {
  outline: 0px solid #dddddd; }
   </style>
<?php include_once('includes/footer.php'); ?>


       <script>
   /****Dropdown toggle ****/
$(".selectbox-title").click(function() {
  $(".selectbox-content").slideToggle("slow");
});

/***Dynamic Checkbox generate***/
// var initial = 0;
// var maxlength = 100000;
// for (i = 0; i <= 10; i++) {
//   $('.ul-list').append('<li><input class="checkbox-custom" name="checkAll" type="checkbox" id="test' + i + '" /><label for="test' + i + '" class="checkbox-custom-label checkbox">Checkbox</label></li>');
// }

/***Add Checkboxes on load more button***/
function buttonclick() {
        for (var i = initial; i < initial + 500; i++) {
            $('.ul-list').append('<li><input class="checkbox-custom" name="checkAll" type="checkbox" id="test' + i + '" /><label for="test' + i + '" class="checkbox-custom-label checkbox">Checkbox</label></li>');
        }
        initial = initial + 500;
    }

$('#box').keyup(function () {

        var valThis = $(this).val().toLowerCase();
        
        if (valThis == "") {
            $('.ul-list > li').show();
        } else {
            $('.ul-list > li').each(function () {
                var text = $(this).text().toLowerCase();
              
                (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
            });
        };
    });
$('.fa-close').click(function () {
        // Selects all elements of type text and clear all
        $('#box').val('');
    });

/***Count Length of checkbox***/
/*var $checkboxes = $('.ul-list input[type="checkbox"]');

$checkboxes.change(function() {
  var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
  $('#edit-count-checked-checkboxes').text(countCheckedCheckboxes);
});

if (countCheckedCheckboxes > 0) {
            $('.plzselect').css('display', 'none');
            $('#edit-count-checked-checkboxes').css('display', 'inline-block');
            $('.selected').css('display', 'inline-block');
        }
        else {
            $('.plzselect').css('display', 'block');
            $('#edit-count-checked-checkboxes').css('display', 'none');
            $('.selected').css('display', 'none');
        }*/


   </script>

   <script>
   /****Dropdown toggle ****/
$(".selectbox-title1").click(function() {
  $(".selectbox-content1").slideToggle("slow");
});

/***Dynamic Checkbox generate***/
// var initial = 0;
// var maxlength = 100000;
// for (i = 0; i <= 10; i++) {
//   $('.ul-list').append('<li><input class="checkbox-custom" name="checkAll" type="checkbox" id="test' + i + '" /><label for="test' + i + '" class="checkbox-custom-label checkbox">Checkbox</label></li>');
// }

/***Add Checkboxes on load more button***/


$('#box1').keyup(function () {

        var valThis = $(this).val().toLowerCase();
        
        if (valThis == "") {
            $('.ul-list1 > li').show();
        } else {
            $('.ul-list1 > li').each(function () {
                var text = $(this).text().toLowerCase();
              
                (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
            });
        };
    });
$('.fa-close1').click(function () {
        // Selects all elements of type text and clear all
        $('#box1').val('');
    });

/***Count Length of checkbox***/
var $checkboxes = $('.ul-list1 input[type="checkbox"]');

$checkboxes.change(function() {
  var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
  $('#edit-count-checked-checkboxes1').text(countCheckedCheckboxes);
});

if (countCheckedCheckboxes > 0) {
            $('.plzselect1').css('display', 'none');
            $('#edit-count-checked-checkboxes1').css('display', 'inline-block');
            $('.selected').css('display', 'inline-block');
        }
        else {
            $('.plzselect1').css('display', 'block');
            $('#edit-count-checked-checkboxes1').css('display', 'none');
            $('.selected').css('display', 'none');
        }


   </script>
