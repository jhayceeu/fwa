<?php 
session_start();
include_once('../includes/config.php');
$job_keyword=$location=$sql_search=$age=$category="";

if(isset($_POST['search']))
{

  $tag=$_POST['tag'];
  if(isset($_POST['checkAll']))
  {
    $category=implode(",",$_POST['checkAll']);
    if($category!="")
    {
      $n_array=explode(',',$category);
    }
  }

  if(isset($_POST['checkAlltag']))
  {
    $location=implode(",",$_POST['checkAlltag']);
    if($location!="")
    {
      $n_array1=explode(',',$location);
    }
  }
  
   $job_keyword=$_POST['job_keyword'];
    if(isset($_POST['age_braket']))
    {
       $age=$_POST['age_braket'];
    }
    $extra="";

    
    if($job_keyword!="")
    {
      if($extra=="")
      {
        $extra=" where (no_qualification like '%".$job_keyword."%') ";
        
      }
      else
      {
        $extra=$extra." and (no_qualification like '%".$job_keyword."%') ";
      }
     
    }

    if($age!="")
    {
      if($extra=="")
      {
        $extra=" where (age_braket like '%".$age."%' )";
      }
      else
      {
        $extra=$extra." and (age_braket like '%".$age."%' )";
      }
     
    }

        if($tag!="")
        {
          if($extra=="")
          {
            $extra=" where (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
          }
          else
          { 

            $extra=$extra." and (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
          }

        }

          if($location!="")
        {


          if($extra=="")
          {
           
            $extra=" where (live_in in (".$location.")  )";
          }
          else
          {  
            $extra=$extra." and (live_in in (".$location.") ) ";
          }
        }



          $sql_search="select * from tbl_jobseeker ".$extra; 
          if($extra=="") 
          {
            $sql_search=$sql_search." where work_status!=2 limit 3";
          }
          else
          {
            $sql_search=$sql_search." and work_status!=2 limit 3";
          }


}

include_once('../includes/external_header.php');
?>

<!DOCTYPE html>
<html lang="en">

<style>
.emp_profile h1,h2,h3,h4,h5,h6{
    
    font-family: 'Comfortaa', cursive;
    font-weight: 600;
}
p {
	font-family: 'Comfortaa', cursive;
    font-weight: 600;
}

.banner2{
  background-image: url(../img/employer-background2.jpg);
  height: 500px;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  top: -20px;
}
</style>

<body>


  <div class="col-md-12 emp_profile" class="banner">
    <div class="row">
      <img src="../img/employer-background.jpg" height="600" width="100%">
        <div class="container">
           <div class="intro-info" style="position:absolute;top:90px;">
              <p>UNDERSTANDING</p>
              <h2>Jobseeker Profile</h2>
              <div>
                <a href="#search-jobseeker" class="btn-get-started scrollto">RUN JOBSEEKER SEARCH</a>
                <a href="../employer/login.php" class="btn-services scrollto">LOGIN TO EMPLOYER ACCOUNT</a>
              </div>
              <h2 style="font-size: 10vw">About</h2>
           </div>
        </div>
    </div>
  </div>



  		<div class="col-md-12 working" id="works">
          <h2>Preview</h2>
          <p>When you run a search, you will see a preview list of results like this one:</p>
          <img src="../img/jobseeker-preview.jpg" height="600" width="80%">
      </div>

      <div class="col-md-12 working" id="works">
          <h2>Expanded Preview</h2>
          <p>You will be able to see more details about the Jobseeker and decide if you want to contact them or save their profile for later.</p>
          <p>The only cost will be if you chose to use a token to unlock the contact information. You will be prompted before any tokens are taken from your account.</p>
          <img src="../img/jobseeker-view.jpg" height="" width="80%">
      </div>


      <div class="container" id="search-jobseeker">
        <div class="col-md-12 search">
          <div class="col-md-12" style="margin-top: 50px;">
             <a class="btn-services scrollto abt_btn">FIND A CANDIDATE</a>
          </div>
    
          </div>
        </div>

      <!-- ------------------------- -->
<div class="container">
           <div class="job-search">
              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

              </ul>
              <div class="tab-content bg-white p-4 rounded" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-job" role="tabpanel" aria-labelledby="pills-job-tab">
                  <form action="#searchResult" method="post">
                    <div class="row">
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      &nbsp;
                      <label class="reg_label">Industry</label>

                      <select id="primary_business_industry" name="job_keyword" class="search_box">
                      <option value="" selected="selected">Select</option>
                          <?php 
                            $result = mysqli_query($con,"SELECT DISTINCT industry_name,id from tbl_industry");
                                while($row = mysqli_fetch_array($result))
                                      { $job_keyword= $row['id']  ?>
                                <option value="<?php if($job_keyword!="") echo $job_keyword; ?>"><?php echo $row['industry_name'];?></option>                                                    
                                    <?php }  ?> 
                                <option value="others">Others</option>
                      </select>
                      </select>
                      </div>
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                    
                      <label class="reg_label">Tag</label>
                      <input class="form-control" type="text" name="tag" placeholder="Tag" >

                      </div>  

          <!-- tag -->
              <div class="col-md-3">              
                  <label class="reg_label">Location</label>

                   <div class="selectbox1 selectwrap1">
                      <div class="selectbox-title1 form-control">
                        <span class="plzselect1">Location</span>          
                      </div>
                  <div class="selectbox-content1">
                    <div class="input-group">

                      <input class="form-control" type="text"  name="location" placeholder="Search" id="box1">
                    </div>
                      <ul class="ul-list1">
                      <?php
                      $sql_town="select * from tbl_towns";
                      $res_town=$con->query($sql_town);
                      while($row_town=$res_town->fetch_assoc())
                      {            
                      ?>
                                  <li style="padding-left: 10px;"><input class="checkbox-custom1" name="checkAlltag[]" type="checkbox" id="test1<?php echo $row_town['id']; ?>" value="<?php echo $row_town['id']; ?>" /><label for="test1<?php echo $row_town['id']; ?>" class="checkbox-custom-label checkbox"><?php echo $row_town['name']; ?></label></li>
                      <?php
                      }
                      ?>          
                      </ul>

                </div>
               
            </div>
              </div>
          <!-- </div> -->
   
                    <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      <label class="reg_label">Age</label>
                      &nbsp;
                      <select id="age_braket" name="age_braket"  class="form-control"  >
                        <option value="" selected="selected" disabled>Age</option>
                        <option value="18-25"<?php  if($age=="18-25"){ echo "selected"; }?>>18 - 25</option>
                        <option value="25-30"<?php  if($age=="25-30"){ echo "selected"; }?>>25 - 30</option>
                        <option value="35-40"<?php  if($age=="35-40"){ echo "selected"; }?>>35 - 40</option>
                        <option value="40-45"<?php  if($age=="40-50"){ echo "selected"; }?>>40 - 50</option>
                        <option value="50+"<?php  if($age=="50+"){ echo "selected"; }?>>50+</option>
                      </select>
                    </div>
          <!-- tag end -->
                   
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      <!-- <br> -->
                        <input type="submit" name="search" class="btn-services scrollto search_btn" value="Search" style="border: none;">
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
</div>

<?php
 $sql_search;
if($sql_search!="")
{
    $res=$con->query($sql_search);


?>


<div class="site-section bg-light" id="searchResult">
      <div class="container">
        <div class="row justify-content-start text-left mb-5">
          <div class="col-md-9" data-aos="fade">
            <h2 class="font-weight-bold text-black">Search Results</h2>
          </div>
          <!-- <div class="col-md-3" data-aos="fade" data-aos-delay="200">
            <a href="#" class="btn btn-primary py-3 btn-block"><span class="h5">+</span> Post a Job</a>
          </div> -->
        </div>
        <?php 
        $i=0;
        while($row=$res->fetch_assoc())
        {
          $_SESSION['qry']=$sql_search;
            $loc="";
            $sql_location="select * from tbl_towns where id='".$row['live_in']."'";
            $res_location=$con->query($sql_location);
            if($row_location=$res_location->fetch_assoc())
            {
                $loc=$row_location['name'];

            }

            $job="";
            if($row['no_qualification']!="")
            {
            $sql_job="select * from tbl_industry where id in (".$row['no_qualification'].")";
            $res_job=$con->query($sql_job);
            while($row_job=$res_job->fetch_assoc())
            {
                if($job=="")
                {
                    $job=$row_job['industry_name'];
                }
                else
                {
                $job=$job."<br>".$row_job['industry_name'];
                }

            }
          }



        ?>

        <div class="row" data-aos="fade" id="result_div">
         <div class="col-md-12">

           <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">

              <div class="mb-4 mb-md-0 mr-5">
               <div class="job-post-item-header d-flex align-items-center">
                 <h2 class="mr-3 text-black h4"><?php echo $row['first_name']." ".$row['last_name']; ?></h2>
                 <?php
                 if($row['work_status']=="1")
                 {
                   ?>
               <img src="../img/one-token.png"  height="30" width="30">
                  <?php
                }
              
                 else  
                 {
                   ?>
                     <img src="../img/two-token.png"  height="30" width="30">
                
                 <?php
                 }
                 ?>

                 <?php
                 if($row['gov_support']=="Yes")
                 {
                   ?>
                   <label>
                  
                  <!-- &#9989; MAY COME WITH GOVERNMENT SUPPORT</label> -->
                 &nbsp;
                 <i class="fa fa-university" aria-hidden="true"></i>
                  MAY COME WITH GOVERNMENT SUPPORT

                 <?php
                 }
              
                 else 
                 {
                   ?>
                      <!-- <input type="checkbox" name="gov_app" readonly/> -->
                    <?php
                 }
                 ?>
              
               </div>
               
               <div><span class="fl-bigmug-line-big104"></span> <span><?php echo $loc; ?></span></div>
               <div class="job-post-item-body d-block d-md-flex">
                 <div class="mr-3"><span ></span> <a href="#"><?php echo $job; ?></a></div>
              
            
            
               </div>
               <div class="mr-3" style="margin-top:5px;"><span >Bio:</span> <a href="#"><?php echo $row['bio'];  ?></a></div> 
              <div class="mr-3"> 
           

               <?php
                 if($row['gender']=="male")
                 {
                   ?>
                   <i class="fa fa-male" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                  <?php
                 }
              
                 else 
                 {
                   ?>
                     <i class="fa fa-female" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                
                 <?php
                 }
                 ?>
              </div> 

               <?php 
                 if($row['visa_type']!="")
                 {
                     ?>
                  <div class="badge-wrap">
                
               
               
                
                 </div>
                </br> <span class="bg-primary text-white badge py-2 px-3"><?php echo $row['visa_type']; ?></span>
                 <?php 
                 }
                 ?>
              </div>

              <div class="ml-auto">
                <!-- <a href="#" class="btn btn-secondary rounded-circle btn-favorite text-gray-500"><span class="icon-heart"></span></a> -->
                <a href="../employer/login.php" class="btn btn-primary py-2">View</a>
              </div>
           </div>

         </div>
        </div>
        <?php
        $i++;


        }
        ?>

      

      </div>
    </div> 

<?php
  }

    ?>


  </div>
</div>

<?php include_once('../includes/external-pre-footer.php'); ?>
<?php include_once('../includes/external_footer.php'); ?>
</body>
</html>

<script>
// $(window).load(function() {
//   // $("html, body").animate({ scrollTop: $(document).height() }, 0);
//   $("html,body").scrollTop(3000);
// });


</script>