<?php 
session_start();
include_once('../includes/config.php');
include_once('../includes/external_header.php'); 

?>

<div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/login.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <p>FIND WORK IN AUSTRALIA</p>
            <h2>Jobseeker Registration</h2>                
         </div>
      </div>
    </div>
  </div>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
       
  <div class="jumbotron text-xs-center"  style="margin-top:20px;">
        <!-- <img src="images/thumbs.png" style="height:100px;width:100px;"/> -->
  <h4 class="display-4">Thank You!</h4>
  <p class="lead">Your jobseeker account is now ready. <strong></strong> Please click below to continue with your account.</p>
  <hr>
  <!-- <p>
    Having trouble? <a href="">Contact us</a>
  </p> -->
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="profile.php" role="button">Continue</a>
  </p>
</div>

         
        </div>
      </div>
    </div>
   
<?php include_once('../includes/external_footer.php'); ?>
