<?php 
session_start();
include_once('../includes/config.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if(isset($_POST['recover-submit']))
{
  $email=$_POST['email'];
  $temp_password = generateRandomString();
  
  // $sql_changepw="update tbl_jobseeker set password='".md5($temp_password)."' where email='".$email."'";
  //   if($con->query($sql_changepw)){
  //   }

  $sql="select * from tbl_jobseeker where email='".$email."'";
  $res=$con->query($sql);
  if($row=$res->fetch_assoc())
  {
    $sql_changepw="update tbl_jobseeker set password='".md5($temp_password)."' where email='".$email."'";
    $result=$con->query($sql_changepw);

    $contact_name = $row['first_name'].' '.$row['last_name'];
    $url = $_SERVER['REQUEST_URI']; //returns the current URL
    $parts = explode('/',$url);
    $dir = $_SERVER['SERVER_NAME'];
    for ($i = 0; $i < count($parts) - 1; $i++) {
    $dir .= $parts[$i] . "/";
    }
    $project_name=$dir;

    

    require '../vendor/autoload.php';
    require '../phpmailer/credential.php';

    $phpmail = new PHPMailer(true);

    // $mail->SMTPDebug = 4;                               // Enable verbose debug output

    $phpmail->isSMTP();                                      // Set mailer to use SMTP
    $phpmail->Host = SMTP_SERVER;  // Specify main and backup SMTP servers
    $phpmail->SMTPAuth = true;                               // Enable SMTP authentication
    $phpmail->Username = EMAIL;                 // SMTP username
    $phpmail->Password = PASS;                           // SMTP password
    $phpmail->SMTPSecure = ENCRYPTION;                            // Enable TLS encryption, `ssl` also accepted
    $phpmail->Port = SMTP_PORT;                                    // TCP port to connect to

    $phpmail->setFrom(EMAIL, 'Find Work Australia');
    $phpmail->addAddress($email);     // Add a recipient

    $phpmail->addReplyTo(EMAIL);
    $phpmail->isHTML(true);                                  // Set email format to HTML

    $phpmail->Subject = 'Find Work Australia: Password Recovery';
    $phpmail->Body    = '<b>Hi '. $contact_name. ',</b>
        <br>
        <br>
        <p>We have received a request to reset your password. Here is your temporary password: <b>'. $temp_password.' </b></p>
        <p>Please login to this <a href="'. $project_name .'login.php">link</a> and change your password.</p>
        <br>
        <p><b>If you did not request this changes, please email our support team.</p>
        <br>
        <br><br>
        <i><p>Thanks for your time,</p>
        <p>Find Work Australia Team</p></i>';

    if(!$phpmail->send()) {
        $_SESSION['error']= 'Message could not be sent. Mailer Error: ' . $phpmail->ErrorInfo;
    } else {
         $_SESSION['success']="Success! Temporary password has been sent to  ".$email.".";
    }


  }
  else
  {
      $_SESSION['error']= "This email is not registered with us! Please register first.";
  }
}

include_once('../includes/external_header.php'); 
?>

<!DOCTYPE html>
<html lang="en">

<body>
<div class="col-md-12" class="login_banner">
    <div class="row">        
      <img src="../img/login.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position: absolute;top:140px;">
            <p>FIND WORK AUSTRALIA</p>
            <h2>Jobseeker Forgot Password</h2>                
        </div>
      </div>
    </div>
  </div>


  <div class="col-md-12 login_back">
    <div class="container">
      <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 login">
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <form method="post">
              <div class="row">
              <?php
                    if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Error!</h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Success!</h4>
                            ".$_SESSION['success']."
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
                ?>
               
              <div class="col-md-11" style="text-align: center;">
                <h3><i class="fa fa-lock" style="color:#000; font-size:180px;"></i></h3>

                <h4 >Forgot Password?</h4>
                <h6>We will email you a temporary password</h6>
                  <input class="form-control" type="text" id="email" name="email" placeholder="Email Address" >

                  <input style="margin-top:10px;" type="submit" name="recover-submit" class="btn btn-info" value="Reset Password" >
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>

  </div>
</div>
</div>

<?php include_once('../includes/external_footer.php'); ?>



</body>
</html>
