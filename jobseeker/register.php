<?php 
session_start();
include_once('../includes/config.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$drive_car=$looking_work=$gender=$qualification=$discription=$bio=$language=$no_qualification=$no_qualification_other=$no_qualification_time_in_role=$no_qualification_describe_work_experience=$no_qualificationany=""; 

$some_qualification=$some_qualification_other=$some_qualification_time_in_role=$some_qualification_describe_work_experience="";
$university_qualification=$university_qualification_other=$university_qualification_time_in_role=$university_qualification_describe_work_experience="";
$work_status=$filename=$move_job=$mob_later=$gov_support="";


if(isset($_POST['submitRegister']))
{
  $secretKey = '6LeiFLEZAAAAAPhmVPm907CtOOBxRFwbIh1Cgy9R';
  $token = $_POST['g-token'];
  $ip = $_SERVER['REMOTE_ADDR'];

  $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$token."&remoteip=".$ip;
  $request = file_get_contents($url);
  $response = json_decode($request);

 $first_name=addslashes($_POST['first_name']);
 $prefered_name=  addslashes($_POST['prefered_name']);
 $last_name=  addslashes($_POST['last_name']);
 $password= addslashes($_POST['password']);
 $mobile_no=  addslashes($_POST['mobile_no']);
 $age_bracket=  addslashes($_POST['age_braket']);
 $work_status=addslashes($_POST['work_status']);
 if(isset($_POST['gender']))
 {
 $gender= addslashes($_POST['gender']);
 }
 if(isset($_POST['move_job']))
  {
    $move_job= 1;
  }
  else
  {
    $move_job= 0;
  }
  if(isset($_POST['mob_later']))
  {
    $mob_later= 1;
    $mobile_no="";
  }
  else
  {
    $mob_later= 0;
  }
 
  $live_in= addslashes($_POST['live_in']);
  $stay_months_from= addslashes($_POST['stay_months_from']);
  $stay_months_to= addslashes($_POST['stay_months_to']);
  $expire_date= addslashes($_POST['expire_date']);
  $visa_type= addslashes($_POST['visa_type']);


  $email= addslashes($_POST['email']);
  $bio= addslashes($_POST['bio']);
  $language= addslashes($_POST['languages']);
 

 $image=$_POST['file_location'];
 $image1=basename($image);
 $filename=$_FILES['filename']['name'];
if(isset($_POST['no_qualification']))
{
  $no_qualification=implode(",",$_POST['no_qualification']);
}

if(isset($_POST['common_job']))
{
  $common_job=implode(",",$_POST['common_job']);
}

 
if(isset($_POST['qualification']))
{
 $qualification=$_POST['qualification'];
 if($qualification="yes")
 {
 $discription=addslashes($_POST['discription']);
 }
 else{
  $discription="";
}
}
if(isset($_POST['drive_car']))
{
 $drive_car=addslashes($_POST['drive_car']);
}
if(isset($_POST['gov_support']))
{
 $gov_support=addslashes($_POST['gov_support']);
}
if(isset($_POST['works']))
{
  $work=implode(",",$_POST['works']);
}


  $sql="insert into tbl_jobseeker(first_name, last_name, prefered_name, email, password, australian_mob, mob_later, gender, work_status, type_work,  languages_spoken, bio, age_braket, visa_type, expire_date, drive_car, move_job, live_in, stay_months_from, stay_months_to, no_qualification, gov_support, image, resume) values ('".$first_name."', '".$last_name."', '".$prefered_name."', '".$email."', '".md5($password)."', '".$mobile_no."', '".$mob_later."', '".$gender."', '".$work_status."', '".$work."', '".$language."', '".$bio."', '".$age_bracket."', '".$visa_type."', '".$expire_date."', '".$drive_car."', '".$move_job."', '".$live_in."', '".$stay_months_from."', '".$stay_months_to."', '".$no_qualification."', '".$gov_support."', '".$image1."', '".$filename."') ";

 if($con->query($sql))
{
  $id=mysqli_insert_id($con);



    require '../vendor/autoload.php';
    require '../phpmailer/credential.php';

    $url = $_SERVER['REQUEST_URI']; //returns the current URL
    $parts = explode('/',$url);
    $dir = $_SERVER['SERVER_NAME'];
    for ($i = 0; $i < count($parts) - 1; $i++) {
    $dir .= $parts[$i] . "/";
    }
    $project_name="http://".$dir;

    $phpmail = new PHPMailer(true);

    // $mail->SMTPDebug = 4;                               // Enable verbose debug output

    $phpmail->isSMTP();                                      // Set mailer to use SMTP
    $phpmail->Host = SMTP_SERVER;  // Specify main and backup SMTP servers
    $phpmail->SMTPAuth = true;                               // Enable SMTP authentication
    $phpmail->Username = EMAIL;                 // SMTP username
    $phpmail->Password = PASS;                           // SMTP password
    $phpmail->SMTPSecure = ENCRYPTION;                            // Enable TLS encryption, `ssl` also accepted
    $phpmail->Port = SMTP_PORT;                                    // TCP port to connect to

    $phpmail->setFrom(EMAIL, 'Find Work Australia');
    $phpmail->addAddress($email);     // Add a recipient

    $phpmail->addReplyTo(EMAIL);
    $phpmail->isHTML(true);                                  // Set email format to HTML

    $phpmail->Subject = 'Welcome to Find Work Australia Jobseeker: '. $first_name. ' '.$last_name;
    $phpmail->Body    = '<b>Hi '. $first_name. ' '.$last_name. ',</b>
        <br>
        <h4><b>Thank you for signing up! Welcome to Find Work Australia.</b></h4>
        <br>
        <p>Your account is now ready. Please click this <a href="'. $project_name .'profile.php">link</a> to login and manage your account.</p>
        <br><br>
        <i><p>Thanks for your time,</p>
        <p>Find Work Australia Team</p></i>

        ';

  if($response->success){

    if(!$phpmail->send()) {
    $_SESSION['error']= 'Message could not be sent. Mailer Error: ' . $phpmail->ErrorInfo;
    } 
    else {
        $_SESSION['success']='Message has been sent. Someone will contact you shortly.';
    }

  }

  else{
    $_SESSION['error']='Captcha Validation Failed.';
  }
 header('location:register_success.php');
  

  if($image)
        {
          
		    if (!file_exists('../admin/uploads/'.$id.'/image'))
                mkdir('../admin/uploads/'.$id.'/image', 0777, true);
            // if(move_uploaded_file($_FILES['upload_image']['tmp_name'], '../admin/uploads/'.$id.'/image' . $_FILES['upload_image']['name']))
            // Your file
$file = $image;

// Open the file to get existing content
 $data = file_get_contents($file);
 $image1=basename($image);
// New file
 $new = '../admin/uploads/'.$id.'/image/'.$image1;

// Write the contents back to a new file
if(file_put_contents($new, $data))
		    {
         // $imageName = $targetPath;
        //remove
        unlink($file);
		    }
		    else
		    {
          header('location:register_success.php');
          $_SESSION['success']= "";
          
          }
        }
        if($filename)
        {
		    if (!file_exists('../admin/uploads/resume/'.$id))
                mkdir('../admin/uploads/resume/'.$id, 0777, true);
            if(move_uploaded_file($_FILES['filename']['tmp_name'], '../admin/uploads/resume/'.$id.'/' . $_FILES['filename']['name']))
		    {
       //  header('location:success_message.php');
          header('location:register_success.php');
           $_SESSION['success']= "";
           
		    }
		    else
		      {
            $_SESSION['error']= "Error";  
          }
        }

        $_SESSION['jobseeker_id']=$id;
        $_SESSION['jobseeker_name']=$first_name;
       
          
          $_SESSION['success']= "";
          //header('location:register_success.php');
       
}
else
{
  $_SESSION['error']= "Error!!! Please try again ";
}


}
include_once('../includes/external_header.php'); 

?>

<style type="text/css">

.industy_tabs {
  margin-top:130px;
}
.industy_tabs h4{
    margin: 0px 0px;
}
.industy_tabs .panel-title a{
  font-size: 13px;
    font-weight: 600;
     color: #fff;
}
.industy_tabs .panel-body {
    background-color: #0c0c0c69;
    border-color: #8696b7;
    padding: 10px 15px;
    text-align: left;
        color: #eee;
    font-size: 14px;
}
.panel-default>.panel-heading {
  color: #fff;
  background-color: #0c0c0c69;
    border-color: #8696b7;
  padding: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
  text-align: left;
}

.panel-default>.panel-heading a:after {
  /*content: "";*/
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
  /*background-color: #eee;*/
}

.panel-default>.panel-heading a[aria-expanded="true"]:after {
  content: "\2212";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-default>.panel-heading a[aria-expanded="false"]:after {
  content: "\002b";
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}
</style>

<style>
.emp_profile h1,h2,h3,h4,h5,h6{
    
    font-family: 'Comfortaa', cursive;
    font-weight: 600;
}
p {
  font-family: 'Comfortaa', cursive;
    font-weight: 600;
}

.section-header h3 {
  font-family: 'Comfortaa', cursive;
     font-size: 42px!important;
    text-align: center;
    font-weight: 500;
    position: relative;
}

.numberCircle {
    width: 100px;
    line-height: 100px;
    border-radius: 50%;
    text-align: center;
    font-size: 32px;
    border: 3px solid #666;
    background:#666;


}

</style>

<!DOCTYPE html>
<html lang="en">

<body>

<!-- jQuery for customized datepicker -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Bootstrap for customized datepicker -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >


<script src="https://www.google.com/recaptcha/api.js?render=6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK"></script>


<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Upload & Crop Image</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 text-center">
              <div id="image_demo" style="width:350px; margin-top:30px"></div>
            </div>
            <div class="col-md-4" style="padding-top:30px;">
                <br/>
                <br/>                    
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success crop_image">Crop & Upload Image</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </div>
  </div>
</div>




  <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/Jobseeker-top.jpg" height="400" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <p>FIND WORK IN AUSTRALIA</p>
            <h2 style="font-family: 'Comfortaa', cursive;">Create Your Free Profile</h2>
            <div>
              <a href="login.php" class="btn-get-started scrollto">LOGIN</a>
            </div>
         </div>

      </div>
    </div>

      <main id="main"> 
    <section id="about">
      <div class="container">
        <header class="section-header">
            <h3 style="color:red!important;">IMPORTANT!</h3>
            <p>Your free Jobseeker profile must be kept up to date!
            <br>If you are successfully employed, please ensure your status is set to either ’employed’ or ’employed but looking for something new’.
            <br><br>Failure to update your profile may result in a poor rating and/or your account suspended.</p>
            <div class="col-md-12" style="text-align: center;">
             <a href="#jobseeker-step" class="btn-services scrollto abt_btn">TELL ME MORE ABOUT THIS</a>
           </div>
        </header>
        </div>
      </section>
  </main>

  </div>


  <div class="col-md-12 login_back">
  <div class="container">

    <div class="row">
      <div class="col-md-2"></div>

        <div class="col-md-8 login regist_head">
                    <?php
                      if(isset($_SESSION['error'])){
                        echo "
                            <div class='row'>
                            <div class='col-md-2'></div>
                              <div class='col-md-8 alert alert-danger alert-dismissible' style='margin-top:30px;'>
                              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                              <h4>Error!</h4>
                              ".$_SESSION['error']."
                              </div>
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                        <div class='row'>
                        <div class='col-md-2'></div>
                            <div class='col-md-8 alert alert-success alert-dismissible' style='margin-top:30px;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Success!</h4>
                            ".$_SESSION['success']."
                           , Please <a href='login.php'> Login </a> to continue..</div>
                           </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
                    ?>

          <div class="row" id="register-top">
            <div class="col-md-1"></div>
              <div class="col-md-10">
               
                <div class="row">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-12">
                      <h4>Contact Details</h4>
                      <input type="hidden" id="g-token" name="g-token"/>
                      <label class="reg_label1">First Name <span class="star">*</span></label>
                        <input type="text" id="first_name" name="first_name" class="reg_box" autocomplete="off" required />

                      <label class="reg_label">Last Name <span class="star">*</span></label>
                        <input type="text"  id="last_name" name="last_name" class="reg_box" autocomplete="off" required />

                      <label class="reg_label">Preferred name to be called</label>
                        <input type="text" id="prefered_name" name="prefered_name" class="reg_box" autocomplete="off" />

                      <div>
                        <label class="reg_label">Email <span class="star">*</span></label>
                          <input type="email" id="email" name="email" onchange="check(this.value)" class="reg_box" autocomplete="off" required />
                            <small id="val" style="color:red;"></small>
                      </div>

                      <label class="reg_label">Australian Phone Number</label>
                        <input type="text" id="mobile_no" name="mobile_no" class="reg_box" autocomplete="off"/>
                        <div class="form-group" style="margin-bottom: 0px;">
                            <input type="checkbox" id="mob_later" name="mob_later"  >
                            <label for="mob_later" class="reg_label"> I will do this later</label>
                            <div id="clr" style="display:none;">

                            <a href="https://www.findworkaustralia.com.au/mobile-number" class="aus_num">Get my australian Phone number</a>
                            </div>
                        </div>

                      <label class="reg_label">Password <span class="star">*</span></label>
                        <input type="text" style="-webkit-text-security: disc;" id="password" name="password" class="reg_box" autocomplete="off" required />

                      <div class="row">
                        <p class="col-md-2" style="margin-top: 5px;">
                          <input type="radio" id="test1"  name="gender"  value="Male">
                          <label for="test1" class="reg_label">Male</label>
                        </p>
                        <p class="col-md-3" style="margin-top: 5px;">
                          <input type="radio" id="test2" name="gender"  value="Female">
                          <label for="test2" class="reg_label">Female</label>
                        </p>
                      </div>

                    <div>
                      <h4>About you</h4>
                      <label class="reg_label">Status <span class="star">*</span></label>
                      <select class="reg_box" name="work_status" id="work_status" onchange="status_warning(this.value);" required>
                        <option value="">--Select--</option>
                        <option value="0">Ready to start work</option>
                        <option value="1">Currently employed and looking for a change</option>
                        <option value="2">Employed</option>   
                      </select>
                      <small id="workstatus_warning" style="color:red;"></small>
                    </div>

                    <label class="reg_label">Types of Work <span class="star">*</span></label>

                    <div class="row">

                    <div class="col-md-2 work_img nopad" style="text-align: center;">
                      <label class="image-checkbox">
                        <img class="img-responsive" style="width:75px;" src="../img/r_icon1.png" />
                      <p>Any</p>
                      <input type="checkbox" name="works[]" value="Any" />
                        <i class="fa fa-check hidden" style="right: 8px;"></i>
                      </label>
                    </div>

                    <div class="col-md-2 work_img nopad" style="text-align: center;">
                      <label class="image-checkbox"  >
                        <img class="img-responsive" style="width:75px;" src="../img/r_icon2.png" />
                      <p>full time</p>
                      <input type="checkbox" name="works[]" value="Full Time" />
                        <i class="fa fa-check hidden" style="right: 8px;"></i>
                      </label>
                    </div>

                    <div class="col-md-2 work_img nopad" style="text-align: center;">
                      <label class="image-checkbox"  >
                        <img class="img-responsive" style="width:75px;" src="../img/r_icon4.png" />
                      <p>Part time</p>
                      <input type="checkbox" name="works[]" value="Part Time" />
                        <i class="fa fa-check hidden" style="right: 8px;"></i>
                      </label>
                    </div>

                    <div class="col-md-2 work_img nopad" style="text-align: center;">
                      <label class="image-checkbox"  >
                        <img class="img-responsive" style="width:75px;" src="../img/r_icon3.png" />
                      <p>Weekend</p>
                      <input type="checkbox"   name="works[]" value="Weekend" />
                        <i class="fa fa-check hidden" style="right: 8px;"></i>
                      </label>
                    </div>

                    <div class="col-md-2 work_img nopad" style="text-align: center;">
                      <label class="image-checkbox"  >
                        <img class="img-responsive" style="width:75px;" src="../img/r_icon5.png" />
                      <p>night shift</p>
                      <input type="checkbox" name="works[]" value="nightshift" />
                        <i class="fa fa-check hidden" style="right: 8px;"></i>
                      </label>
                    </div>

                    </div>

                    <label class="reg_label">Languages Spoken <span class="star">*</span></label>
                      <input type="text" id="languages" name="languages"  class="reg_box" autocomplete="off" required />

                    <label class="reg_label">Bio</label>
                    <textarea class="reg_box" rows="5" cols="20" name="bio" id="bio" maxlength = "100"></textarea>

                    <label class="reg_label">How old are you? <span class="star">*</span></label>
                    <select class="reg_box" id="age_braket" name="age_braket">
                      <option value="18-25">18 - 25</option>
                      <option value="25-30">25 - 30</option>
                      <option value="35-40">35 - 40</option>
                      <option value="40-50">40 - 50</option>
                      <option value="50+">50+</option>  
                    </select>

                    <label class="reg_label">What visa do you hold in Australia? <span class="star">*</span></label>
                    <select class="reg_box"  id="visa_type" name="visa_type" required>
                          <option value="">--Select One--</option> 
                          <option value="Australian Resident">Australian Resident - no visa required</option>
                          <option value="Business Visa">Business Visa</option>
                          <option value="Skilled Migration">Skilled Migration</option>
                          <option value="Partner Visa">Partner Visa</option>
                          <option value="Resident Return Visa">Resident Return Visa</option>
                          <option value="Family Visa">Family Visa</option>
                          <option value="Student Visa">Student Visa</option>
                          <option value="Other">Other</option> 
                    </select>

                    <label class="reg_label">When does it expire? </label>
                      <input placeholder="DD/MM/YYYY" name="expire_date" type='text' class="form-control" id='dp-expire' autocomplete="off" style='width: 300px;' />

                    <label class="reg_label">Do you have a full motor vehicle license? <span class="star">*</span></label>
                    <div class="row">
                      <p class="col-md-2" style="margin-top: 0px;">
                        <input type="radio"  name="drive_car" id="test3" value="Yes">
                        <label for="test3" class="reg_label">Yes</label>
                      </p>
                      <p class="col-md-3" style="margin-top: 0px;">
                        <input type="radio"  name="drive_car" id="test4" value="No">
                        <label for="test4" class="reg_label">No</label>
                      </p>
                    </div>

                    <h4>Location</h4>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <input type="checkbox" name="move_job" id="cb_post" >
                        <label for="cb_post" class="reg_label"> I will move to the job</label>
                    </div>
                   

                    <div id="hiddenDiv" style="display:show">
                    <label class="reg_label">What city do you live in or plan to live in? </label>
                    <select class="reg_box" id="live_in" name="live_in">
                          <option value="">--Select One--</option> 
                          <?php
                          $sql="select * from tbl_towns";
                          $res=$con->query($sql);
                          while($row=$res->fetch_assoc())
                          {

                          ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                          <?php
                          }
                          ?>
                    </select>
                    <label class="reg_label" style="margin-top:28px;">How long do you intend to stay in this area? </label>
                    <div class="row">
                      <div class="col-md-6">
                        <label class="reg_label1">From </label>
                         <!-- <input type="date" id="datepicker" name="stay_months_from" class="reg_box"> -->
                         <input placeholder="DD/MM/YYYY" name="stay_months_from" type='text' class="form-control" id='dp-stay-from' autocomplete="off" style='width: 300px;' >
                      </div>
                      <div class="col-md-6">
                        <label class="reg_label1">To </label>
                        <input placeholder="DD/MM/YYYY" name="stay_months_to" type='text' class="form-control" id='dp-stay-to' autocomplete="off" style='width: 300px;' >
                         <!-- <input type="date"  id="datepicker" name="stay_months_to" class="reg_box"> -->
                      </div>
                    </div>
                    </div>

                  <script type="text/javascript">
                    $(document).ready(function(){

                     $('#dp-expire').datepicker({ format: 'dd/mm/yyyy', autoclose: true });
                     $('#dp-stay-from').datepicker({ format: 'dd/mm/yyyy', autoclose: true }); 
                     $('#dp-stay-to').datepicker({ format: 'dd/mm/yyyy', autoclose: true }); 
                    });
                  </script>

                  <h4 style="margin-top: 35px;margin-bottom: 4px;">I'm Looking for a job in :</h4>
                    <label class="reg_label1">Common Categories - Select as many as you like</label>

                    <div class="row">
                      <?php
                      $sql_noq="Select * From tbl_industry where status=2";
                      $res_noq=$con->query($sql_noq);
                      while($row_noq=$res_noq->fetch_assoc())
                      {
                        ?>
                        <div class="col-md-2 categories_img nopad" style="text-align: center;">
                          <label class="image-checkbox" id="cany" onclick="show_individual('any')">
                            <img class="img-responsive" height="auto" width="100%" style="max-width: 100px;"  src="../img/<?php echo $row_noq['id']; ?>.png" />
                          <input type="checkbox" id="any" name="no_qualification[]" value="<?php echo $row_noq['id']; ?>" />
                            <i class="fa fa-check hidden" style="right: 4px;"></i>
                          </label>
                        </div>
                      <?php
                         }
                      ?> 
                    </div>
                    
                    <label class="reg_label" for="experience">Additional Industries</label>
                      <div class="row">
                      <?php
                        $sql="Select * From tbl_industry where status=1 ORDER BY industry_name";
                        $res=$con->query($sql);
                        while($row=$res->fetch_assoc())
                        {
                      ?>

                       <div class="col-md-6">
                        <label class="label_text">
                          <input type="checkbox" name="no_qualification[]" value="<?php echo $row['id']; ?>">
                          <span class="checkmark"></span>
                          <label class="checkbox_text"><?php echo $row['industry_name']; ?></label>
                        </label>
                      </div>

                      <?php 
                        }
                      ?>

                      </div>

                      <label class="reg_label">MAY COME WITH GOVERMENT SUPPORT ?</label>
                        <div class="row">
                            <p class="col-md-2" style="margin-top: 0px;">
                              <input type="radio" id="test5" name="gov_support" value="Yes">
                              <label for="test5" class="reg_label">Yes</label>
                            </p>
                            <p class="col-md-3" style="margin-top: 0px;">
                              <input type="radio" id="test6" name="gov_support" value="No">
                              <label for="test6" class="reg_label">No</label>
                            </p>
                        </div>

                      <label  class="reg_label1">Select Profile Image</label>
                      <div class="custom-file-upload">                   
                          <input type="file" name="upload_image" id="upload_image"/>
                       
                          <br />
                          <img src="" id="uploaded_image" class="img-thumbnail" />

                          <input type="hidden" name="file_location" id="file_location">
                      </div>

                      <label  class="reg_label">Upload cover letter or Resume</label>
                        <div class="custom-file-upload">                     
                            <input type="file" id="filename" name="filename"  />
                        </div>
                        <p class="size">Maximum size : 1MB</p>
                        <!-- <input type="submit" name="submit" class="login_btn"> -->
                      <input type="submit" value="Submit" name="submitRegister" class="login_btn" />



                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>

        </div>
              

      </div>

  <main id="main"> 
    <section id="about">
      <div class="container" id="jobseeker-step">
        <header class="section-header">
            <h3>How does it work?</h3>
            <h4 style="text-align:justify;font-family: 'Comfortaa', cursive;">Find Work Australia lets you create your free profile for employers who are looking for workers.<br>
            Your information will be kept safe and when an employer runs a search, a preview of your profile will be shown to them. It is important to keep your information up to date as the employer will be paying to unlock your details.
            <br><br>
            You can set your profile to “Ready to start work”, “Employed but looking for a change” and “Employed”.
            When you change your status to ‘Employed’ your profile will be hidden until you are ready to be found again.</h4>
            <ul class="progressbar">
              <li><p>Create your profile
              </p>
              <h5>A small preview of this will appear when an employer does a search.</h5>
              </li>

              <li><p>Get a phone number
              <h5>Have your Australian mobile number ready to receive calls from your employer – we can help you with this!</h5></p>
              
              </li>
              <li><p>Keep your profile updated</p>
              <h5>Updating profile will makes you more competent.</h5>
              </li>
              <li><p>Get hired</p>
              <h5>Receive a phone call and/or email from interested employers!</h5>
              </li>
            </ul>



        </header>


        </div>
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
             <a href="#register-top" class="btn-services scrollto abt_btn">CREATE YOUR PROFILE</a>
           </div>
      </section>

  </main>



</div>


<div class="col-md-12 login_back">
<div class="col-md-12 industries">
  <div class="container">
    <h3>Popular Industries</h3>
    <div class="row">
      <div class="col-md-3">
        <img src="../img/icon1.png" style="height: 126px;width: 90px;">
        <h4>BARISTA | CAFE</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon2.png" style="height: 126px;width: 90px;">
         <h4>BAR | RESTAURANT</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon3.png" style="height: 126px;width: 90px;">
         <h4>FRUIT PICKING</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon4.png" style="height: 126px;width: 90px;">
         <h4>RETAIL</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon5.png" style="height: 126px;width: 90px;">
         <h4>CONSTRUCTION</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon6.png" style="height: 126px;width: 90px;">
         <h4>CUSTOMER | CALL CENTRE</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon7.png" style="height: 126px;width: 90px;">
         <h4>CLEANING</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon8.png" style="height: 126px;width: 90px;">
         <h4>FACTORY</h4>
      </div>
    </div>

  <!-- <div class="col-md-12 "> -->
      <!-- <div class="container"> -->

  </div>
</div>
</div>

<div class="col-md-12 banner3">
  <div class="row">   
      <div class="col-md-12 banner_text">
        <div class="container">
     

  <div class="industy_tabs panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="row">
        <div class="col-md-6">
          <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:black;">
              CAFE | BARISTA 
              </a>
            </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <p>Working in the hospitality industry in Australia is great way to work and travel. Cafe’s are located in the key tourist locations and many have high and low seasons, so there are plenty of hiring opportunitues.</p>
                <p>You can work as a Barista without formal qualifications. We recommend you know the vocabularly extensively used in cafe’s and restaurants.</p>
              </div>
            </div>
          </div>
          <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo" style="color:black;">
                BAR | RESTAURANT
              </a>
            </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <p>In Australia you need a RSA certificate to server alcohol. Gaining this certificate is easy and can be completed online.</p>

                <p>To get an idea of pay rates, please <a target="_blank" href="https://www.payscale.com/research/AU/Job=Bar_Attendant/Hourly_Rate">click here.</a></p>
              </div>
            </div>
          </div>
           <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo" style="color:black;">
                FRUIT PICKING
              </a>
            </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                  <h5>When & where to go for fruit picking jobs Australia</h5>
                  <p>Before you are going to look for a fruit picking job, it would be useful to determine where you would like to go to. There are many possibilities of different fruit picking jobs as Australia is a huge country with numerous different landscapes and weather conditions.</p>
                  <h5>Queensland</h5>
                  <div class="row">
                    <p class="col-md-8">Queensland grows 1/3 of the nation’s fruit and vegetables. Farmers hire migrant workers, fruit & vegetables pickers and other (experienced) farmhands. Farm work varies across the Queensland region – the farmers grow sugar cane, beans, seasonal fruits and vegetables, also cattle, cotton and wool.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit1.png" height="auto" width="100%">
                    </div>
                  </div>
                  <p>In Tully they grow many bananas. Tully is located 180 kilometres south of Cairns and is known for being the wettest place in Australia, receiving an average of 4134 mm of rain per year. In the Tully area there are lots of beautiful beach towns such as Mission Beach and Bingal Bay. The Tully River is also famous among people who are crazy about rafting!</p>

                  <p>From Stanthorpe in the south to the Atherton Tablelands in the far North; more than 120 types of fruit and vegetable are picked and packed in the state every year all year round so new and motivated workers are needed to fill a variety of fruit picking and harvest jobs.</p>
                  <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Feb – Mar</td>
<td>Pears / Apples</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Rock Melon</td>
<td>St George</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Ginger</td>
<td>Sunshine Coast</td>
</tr>
<tr>
<td>Mar – Dec</td>
<td>Vegetables</td>
<td>Bundaberg</td>
</tr>
<tr>
<td>Apr – Jun</td>
<td>Citrus</td>
<td>Mundubbera</td>
</tr>
<tr>
<td>Apr – Oct</td>
<td>Various</td>
<td>Lockyer Valley</td>
</tr>
<tr>
<td>Apr – Nov</td>
<td>Beans</td>
<td>Mary Valley</td>
</tr>
<tr>
<td>Apr – Dec</td>
<td>Vegetables / Tomatoes</td>
<td>Bowen / Ayr</td>
</tr>
<tr>
<td>May – Oct</td>
<td>Broccoli</td>
<td>Toowoomba</td>
</tr>
<tr>
<td>May – Dec</td>
<td>Sugar Cane</td>
<td>Ayr / Ingham / Innisfail</td>
</tr>
<tr>
<td>Jul – Sep</td>
<td>Ginger</td>
<td>Sunshine Coast</td>
</tr>
<tr>
<td>Jul – Dec</td>
<td>Onions</td>
<td>Lockyer Valley</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Mangoes / Lychees /<br> Avocados / Bananas</td>
<td>Mareeba</td>
</tr>
<tr>
<td>Nov – Jan</td>
<td>Plums</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Nov – Jan</td>
<td>Cotton</td>
<td>Goodiwindi / St George</td>
</tr>
<tr>
<td>Oct – Jan</td>
<td>Peaches</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Nov – Mar</td>
<td>Cotton</td>
<td>Toowoomba / Milleran</td>
</tr>
<tr>
<td>Dec – Jan</td>
<td>Bananas / Sugar</td>
<td>Innisfail / Tully</td>
</tr>
<tr>
<td>Dec – Mar</td>
<td>Various</td>
<td>Stanthorpe</td>
</tr>
</tbody>
</table>
              <h5>New South Wales</h5>
                  <div class="row">
                    <p class="col-md-8">New South Wales is one of best places to find seasonal farm jobs in Australia. The main produce in New South Wales include sheep, cattle, pigs, hay, apples, cherries, pears, legumes, maize, nuts, wool, wheat, and rice. Farmhands, migrant workers, fruit pickers, and vegetable pickers may all find work across New South Wales in fields and on ranches. November to April is the busiest harvest period which peaks in February. However, the harvest differs per crop.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit2.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>For example – Griffith is a thriving agricultural and industrial city boasting the third largest population in the Riverina region and many well regarded wineries. Next to grape picking it’s also possible to do orange picking or onion picking at different times of the year. Griffith is located approximately 600 kilometres west of Sydney. This country town is well known for its food, wine and festivals.</p>
                  
                  <table>
                  <tbody>
                  <tr>
                  <td><strong>Harvest</strong></td>
                  <td><strong>Crop</strong></td>
                  <td><strong>Location</strong></td>
                  </tr>
                  <tr>
                  <td>Jan – Mar</td>
                  <td>Stonefruit</td>
                  <td>Young</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Prunes</td>
                  <td>Young</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Pears</td>
                  <td>Orange</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Grapes</td>
                  <td>Leeton / Hunter Valley</td>
                  </tr>
                  <tr>
                  <td>Feb – Apr</td>
                  <td>Apples</td>
                  <td>Orange</td>
                  </tr>
                  <tr>
                  <td>Mar – Apr</td>
                  <td>Grapes</td>
                  <td>Tumbarumba</td>
                  </tr>
                  <tr>
                  <td>Mar – May</td>
                  <td>Apples</td>
                  <td>Batlow</td>
                  </tr>
                  <tr>
                  <td>Mar – Jun</td>
                  <td>Cotton Picking</td>
                  <td>Narrabri / Mooree</td>
                  </tr>
                  <tr>
                  <td>Sep – Oct</td>
                  <td>Asparagus</td>
                  <td>Gundagai</td>
                  </tr>
                  <tr>
                  <td>Sep – Dec</td>
                  <td>Asparagus</td>
                  <td>Cowra</td>
                  </tr>
                  <tr>
                  <td>Sep – Apr</td>
                  <td>Oranges</td>
                  <td>Griffith</td>
                  </tr>
                  <tr>
                  <td>Nov – Dec</td>
                  <td>Cherries</td>
                  <td>Young / Orange</td>
                  </tr>
                  <tr>
                  <td>Nov – Apr</td>
                  <td>Oranges</td>
                  <td>Lecton</td>
                  </tr>
                  <tr>
                  <td>Dec – Jan</td>
                  <td>Onions</td>
                  <td>Griffith</td>
                  </tr>
                  <tr>
                  <td>Dec – Mar</td>
                  <td>Stone Fruit</td>
                  <td>Tumut / Batlow</td>
                  </tr>
                  <tr>
                  <td>Dec – Apr</td>
                  <td>Blueberries</td>
                  <td>Tumbarumba</td>
                  </tr>
                  </tbody>
                  </table>

                  <h5>Victoria</h5>
                  <div class="row">
                    <p class="col-md-8">November to April (peaking in February) is the main season for fruit and vegetable picking jobs in Victoria. There is plenty of work in the central northern areas around Shepparton. The Murray River area is also a great area to look for work; places such as Mildura and Swan Hill often require fruit picking / harvest workers. Main harvests in these areas include orchard fruits, tomatoes, tobacco, grapes and soft fruits.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit3.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>Fruit picking in Cobram – “Cobram is a large fruit growing area situated on the Murray River just 250 kilometres north of Melbourne. This region is well known for its irrigated fruit and dairy with large areas of stone fruit varieties. The small farm area expanded after WWII , when a significant number of Italian immigrants arrived and established themselves in the farming community.” –<a target="_blank" href="http://workstay.com.au/fruit-picking-cobram-victoria"> http://workstay.com.au/fruit-picking-cobram-victoria</a></p>
                    <p>Fruit picking Yarra Valley in Victoria – “This valley is about 40 kilometres north east of Melbourne. A large variety of cool climate crops have traditionally been grown in the Valley and there are more than 80 vineyards and wineries in the Valley with many different selections of wines.” –<a target="_blank" href="https://jobsearch.gov.au/"> https://jobsearch.gov.au/</a></p>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Tomatoes</td>
<td>Ardmona / Shepparton / Rochester</td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Pears / Peaches / Apples</td>
<td>Ardmona / Shepparton / Cobram</td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Tobacco</td>
<td>Ovens / King &amp; Kiewa Valleys</td>
</tr>
<tr>
<td>Feb – Mar</td>
<td>Grapes</td>
<td>Lake Boga / Swan Hill / Nyah West</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Ovens / King &amp; Kiewa Valleys</td>
</tr>
<tr>
<td>Mar – Apr</td>
<td>Apples</td>
<td>Buckland Valley / Stanley / Wandilong / Red Hill / Main Range</td>
</tr>
<tr>
<td>Sep – Nov</td>
<td>Asparagus</td>
<td>Dalmore</td>
</tr>
<tr>
<td>Oct – Dec</td>
<td>Strawberries</td>
<td>Silvan</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Cherries</td>
<td>Boweya / Glenrowan / Wagandary</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Cherries / Berries</td>
<td>Wandin / Silvan</td>
</tr>
<tr>
<td>Nov – Dec</td>
<td>Tomato Weeding</td>
<td>Echuca / Rochester</td>
</tr>
</tbody>
</table>
              <h5>Tasmania</h5>
                  <div class="row">
                    <p class="col-md-8">Tasmania is all about apples. It grows almost a fifth of the apples in Australia, approximately 55,000 tonnes every year. The main areas are in the Huon district South of Hobart. Tasmania has a relatively short fruit picking season, generally from December to the end of May, fruit picking and harvest jobs are available all around Tasmania. As well as picking apples there are also pears, stone fruits, hops, grapes, berries and much more.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit4.png" height="auto" width="100%">
                    </div>
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Feb</td>
<td>Scallop Splitting</td>
<td>Bicheno</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Huon / Tamar Valley</td>
</tr>
<tr>
<td>Mar – Apr</td>
<td>Hops</td>
<td>Scottsdale / New Norfolk / Devenport</td>
</tr>
<tr>
<td>Mar – May</td>
<td>Apples</td>
<td>Hunter Valley / Tasman Peninsula / West Tamar</td>
</tr>
<tr>
<td>Dec – Jan</td>
<td>Soft Fruit</td>
<td>Channel District / Huon / Kingborough / Derwent Valley</td>
</tr>
</tbody>
</table>
                 <h5>South Australia</h5>
                  <div class="row">
                    <p class="col-md-8">fruit and vegetable picking jobs all year around e.g. picking citrus and soft fruits such as raspberries and strawberries around the Riverland area.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit5.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>February to April work is available around The Barossa Valley picking and packing the grapes.</p>

<table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Mar</td>
<td>Dried Fruits</td>
<td>Riverland</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes &amp; Peaches</td>
<td>Riverland</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Southern Vales / Barossa Valley</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Apples &amp; Pears</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Feb – Aug</td>
<td>Brussel Sprouts</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Jun – Aug</td>
<td>Oranges</td>
<td>Riverland</td>
</tr>
<tr>
<td>Jun – Sep</td>
<td>Pruning</td>
<td>Riverland</td>
</tr>
<tr>
<td>Sep – Jan</td>
<td>Oranges</td>
<td>Riverland</td>
</tr>
<tr>
<td>Oct – Feb</td>
<td>Strawberries</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Dec – Feb</td>
<td>Apricots</td>
<td>Riverland</td>
</tr>
</tbody>
</table>
                  <h5>Western Australia</h5>
                  <div class="row">
                    <p class="col-md-8">Fruit picking and harvest jobs are available in the Southwest of the state. From October to June you can harvest grapes and orchard fruits. March to November there is also fishing and processing work available for crayfish, prawns and scallops between Fremantle and Carnarvon. From May to October head northeast, around Kununurra for harvest jobs and fruit picking.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit6.png" height="auto" width="100%">
                    </div>
                    
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Mar</td>
<td>Grapes</td>
<td>Margaret River / Swan Valley / Mt Barker</td>
</tr>
<tr>
<td>Mar – May</td>
<td>Apples &amp; Pears</td>
<td>Pemberton / Donnybrook / Manjimup</td>
</tr>
<tr>
<td>Mar – Oct</td>
<td>Prawning &amp; Scalloping</td>
<td>Carnavon</td>
</tr>
<tr>
<td>May – Sep</td>
<td>Zucchini &amp; Rock Melons</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Apr – Nov</td>
<td>Melons</td>
<td>Coorow</td>
</tr>
<tr>
<td>Jun – Dec</td>
<td>Melons &amp; Tomatoes</td>
<td>Carnavon</td>
</tr>
<tr>
<td>Jul – Aug</td>
<td>Bananas</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Jul – Dec</td>
<td>Wildflowers</td>
<td>Coorow / Muchea</td>
</tr>
<tr>
<td>Oct – Jan</td>
<td>Mangoes</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Nov – Jun</td>
<td>Rock Lobster</td>
<td>Fremantle / Kalbarri / Dongara / Geraldton / Broome</td>
</tr>
<tr>
<td>All Year</td>
<td>Fishing</td>
<td>Broome</td>
</tr>
</tbody>
</table>
               <h5>Northern Territory</h5>
                  <div class="row">
                    <p class="col-md-8">May to October picking melons around Katherine and Darwin is popular. From October to November head to Katherine and Darwin for mango picking and packing jobs as well as citrus and bananas which are grown all year around there.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit7.png" height="auto" width="100%">
                    </div>
                    
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Mar – Oct</td>
<td>Melons</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>Oct – Nov</td>
<td>Mangoes</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>All Year</td>
<td>Citrus / Bananas</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>All Year</td>
<td>Asian Vegetables &amp; Cutting Flowers</td>
<td>Darwin</td>
</tr>
</tbody>
</table>
<p>Information from:<a target="_blank" href="http://www.fruitpicking.org/fruit–picking–essentials/crops–calendar"> http://www.fruitpicking.org/fruit–picking–essentials/crops–calendar</a></p>

              </div>
            </div>
          </div>
           <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:black;">
                RETAIL
              </a>
            </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <p>Every part of Australia needs people to work in retail.</p>

                <p>Shop work can be physical, such as if you work in a warehouse and require long hours. Don’t forget to put in your profile if you have any physical issues that may be important.</p>

                <p><a target="_blank" href="https://www.payscale.com/research/US/Skill=Retail/Hourly_Rate">Click here</a> to see the Award Rates for retail in Australia.</p>
              </div>
            </div>
          </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="color:black;">
              CONSTRUCTION
            </a>
          </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
             <p>Building and Construction award rates can be found by clicking </p> <a target="_blank" href="http://awardviewer.fwo.gov.au/award/show/MA000020#P551_59778">Click here.</a>

            </div>
          </div>
        </div>
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="heading6">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6" style="color:black;">
              CUSTOMER SERVICE
            </a>
          </h4>
          </div>
          <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Customer Service covers everything from retail to call centres.</p>

                <p>If you are good with people, this could be a great option to get you working quicking with room to move into adminstration or management.</p>
            </div>
          </div>
        </div>

        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapseThree" style="color:black;">
              CLEANING
            </a>
          </h4>
          </div>
          <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Cleaning can be a rewarding job that could include domestic or commercial work.</p>

              <p>Experience will definately pay, so if you have any, put it in your profile!</p>

              <p><a target="_blank" href="https://www.payscale.com/research/US/Skill=Cleaning/Hourly_Rate">Click here</a> to get an idea of the wages you could be paid.</p>
            </div>
          </div>
        </div>
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseThree" style="color:black;">
              FACTORY
            </a>
          </h4>
          </div>
          <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Factory work may not sound glamourous, but you can be assured that the training will be excellent and all legal conditions will be met.</p>

              <p><a target="_blank" href="https://www.payscale.com/research/US/Job=Factory_Worker/Hourly_Rate">Click here</a> to see a pay estimate for this industry.</p>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </div>


        </div>
      </div>

</div>
</div>


  <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/circles-v3.jpg" height="600" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <h2 style="font-family: 'Comfortaa', cursive;">
            Learn more about how a 
            <br>professional CV can help 
            <br>get you the job you want.</h2>
            <div>
              <input class="form-control" type="text" name="Email" placeholder="Email" style="width:300px;">
              <br>
              <a href="#" class="btn-get-started scrollto" style="width:300px;text-align:center;">SUBSCRIBE</a>
            </div>
         </div>

      </div>
    </div>
<?php include_once('../includes/external-pre-footer.php');?>

<?php include_once('../includes/register_footer.php');?>
</div>


</body>
</html>



  <style type="text/css">
   .progressbar {
      counter-reset: step;
      font-size: 20px;
      font-family: 'Comfortaa', cursive;
    }

    .progressbar li {
      list-style-type: none;
      float: left;
      width: 25%;
      position: relative;
      text-align: center
    }

    .progressbar li:before {
      content: counter(step);
      counter-increment: step;
      color: white;
      width: 50px;
      height: 50px;
      line-height: 50px;
      border: 3px solid #ddd;
      display: block;
      text-align: center;
      margin: 0 auto 10px auto;
      border-radius: 50%;
      background-color: #003E6B;
    }

    .progressbar li:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 1px;
      background-color: #ddd;
      top: 25px;
      left: -50%;
      z-index: -1;
    }

    .progressbar li:first-child:after {
      content:none;
    }

    .progressbar li.active {
      color: green;
    }



  </style>






  


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<!--     <script src="js/jquery.min.js"></script> -->

  <!-- <script src="lib/jquery/jquery.min.js"></script> -->



<!--   <script src="../lib/jquery/jquery-migrate.min.js"></script>
  <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../lib/easing/easing.min.js"></script>
  <script src="../lib/mobile-nav/mobile-nav.js"></script>
  <script src="../lib/wow/wow.min.js"></script>
  <script src="../lib/waypoints/waypoints.min.js"></script>
  <script src="../lib/counterup/counterup.min.js"></script>
  <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../lib/isotope/isotope.pkgd.min.js"></script>
  <script src="../lib/lightbox/js/lightbox.min.js"></script> -->

  <script src="../js/croppie.js"></script>
        <!-- Datepicker -->
 <!--  <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- Contact Form JavaScript File -->
  <script src="../contactform/contactform.js"></script>
 

  
  <!-- Template Main Javascript File -->
  <script src="../js/main.js"></script>




  <script>
  function check(name)
  {  
		$.ajax({
          url: "../ajax/check_name.php",
    			type: 'POST',
    			data: {name:name},
     			success:function(data)
           {
            if(data==1)
            {
              document.getElementById('val').innerHTML ="Email already in use!";
              document.getElementById("email").setCustomValidity("Email already in use!");
            } 
            else
            {
              document.getElementById('val').innerHTML ="";
              document.getElementById("email").setCustomValidity("");
            }
           }
				});			
		
  }

</script>

<script>
        grecaptcha.ready(function() {
          grecaptcha.execute('6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK', {action: 'submit'}).then(function(token) {
              document.getElementById("g-token").value = token;
          });
        });
</script>

<script>
function status_warning(val)
{
  if(val=='2'){
    document.getElementById('workstatus_warning').innerHTML ="<i class='fa fa-warning' style='font-size:15px;color:orange'></i><strong> Warning!</strong> Your profile will be hidden while this is selected.";
  }
  else{
    document.getElementById('workstatus_warning').innerHTML ="";
  }
  // if(val=="2")
  // {
  // var x = document.getElementById("snackbar");
  // 				x.className = "show";
  // 				setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  // }
}

document.getElementById("filename").onchange = function (e) {

//var fname = "the file name here.ext";
var fname = e.target.files[0].name; 
var re = /(\.pdf)$/i;
var maxfilesize = 3024 * 3024;
var fsize=e.target.files[0].size;
if(fsize<maxfilesize)
{
if(!re.exec(fname))
{
alert("File extension not supported!");
document.getElementById('filename').setCustomValidity("File extension not supported!")
}
else
{
  document.getElementById('filename').setCustomValidity("")
}
}
else
{
  alert("File size must be less than 1MB");
document.getElementById('filename').setCustomValidity("File size must be less than 1MB")
}


};
</script>

<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_job_image.php",
        type: "POST",
        data:{"image": response},
  
        success:function(data)
        {
         
       
          $('#uploadimageModal').modal('hide');
          // $('#uploaded_image').html(data);
          $('#uploaded_image').attr("src",data);
          document.getElementById('file_location').value=data;
        }
      });
    })
  });

});  
</script>
 
 <script>
                $("#cb_post").on('click', function(){

               $("#hiddenDiv").toggle();

               });
  </script>
  <script>
$(function() {
  var checkbox = $("#mob_later");
  var hidden = $("#div01");
 
  checkbox.change(function() {
    if (checkbox.is(':checked')) {
      
      $('#mobile_no').prop('required', false); //to add required
      $("#clr").toggle();
    
    } else {
     // $("#mobile_no").val("");
   
      $('#mobile_no').prop('required', true); //to remove required
      $("#clr").hide();
    }
  });
});


</script>


<style>
#snackbar {
  /*visibility: hidden;*/
  visibility: visible;
  min-width: 150px;
  margin-left: -125px;
  background-color: #f23a2e;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 8px;
  position: fixed;
  z-index: 1;
  left: 50%;
  top: 30px;
  font-size: 15px;
}

#snackbar.show {
  visibility: visible;
  /*-webkit-animation: fadein 0.8s, fadeout 0.8s 2.5s;
  animation: fadein 0.8s, fadeout 0.8s 2.5s;*/
}

@-webkit-keyframes fadein {
  from {top: 0; opacity: 0;} 
  to {top: 30px; opacity: 1;}
}

@keyframes fadein {
  from {top: 0; opacity: 0;}
  to {top: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {top: 30px; opacity: 1;} 
  to {top: 0; opacity: 0;}
}

@keyframes fadeout {
  from {top: 30px; opacity: 1;}
  to {top: 0; opacity: 0;}
}

.nopad {
	padding-left: 0 !important;
	padding-right: 0 !important;
  margin-right:20px;
}
/*image gallery*/
.image-checkbox {
	cursor: pointer;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 4px solid transparent;
	margin-bottom: 0;
	outline: 0;
  padding-right: 5px;
    padding-left: 5px;

}
.image-checkbox p
{
  margin:0px;
}
.image-checkbox input[type="checkbox"] {
	display: none;
}

.image-checkbox-checked {
	border-color: #4783B0;
}
.image-checkbox .fa {
  position: absolute;
  color: #b1d5f3;
  background-color:#4A79A3;
  padding: 10px;
  top: 0;
  right: 0;
}
.image-checkbox-checked .fa {
  display: block !important;
}
.hidden
{
  display:none;
}
.categories_img 
{
  padding:20px;
  margin-right:40px;
}
.categories_img .fa-check
{
  margin-top:20px !important;
}
  </style>









  <script>
// image gallery
// init the state from the input
$(".image-checkbox").each(function () {
  if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
    $(this).addClass('image-checkbox-checked');
  }
  else {
    $(this).removeClass('image-checkbox-checked');
  }
});


    </script>
<script>



// sync the state to the input
$(".image-checkbox").on("click", function (e) {
  $(this).toggleClass('image-checkbox-checked');
  var $checkbox = $(this).find('input[type="checkbox"]');
  $checkbox.prop("checked",!$checkbox.prop("checked"))
 if($(this).find('input[type="checkbox"]').attr('id')=='any')
 {
                $('#any').prop('required', true);
                $('#full').prop('required', false);
                $('#part').prop('required', false);
                $('#week').prop('required', false);
                $('#night').prop('required', false);

                $('#full').prop('checked', false); // Unchecks it
                $('#part').prop('checked', false); // Unchecks it
                $('#week').prop('checked', false); // Unchecks it
                $('#night').prop('checked', false); // Unchecks it

                $("#cfull").removeClass("image-checkbox-checked");
                $("#cpart").removeClass("image-checkbox-checked");
                $("#cweek").removeClass("image-checkbox-checked");
                $("#cnight").removeClass("image-checkbox-checked");
              

 }
 if($(this).find('input[type="checkbox"]').attr('id')=='full')
 {
                $('#any').prop('required', false);
                $('#full').prop('required', true);
                $('#part').prop('required', false);
                $('#week').prop('required', false);
                $('#night').prop('required', false);

                $('#any').prop('checked', false); // Unchecks it
                $('#part').prop('checked', false); // Unchecks it
                $('#week').prop('checked', false); // Unchecks it
                $('#night').prop('checked', false); // Unchecks it

                $("#cany").removeClass("image-checkbox-checked");
                $("#cpart").removeClass("image-checkbox-checked");
                $("#cweek").removeClass("image-checkbox-checked");
                $("#cnight").removeClass("image-checkbox-checked");

 }
 if($(this).find('input[type="checkbox"]').attr('id')=='part')
 {
                $('#any').prop('required', false);
                $('#full').prop('required', false);
                $('#part').prop('required', true);
                $('#week').prop('required', false);
                $('#night').prop('required', false);

                $('#any').prop('checked', false); // Unchecks it
                $('#full').prop('checked', false); // Unchecks it
                $('#week').prop('checked', false); // Unchecks it
                $('#night').prop('checked', false); // Unchecks it

                $("#cany").removeClass("image-checkbox-checked");
                $("#cfull").removeClass("image-checkbox-checked");
                $("#cweek").removeClass("image-checkbox-checked");
                $("#cnight").removeClass("image-checkbox-checked");

 }
 if($(this).find('input[type="checkbox"]').attr('id')=='week')
 {
                $('#any').prop('required', false);
                $('#full').prop('required', false);
                $('#part').prop('required', false);
                $('#week').prop('required', true);
                $('#night').prop('required', false);

                $('#any').prop('checked', false); // Unchecks it
                $('#full').prop('checked', false); // Unchecks it
                $('#part').prop('checked', false); // Unchecks it
                $('#night').prop('checked', false); // Unchecks it

                $("#cany").removeClass("image-checkbox-checked");
                $("#cfull").removeClass("image-checkbox-checked");
                $("#cpart").removeClass("image-checkbox-checked");
                $("#cnight").removeClass("image-checkbox-checked");

 }
 if($(this).find('input[type="checkbox"]').attr('id')=='night')
 {
                $('#any').prop('required', false);
                $('#full').prop('required', false);
                $('#part').prop('required', false);
                $('#week').prop('required', false);
                $('#night').prop('required', true);

                $('#any').prop('checked', false); // Unchecks it
                $('#full').prop('checked', false); // Unchecks it
                $('#part').prop('checked', false); // Unchecks it
                $('#week').prop('checked', false); // Unchecks it

                $("#cany").removeClass("image-checkbox-checked");
                $("#cfull").removeClass("image-checkbox-checked");
                $("#cpart").removeClass("image-checkbox-checked");
                $("#cweek").removeClass("image-checkbox-checked");

 }
  e.preventDefault();
});
</script>

<script type="text/javascript">

;(function($) {

      // Browser supports HTML5 multiple file?
      var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
          isIE = /msie/i.test( navigator.userAgent );

      $.fn.customFile = function() {

        return this.each(function() {

          var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
              $wrap = $('<div class="file-upload-wrapper">'),
              $input = $('<input type="text" class="file-upload-input" />'),
              // Button that will be used in non-IE browsers
              $button = $('<button type="button" class="file-upload-button">Select a File</button>'),
              // Hack for IE
              $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

      
          $file.css({
            position: 'absolute',
            left: '-9999px'
          });

          $wrap.insertAfter( $file )
            .append( $file, $input, ( isIE ? $label : $button ) );

          // Prevent focus
          $file.attr('tabIndex', -1);
          $button.attr('tabIndex', -1);

          $button.click(function () {
            $file.focus().click(); // Open dialog
          });

          $file.change(function() {

            var files = [], fileArr, filename;

            // If multiple is supported then extract
            // all filenames from the file array
            if ( multipleSupport ) {
              fileArr = $file[0].files;
              for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                files.push( fileArr[i].name );
              }
              filename = files.join(', ');

            // If not supported then just take the value
            // and remove the path to just show the filename
            } else {
              filename = $file.val().split('\\').pop();
            }

            $input.val( filename ) // Set the value
              .attr('title', filename) // Show filename in title tootlip
              .focus(); // Regain focus

          });

          $input.on({
            blur: function() { $file.trigger('blur'); },
            keydown: function( e ) {
              if ( e.which === 13 ) { // Enter
                if ( !isIE ) { $file.trigger('click'); }
              } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                // On some browsers the value is read-only
                // with this trick we remove the old input and add
                // a clean clone with all the original events attached
                $file.replaceWith( $file = $file.clone( true ) );
                $file.trigger('change');
                $input.val('');
              } else if ( e.which === 9 ){ // TAB
                return;
              } else { // All other keys
                return false;
              }
            }
          });

        });

      };

      // Old browser fallback
      if ( !multipleSupport ) {
        $( document ).on('change', 'input.customfile', function() {

          var $this = $(this),
              // Create a unique ID so we
              // can attach the label to the input
              uniqId = 'customfile_'+ (new Date()).getTime(),
              $wrap = $this.parent(),

              // Filter empty input
              $inputs = $wrap.siblings().find('.file-upload-input')
                .filter(function(){ return !this.value }),

              $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

          // 1ms timeout so it runs after all other events
          // that modify the value have triggered
          setTimeout(function() {
            // Add a new input
            if ( $this.val() ) {
              // Check for empty fields to prevent
              // creating new inputs when changing files
              if ( !$inputs.length ) {
                $wrap.after( $file );
                $file.customFile();
              }
            // Remove and reorganize inputs
            } else {
              $inputs.parent().remove();
              // Move the input so it's always last on the list
              $wrap.appendTo( $wrap.parent() );
              $wrap.find('input').focus();
            }
          }, 1);

        });
      }

}(jQuery));

$('input[type=file]').customFile();
</script>

