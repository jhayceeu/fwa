<?php
session_start();
include_once('../includes/config.php');
include('gpConfig.php');

if(isset($_POST['submit']))
{
  $secretKey = '6LeiFLEZAAAAAPhmVPm907CtOOBxRFwbIh1Cgy9R';
  $token = $_POST['g-token'];
  $ip = $_SERVER['REMOTE_ADDR'];

  $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$token."&remoteip=".$ip;
  $request = file_get_contents($url);
  $response = json_decode($request);

    $name=$_POST['email'];
    $password=$_POST['password'];
    $sql="select * from tbl_jobseeker where email='".$name."' and password='".md5($password)."'";
    $result=$con->query($sql);

    if($response->success){
      if($row=$result->fetch_assoc())
      {        
      $_SESSION['jobseeker_id']=$row['id'];
      $_SESSION['jobseeker_name']=$row['first_name'];
      header('location:../jobseeker/profile.php');
      }
      else
      {        
        $_SESSION['error']= "invalid email or password";
      }

   }
   else{
    $_SESSION['error']='Captcha Validation Failed.';
   }
}

if(isset($_GET["code"]))
{
 //It will Attempt to exchange a code for an valid authentication token.
 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
 if(!isset($token['error']))
 {
  //Set the access token used for requests
  $google_client->setAccessToken($token['access_token']);

  //Store "access_token" value in $_SESSION variable for future use.
  $_SESSION['access_token'] = $token['access_token'];

  //Create Object of Google Service OAuth 2 class
  $google_service = new Google_Service_Oauth2($google_client);

  //Get user profile data from google
  $data = $google_service->userinfo->get();

  //Below you can find Get profile data and store into $_SESSION variable
  if(!empty($data['given_name']))
  {
   $_SESSION['user_first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['user_last_name'] = $data['family_name'];
  }

  if(!empty($data['email']))
  {
   $_SESSION['user_email_address'] = $data['email'];
  }

  if(!empty($data['gender']))
  {
   $_SESSION['user_gender'] = $data['gender'];
  }

  if(!empty($data['picture']))
  {
   $_SESSION['user_image'] = $data['picture'];
  }
 }

$sql="select * from tbl_jobseeker where email='".$_SESSION['user_email_address']."'";
  $res=$con->query($sql);
      if($res->num_rows>0)
        {
         
          while($row=$res->fetch_assoc())
            {
              $_SESSION['jobseeker_id']=$row['id'];
              $_SESSION['jobseeker_name']=$row['first_name'];
            }
                   
              header ('location:profile.php');
        }

        else
         {
          $sqlgoogle="insert into tbl_jobseeker (first_name, last_name, email) values ('" .$_SESSION['user_first_name']. "', '".$_SESSION['user_last_name']."', '".$_SESSION['user_email_address']."') ";

          if($con->query($sqlgoogle))
          {

            $sql="select * from tbl_jobseeker where email='".$_SESSION['user_email_address']."'";
              $res=$con->query($sql);
                if($res->num_rows>0)
                {
           
                  while($row=$res->fetch_assoc())
                  {
                    $_SESSION['jobseeker_id']=$row['id'];
                    $_SESSION['jobseeker_name']=$row['first_name'];
                  }
                     
                  header ('location:profile.php');
                }
          }

         }

}

//This is for check user has login into system by using Google account, if User not login into system then it will execute if block of code and make code for display Login link for Login using Google account.
if(!isset($_SESSION['access_token']))
{
  //Create a URL to obtain user authorization
  $googleLogin = $google_client->createAuthUrl();
}


include_once('../includes/external_header.php');

?>
<script src="https://www.google.com/recaptcha/api.js?render=6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK"></script>

  <div class="col-md-12" class="login_banner">
    <div class="row">        
      <img src="../img/login.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position: absolute;top:140px;">
            <p>FIND WORK AUSTRALIA</p>
            <h2>Jobseeker Login</h2>                
        </div>
      </div>
    </div>
  </div>


  <div class="col-md-12 login_back">
    <div class="container">
      <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 login">
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <form method="post">
              <div class="row">
              <?php
                    if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Error!</h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Success!</h4>
                            ".$_SESSION['success']."
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
                ?>
               
              <div class="col-md-11">
                <h4>Login</h4>
                <div class="inputWithIcon" style="margin-top: 25px;">
                  <input type="hidden" id="g-token" name="g-token"/>
                  <input type="text" id="email" name="email" placeholder="Email" autocomplete="off" required >
                  <i class="fa fa-envelope-o fa-lg fa-fw" aria-hidden="true"></i>
                </div>
                <div class="inputWithIcon" style="margin-top: 25px;">
                   <input type="text" style="-webkit-text-security: disc;" id="password" name="password"  placeholder="Password" autocomplete="off" required>
                  <i class="fa fa-lock fa-lg fa-fw" aria-hidden="true"></i>
                </div>
                <input type="submit" name="submit" class="login_btn">
                <p><a href="forgot_password.php">Forgot password?</a></p>
                <!-- <p><a href="jobseeker_password_reset.php">Forgot password?</a></p> -->
              </div>
                      
               <!-- <div class="vl"></div> -->
             </div>
             </form>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>
            <div class="col-md-8 fb" style="">
              <h4>Also, you can</h4>
              <i class="fa fa-facebook" aria-hidden="true"></i>
              <a href="fblogin.php"><button class="fb_btn">Login with Facebook</button></a>
                
              <i class="fa fa-google" aria-hidden="true"></i> 
              <a href="<?php echo $googleLogin; ?> "><button class="google_btn">Sign in with Google</button></a> 
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-2"></div>
            <div class="col-md-8 fb" style="margin-top: 20px;">
              <div class="row">
                <div class="col-md-6">             
                  <a href="register.php" class="button btnPush btnLightBlue">register</a>
                </div>
                <div class="col-md-6">             
                  <a href="../employer/overview.php#jobseeker-overview" class="button btnPush btnLightBlue">learn more</a> 
                </div>
              </div>
            </div>

<script>
        grecaptcha.ready(function() {
          grecaptcha.execute('6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK', {action: 'submit'}).then(function(token) {
              document.getElementById("g-token").value = token;
          });
        });
</script>   

<style type="text/css">


</style>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
    /*@import url('https://fonts.googleapis.com/css?family=Rubik:700&display=swap');*/

/*$bg: #fff;
$text: #382b22;
$light-pink: #fff0f0;
$pink: #ffe9e9;
$dark-pink: #f9c4d2;
$pink-border: #b18597;
$pink-shadow: #ffe3e2;*/


/*

button {
  position: relative;
  display: inline-block;
  cursor: pointer;
  outline: none;
  border: 0;
  vertical-align: middle;
  text-decoration: none;
  font-size: inherit;
  font-family: inherit;
}
  .learn-more {
    font-weight: 600;
    color: #382b22;
    text-transform: uppercase;
    padding: 1.25em 2em;
    background: #fff0f0;
    border: 2px solid #b18597;
    border-radius: 0.75em;
    transform-style: preserve-3d;
    transition: transform 150ms cubic-bezier(0, 0, 0.58, 1), background 150ms cubic-bezier(0, 0, 0.58, 1);
  }
   .learn-more ::before {
      position: absolute;
      content: '';
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background: #f9c4d2;
      border-radius: inherit;
      box-shadow: 0 0 0 2px #b18597, 0 0.625em 0 0 #ffe3e2;
      transform: translate3d(0, 0.75em, -1em);
      transition: transform 150ms cubic-bezier(0, 0, 0.58, 1), box-shadow 150ms cubic-bezier(0, 0, 0.58, 1);
    }
    .learn-more:hover {
      background:  #ffe9e9;
      transform: translate(0, 0.25em);
    }
     .learn-more ::before {
        box-shadow: 0 0 0 2px  #b18597, 0 0.5em 0 0 #ffe3e2;
        transform: translate3d(0, 0.5em, -1em);
      }
    
    .learn-more :active {
      background:  #ffe9e9;
      transform: translate(0em, 0.75em);
    }
     .learn-more ::before {
        box-shadow: 0 0 0 2px  #b18597, 0 0 #ffe3e2;
        transform: translate3d(0, 0, -1em);
      }*/
/*    }
  }
}*/
  </style>
<?php include_once('../includes/external_footer.php'); ?>