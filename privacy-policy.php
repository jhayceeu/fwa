<?php 
session_start();
include_once('includes/config.php');

include_once('includes/header.php'); 

?>
 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- <link href="assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" /> -->
<!-- <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'> -->
 
<body>

  <div class="col-md-12" class="login_banner">
    <div class="row">      
      <img src="img/privacy2.jpg" height="auto" width="100%">
       <div class="container">
         <div class="intro-info" style="position: absolute;top:228px;">
            <!-- <p>CONTACT US</p> -->
            <h2>Privacy Policy</h2>                
        </div>
      </div>
    </div>
  </div>

        <div class="col-md-12 privacy">
          <div class="container"> 
            <div class="row">
              <div class="col-md-12" >
                 <h4>Who we are</h4>
                 <p>Our website address is: https://fwal.choicedemo.com.au.</p>
                 <h4>What personal data we collect and why we collect it</h4>
                 <h6 style="margin-top: 20px;">Media</h6>
                 <p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>
                 <h6>Contact forms</h6>
                 <p>We use these along with a ticketing system to track queries, issues and answer questions. They will be forwarded to the relevant party.</p>
                 <p>Some forms may add you to our mailing list.</p>
                 <h6>Cookies</h6>
                 <p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>
                 <p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>
                 <p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select “Remember Me”, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>

                 <h6>Embedded content from other websites</h6>
                 <p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>               
                 <p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>
                 <h6>Analytics</h6>
                 <p>We use standard website tracking tools provided by reputable companies to track the behaviour of people on our site. We do not track individuals but use the traffic results as a whole to modify and improve your experience.</p>
                 <h6>Who we share your data with</h6>
                 <p>Find Work Australia will not share your data.</p>


                 <h6>How long we retain your data</h6>
                 <p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>

                 <h6>What rights you have over your data</h6>
                 <p>If you have an account on this site, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>
                 <h6>Where we send your data</h6>
                 <p>Regular back ups of Find Work Australia will be taken and kept in a secure location. We do this in case the primary site becomes compromised in any way.</p>
                 <h6>Your contact information</h6>
                 <p>If you would like to know more, please send an email to info@findworkaustralia.com.au</p>
                 <h4>Additional information</h4>

                  <h6 style="margin-top: 20px;">How we protect your data</h6>
                 <p>We are dedicating to providing a secure zone for your information. We have a dedicated online security team who manage our site to ensure it meets the latest security standards.</p>
                  <h6>What data breach procedures we have in place</h6>
                 <p>A copy of our Data Break Procedures can be requested by emailing info@findworkaustralia.com.au</p>
                  <h6>What third parties we receive data from</h6>
                 <p style="padding-bottom: 20px;">Google.</p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

<?php include_once('includes/pre-footer.php'); ?>
<style type="text/css">
  .privacy{
  margin-top: 30px;
}
  .search {
  width: 100%;
  position: relative;
  display: flex;
}

.searchTerm {
  border: 1px solid #7d7e82;
    border-right: none;
    padding: 5px;
    /* height: 20px; */
    border-radius: 5px 0 0 5px;
    outline: none;
    color: #9DBFAF;
}

.searchTerm:focus{
  color: #00B4CC;
}

.searchButton {
  height: 36px;
    border: 1px solid #000f4f;
    background: #001469;
    text-align: center;
    color: #fff;
    border-radius: 0 5px 5px 0;
    cursor: pointer;
    text-transform: capitalize;  
    font-size: 14px;
}
.privacy h4{
  font-family: 'Comfortaa', cursive; 
}
.right_search h6{
font-size: 13px;
}
.right_search h4{
  font-size: 16px;
      font-family: 'Comfortaa', cursive;
}
.wrap{
    width: 30%;
    margin-top: 25%;
    margin-left: 15%;
  transform: translate(-50%, -50%);
}
</style>

    <?php include_once('includes/footer.php');?>
   <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
<script type="text/javascript">
  $(document).ready(function() {
  $('.collapse.in').prev('.panel-heading').addClass('active');
  $('#accordion, #bs-collapse')
    .on('show.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').addClass('active');
    })
    .on('hide.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').removeClass('active');
    });
});
</script>






      <!-- <script src="lib/jquery/jquery.min.js"></script> -->
  <!-- <script src="lib/jquery/jquery-migrate.min.js"></script> -->
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <!-- <script src="lib/owlcarousel/owl.carousel.min.js"></script> -->
  <!-- <script src="lib/isotope/isotope.pkgd.min.js"></script> -->
  <script src="lib/lightbox/js/lightbox.min.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  