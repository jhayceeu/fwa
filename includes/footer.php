
  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info" style="    margin-top: -75px;">
            <img src="img/LOGO_FOOTER.png">            
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Jobseeker</h4>
            <ul>
              <li><a href="jobseeker/register.php">Create a Jobseeker Profile</a></li>
              <li><a href="jobseeker/login.php">Jobseeker Login</a></li>
              <li><a href="employer/overview.php#jobseeker-overview">Jobseeker Overview</a></li>
              
             
            </ul>
          </div>
           <div class="col-lg-3 col-md-6 footer-links">
            <h4>Employers</h4>
            <ul>
              <li><a href="employer/register.php">Create an Employer Account</a></li>
              <li><a href="employer/login.php">Employer Login</a></li>
              <li><a href="employer/overview.php#employer-overview">Employer Overview</a></li>
              <li><a href="jobseeker/understanding.php">Understanding Jobseeker Profile</a></li> 
            </ul>
          </div>
<!--           <div class="col-lg-3 col-md-6 footer-links">
            <h4>Agency</h4>
            <ul>
              <li><a href="agency.php">Agency</a></li>          
            </ul>
          </div> -->
          <div class="col-md-6 footer_job">
            <h1 style="font-size: 13vw;">Jobs</h1>
          </div>

         </div>
      </div>
    </div>
<div class="col-md-12" style="background-color: #002d5e;">
    <div class="container">
      <div class="row">
      <div class="col-md-6 copyright" style="text-align: left;">
       Find Work Australia
      </div>
      <div class="col-md-6 credits" style="text-align: right;">
           <i class="fa fa-facebook" aria-hidden="true"></i>
           <i class="fa fa-twitter" aria-hidden="true"></i>
           <i class="fa fa-google-plus" aria-hidden="true"></i>
           <i class="fa fa-instagram" aria-hidden="true"></i>
           <i class="fa fa-linkedin" aria-hidden="true"></i>
      </div>
    </div>
    </div>
  </div>
  </footer>

  <!-- JavaScript Libraries -->
<script src="js/jquery-3.3.1.min.js"></script>
 <script src="js/jquery.Jcrop.min.js"></script>
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script> 
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script> 
  <script src="lib/mobile-nav/mobile-nav.js"></script> 
  <script src="lib/wow/wow.min.js"></script> 
  <script src="lib/waypoints/waypoints.min.js"></script> 
  <script src="lib/counterup/counterup.min.js"></script> 
  <script src="lib/owlcarousel/owl.carousel.min.js"></script> 
  <script src="lib/isotope/isotope.pkgd.min.js"></script> 
  <script src="lib/lightbox/js/lightbox.min.js"></script> 
  
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>  
  <script src='js/jquery.min.js'></script>
  <script src='js/bootstrap.min.js'></script>

    <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>

<script>
  if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>
</body>
</html>