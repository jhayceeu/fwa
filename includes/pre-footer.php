 <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="col-md-12 info">
            <img src="img/FWA-Logo.png" width="100%">
            </div>
          </div>
          <div class="col-md-6">

          <div class="col-md-12 social-links2">
           <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
           <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
           <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
           <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
         </div>
          </div>

          <style>

           </style>
          
        </div>
        <div class="row">

          <div class="col-md-2">
            <div class="col-md-12 info">
               <h3>Office</h3>
                <i class="ion-ios-location-outline"></i>
                <p>Park Road, Milton <br>
                  QLD 4064 Australia</p>
              </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12 info">
               <h3>Contact Us</h3>
                 <i class="ion-ios-email-outline"></i>
                <a href="mailto:hello@findworkaustralia.com.au"><p>hello@findworkaustralia.com.au</p></a>               
                  <i class="ion-ios-telephone-outline"></i>
                  <p>+61 7 3040 5855</p>               
            </div>
          </div>
          <div class="col-md-6 contact_text">
            <h2>I'd like to hear more about what Find Work Australia are doing.</h2>
            <input type="email" class="email_box" placeholder="Email">
            <button type="submit" title="Send Message" class="subscribe_btn">Subscribe</button>
          </div>

      </div>
    </section><!-- #contact -->