<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Jobseeker</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <!-- <link href="img/favicon.png" rel="icon"> -->
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
  <link rel="stylesheet" href="css/croppie.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <!-- <link rel='stylesheet' href='css/bootstrap1.min.css'>  -->

</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php
        if(isset($_SESSION['employee_id']))
        {
          ?>
<ul>
          <li class="active"><a href="index.php">Home</a></li> 
          
          <li><a href="login.html">My Profile</a></li> 
          <li><a href="logout.php">Logout</a></li> 
        </ul>
          <?php
        }
        else
        {
        ?>
      <ul>
          <li class="active"><a href="index.php">Home</a></li> 
          <li class="drop-down"><a href="">Jobseeker</a>
            <ul>
              <li><a href="login.php">Login</a></li>       
              <li><a href="#">Jobseeker Overview</a></li>
              <li><a href="jobseeker_register.php">Jobseeker Registration</a></li>
            
            </ul>
          </li>
          <li class="drop-down"><a href="">Employer</a>
            <ul>
                <li><a href="#">Login</a></li>       
              <li><a href="#">Employer Overview</a></li>
              <li><a href="#">See a Jobseeker profile</a></li>
            
            </ul>
          </li>
          <li class="drop-down"><a href="">Agency</a>
            <ul>
              <li><a href="#">Agency Login</a></li>
            </ul>
          </li>
          <li class="drop-down"><a href="">Contactff</a>
            <ul>
              <li><a href="#">Privacy Policy</a></li>
            </ul>
          </li>
          <!-- <li><a href="login.html">Login</a></li> 
          <li><a href="#intro">Registration</a></li>  -->
        </ul>
        <?php
        }
        ?>
      
      
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->