
  <?php
if(isset($_SESSION['employer_id']))
{
 
  $balance=0;
  $balance_active=0;
  $subscription_id="";
    $tdate= date("Y-m-d");

    // $sql="select * from tbl_subscription where employer_id='".$_SESSION['employer_id']."' and status=2 and  (expiry_date>='".$tdate."' or expiry_date='unlimited') order by subscription_date asc";
    // $res=$con->query($sql);
    // while($row=$res->fetch_assoc())
    // {
    //   $subscription_id=$row['subscription_id'];
    //   $token_name="";
    //   $expiry_date="";
    //   $view_cnt="";
    //   $sql_pack="select * from tbl_package where package_id='".$row['token_id']."'";
    //   $res_pack=$con->query($sql_pack);
    //   if($row_pack=$res_pack->fetch_assoc())
    //   {
    //       $token_name=$row_pack['package_name'];
    //       $expiry_date=$row['expiry_date'];
    //       $expiry_date = date("d-m-Y", strtotime($expiry_date));
    //       $view_cnt=$row_pack['view_count'];
    //   }

    //   $sql_balance="select sum(used_tokens) as cnt from tbl_employer_view_list where subscription_id='".$subscription_id."'";
    //   $res_balance=$con->query($sql_balance);
    //   if($row_balance=$res_balance->fetch_assoc())
    //   {
    //     $balance_active=$balance_active+($view_cnt-$row_balance['cnt']);
    //   }
    // }

    $sql="select * from tbl_subscription where employer_id='".$_SESSION['employer_id']."' and status<>0 and  (expiry_date>='".$tdate."' or expiry_date='unlimited') order by subscription_date asc";
    $res=$con->query($sql);
    while($row=$res->fetch_assoc())
    {
      $subscription_id=$row['subscription_id'];
      $token_name="";
      $expiry_date="";
      $view_cnt="";
      $sql_pack="select * from tbl_package where package_id='".$row['token_id']."'";
      $res_pack=$con->query($sql_pack);
      if($row_pack=$res_pack->fetch_assoc())
      {
          $token_name=$row_pack['package_name'];
          $expiry_date=$row['expiry_date'];
          $expiry_date = date("d-m-Y", strtotime($expiry_date));
          $view_cnt=$row_pack['view_count'];
      }
if($view_cnt!="unlimited")
{
      $sql_balance="select sum(used_tokens) as cnt from tbl_employer_view_list where subscription_id='".$subscription_id."'";
      $res_balance=$con->query($sql_balance);
      if($row_balance=$res_balance->fetch_assoc())
      {
        $balance=$balance+($view_cnt-$row_balance['cnt']);
      }
    }
    else if($view_cnt=="unlimited")
    {
      $balance="Unlimited";
    }
    }
$extra=0;
    $sql="select extra_tokens from tbl_employer where id='".$_SESSION['employer_id']."' and extra_tokens>0";
    $res=$con->query($sql);
    if($row=$res->fetch_assoc())
    {
      $extra=$row['extra_tokens'];
    }
    

  ?>
<section class="custom-social-proof">
    <div class="custom-notification">
      <div class="custom-notification-container">
       
        <div class="custom-notification-content-wrapper">
          <!-- <p class="custom-notification-content" style="color:#fff;">Active plan Token Balance : <?php echo  $balance_active; ?></p> -->
          <p class="custom-notification-content" style="color:#fff;">
          <?php
      
            if($balance==='Unlimited')
            {
              
              ?>
                Token Balance  : <b> <?php  echo "Unlimited"; ?></b> 
              <?php
            }
            else
            {
          ?>
           Token Balance  : <b> <?php  echo $balance+$extra; ?></b> 
           <?php
            }
            ?> 
            
          </p>
          <?php 
          // if($extra>0)
          // {
          ?>
          <!-- <p class="custom-notification-content" style="color:#fff;">Extra Tokens : <?php echo   $extra; ?></p> -->
        <?php
          // }
          ?>
        </div>
      </div>
      <div class="custom-close"></div>
    </div>
  </section>
  <?php
}
  ?>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info" style="    margin-top: -75px;">
            <img src="../img/LOGO_FOOTER.png">            
          </div>

           
          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Jobseeker</h4>
            <ul>
              <li><a href="../jobseeker/register.php">Create a Jobseeker Profile</a></li>
              <li><a href="../jobseeker/login.php">Jobseeker Login</a></li>
              <li><a href="../employer/overview.php#jobseeker-overview">Jobseeker Overview</a></li>
              
             
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Employers</h4>
            <ul>
              <li><a href="../employer/register.php">Create an Employer Account</a></li>
              <li><a href="../employer/login.php">Employer Login</a></li>
              <li><a href="../employer/overview.php#employer-overview">Employer Overview</a></li>
              <li><a href="../jobseeker/understanding.php">Understanding Jobseeker Profile</a></li>
            </ul>
          </div>

<!--           <div class="col-lg-3 col-md-6 footer-links">
            <h4>Agency</h4>
            <ul>
              <li><a href="../agency.php">Agency</a></li>          
            </ul>
          </div> -->
          <div class="col-md-6 footer_job">
            <h1 style="font-size: 13vw;">Jobs</h1>
          </div>

         </div>
      </div>
    </div>
<div class="col-md-12" style="background-color: #002d5e;">
    <div class="container">
      <div class="row">
      <div class="col-md-6 copyright" style="text-align: left;">
       Find Work Australia
      </div>
      <div class="col-md-6 credits" style="text-align: right;">
           <i class="fa fa-facebook" aria-hidden="true"></i>
           <i class="fa fa-twitter" aria-hidden="true"></i>
           <i class="fa fa-google-plus" aria-hidden="true"></i>
           <i class="fa fa-instagram" aria-hidden="true"></i>
           <i class="fa fa-wifi" aria-hidden="true"></i>
      </div>
    </div>
    </div>
  </div>
  </footer>

  <!-- JavaScript Libraries -->
<script src="../js/jquery-3.3.1.min.js"></script>
 <script src="../js/jquery.Jcrop.min.js"></script>
  <script src="../lib/jquery/jquery.min.js"></script>
  <script src="../lib/jquery/jquery-migrate.min.js"></script> 
  <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../lib/easing/easing.min.js"></script> 
  <script src="../lib/mobile-nav/mobile-nav.js"></script> 
  <script src="../lib/wow/wow.min.js"></script> 
  <script src="../lib/waypoints/waypoints.min.js"></script> 
  <script src="../lib/counterup/counterup.min.js"></script> 
  <script src="../lib/owlcarousel/owl.carousel.min.js"></script> 
  <script src="../lib/isotope/isotope.pkgd.min.js"></script> 
  <script src="../lib/lightbox/js/lightbox.min.js"></script> 
  
  <script src="../contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="../js/main.js"></script>  
  <script src='../js/jquery.min.js'></script>
  <script src='../js/bootstrap.min.js'></script>

    <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&libraries=places&callback=initAutocomplete"
        async defer></script>

  <script src="js/main.js"></script>
  <style>
@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600');
 .custom-social-proof {
   position: fixed;
   bottom: 20px;
   left: 20px;
   z-index: 9999999999999 !important;
   font-family: 'Open Sans', sans-serif;
}
 .custom-social-proof .custom-notification {
   width: 250px;
   border: 0;
   text-align: left;
   z-index: 99999;
   box-sizing: border-box;
   font-weight: 400;
   border-radius: 6px;
   box-shadow: 2px 2px 10px 2px rgba(11, 10, 10, 0.2);
   background-color: green;
   position: relative;
   cursor: pointer;
}
 .custom-social-proof .custom-notification .custom-notification-container {
   display: flex !important;
   align-items: center;
   height: 45px;
}
 .custom-social-proof .custom-notification .custom-notification-container .custom-notification-image-wrapper img {
   max-height: 75px;
   width: 90px;
   overflow: hidden;
   border-radius: 6px 0 0 6px;
   object-fit: cover;
}
 .custom-social-proof .custom-notification .custom-notification-container .custom-notification-content-wrapper {
   margin: 0;
   height: 100%;
   color: gray;
   padding-left: 20px;
   padding-right: 20px;
   border-radius: 0 6px 6px 0;
   flex: 1;
   display: flex !important;
   flex-direction: column;
   justify-content: center;
}
 .custom-social-proof .custom-notification .custom-notification-container .custom-notification-content-wrapper .custom-notification-content {
   font-family: inherit !important;
   margin: 0 !important;
   padding: 0 !important;
   font-size: 14px;
   line-height: 16px;
}
 .custom-social-proof .custom-notification .custom-notification-container .custom-notification-content-wrapper .custom-notification-content small {
   margin-top: 3px !important;
   display: block !important;
   font-size: 12px !important;
   opacity: 0.8;
}
 .custom-social-proof .custom-notification .custom-close {
   position: absolute;
   top: 8px;
   right: 8px;
   height: 12px;
   width: 12px;
   cursor: pointer;
   transition: 0.2s ease-in-out;
   transform: rotate(45deg);
   opacity: 0;
}
 .custom-social-proof .custom-notification .custom-close::before {
   content: "";
   display: block;
   width: 100%;
   height: 2px;
   background-color: #fff;
   position: absolute;
   left: 0;
   top: 5px;
}
 .custom-social-proof .custom-notification .custom-close::after {
   content: "";
   display: block;
   height: 100%;
   width: 2px;
   background-color: #fff;
   position: absolute;
   left: 5px;
   top: 0;
}
 .custom-social-proof .custom-notification:hover .custom-close {
   opacity: 1;
}
 
  </style>
  <script>
     setInterval(function(){ $(".custom-social-proof").stop().slideToggle('slow'); }, 800000);
      $(".custom-close").click(function() {
        $(".custom-social-proof").stop().slideToggle('slow');
      });
  </script>

<script>
  if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>

</body>
</html>