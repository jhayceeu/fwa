<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Find Work Australia</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <!-- <link href="img/favicon.png" rel="icon"> -->
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  
  <link rel="stylesheet" href="css/croppie.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <!-- <link rel='stylesheet' href='css/bootstrap1.min.css'>  -->
  <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
  <link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="index.php" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <?php
        if(isset($_SESSION['jobseeker_id']))
        {
          ?>
          <ul>
          <li><a href="#">Home</a></li> 
          
          <li><a href="jobseeker/profile.php">My Profile</a></li> 
          <li><a href="includes/logout.php">Logout</a></li> 
        </ul>
          <?php
        }
     else  if(isset($_SESSION['employer_id']))
        {
          ?>
          <ul>
          <li><a href="employer/home.php">Home</a></li> 
          
          <li><a href="employer/account.php">My Account</a></li> 
          <li><a href="includes/logout.php">Logout</a></li> 
        </ul>
          <?php
        }
        else
        {
          

        ?>
      <ul>
          <li><a href="index.php">Home</a></li> 
          <li class="drop-down"><a href="">Jobseeker</a>
            <ul>
              <li><a href="jobseeker/login.php">Login</a></li>
              <li><a href="employer/overview.php#jobseeker-overview">Jobseeker Overview</a></li>      
              <li><a href="jobseeker/register.php">Jobseeker Registration</a></li>
              <li><a href="benefits.php">Jobseeker Benefits</a></li>
            
            </ul>
          </li>
          <li class="drop-down"><a href="">Employer</a>
            <ul>
                <li><a href="employer/login.php">Login</a></li>      
                <!-- <li><a href="employee_registration.php">Employer Registration</a></li>  -->
              <li><a href="employer/overview.php#employer-overview">Employer Overview</a></li>
              <li><a href="employer/register.php">Employer Registration</a></li>
              <li><a href="jobseeker/understanding.php">See a Jobseeker Profile</a></li>
            
            </ul>
          </li>
<!--           <li class="drop-down"><a href="">Agency</a>
            <ul>
              <li><a href="agency.php">Agency Login</a></li>
            </ul>
          </li> -->

          <!-- <li><a href="fwaticket/index.php" target="_blank">Request a Ticket</a>
            
          </li> -->

          <li><a href="privacy-policy.php">Privacy Policy</a></li>

          <li><a href="contact.php">Contact</a>
          </li>
          <!-- <li><a href="login.html">Login</a></li> 
          <li><a href="#intro">Registration</a></li>  -->
        </ul>
        <?php
        }
        ?>
      
      
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->