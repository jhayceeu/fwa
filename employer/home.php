<?php 
session_start();
include_once('../includes/config.php');

// SQL QUERY FOR EMPLOYER PROFILE
date_default_timezone_set("Australia/Sydney");

$category=$tag=$location=$age="";
$search_id="";
if(!isset($_SESSION['employer_id']))
{
$_SESSION['msg']="Please login to view results";
header('location:login.php');
}
$job_keyword=$location=$sql_search=$age="";

if(isset($_SESSION['job_keyword'])&& !isset($_SESSION['msg']))
{
   $job_keyword=$_SESSION['job_keyword'];
   $location=$_SESSION['location'];
   $category=$_SESSION['category'];
   $tag=$_SESSION['tag'];
   $age=$_SESSION['age_braket'];


    if($category!="")
    {
      $n_array=explode(',',$category);
    }
$extra="";

    if($location!="")
    {
      if($extra=="")
          {
            $extra=" where (live_in in (select id from tbl_towns where name like '%".$location."%'))";
          }
          else
          {  
            $extra=$extra." and (live_in in (select id from tbl_towns where name like '%".$location."%'))";
          }
     
    }

    if($age!="")
    {
      if($extra=="")
      {
        $extra=" where (age_braket like '%".$age."%')";
      }
      else
      {
        $extra=$extra." and (age_braket like '%".$age."%')";
      }
     
    }
    if($job_keyword!="")
    {
      if($extra=="")
      {
        $extra=" where ( no_qualification in (select id from tbl_industry where industry_name like '%".$job_keyword."%')) ";
        // $extra=" where (bio like '%".$job_keyword."%' or visa_type like '%".$job_keyword."%'  or qualification like '%".$job_keyword."%' or no_qualification_other like '%".$job_keyword."%' or no_qualifiaction_any like '%".$job_keyword."%' or no_qualification_description like '%".$job_keyword."%' or university_qualification_other like '%".$job_keyword."%' or university_qualifiaction_any like '%".$job_keyword."%' or university_qualification_description like '%".$job_keyword."%' or some_qualification_other like '%".$job_keyword."%' or some_qualifiaction_any like '%".$job_keyword."%' or some_qualification_description like '%".$job_keyword."%')";
      }
      else
      {
        $extra=$extra." and ( no_qualification in (select id from tbl_industry where industry_name like '%".$job_keyword."%')) ";
        // $extra=$extra." and (bio like '%".$job_keyword."%' or visa_type like '%".$job_keyword."%'  or qualification like '%".$job_keyword."%' or no_qualification_other like '%".$job_keyword."%' or no_qualifiaction_any like '%".$job_keyword."%' or no_qualification_description like '%".$job_keyword."%' or university_qualification_other like '%".$job_keyword."%' or university_qualifiaction_any like '%".$job_keyword."%' or university_qualification_description like '%".$job_keyword."%' or some_qualification_other like '%".$job_keyword."%' or some_qualifiaction_any like '%".$job_keyword."%' or some_qualification_description like '%".$job_keyword."%')";
      }
     
    }

    if($category!="")
    {
       // $extra="";
        if(in_array('NQ',$n_array))
        {
          if($extra=="")
          {
               $extra=" where no_qualification<>''";
          }
          else
          {
            $extra=$extra." or no_qualification<>''";
          }
        }
        if(in_array('SQ',$n_array))
        {
          if($extra=="")
          {
            $extra=" where some_qualification<>''";
          }
          else
          {
            $extra=$extra." or some_qualification<>''";
          }
        }
        if(in_array('UM',$n_array))
        {
          if($extra=="")
          {
            $extra=" where university_qualification<>''";
          }
          else
          {
            $extra=$extra." or university_qualification<>''";
          }
        }
      }

        if($tag!="")
        {
          if($extra=="")
          {
            $extra=" where (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
            // $extra=" where ( no_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or  some_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or university_qualification in (select id from tbl_jobs where job_name like '%".$tag."%') or experience_work in (select id from tbl_jobs where job_name like '%".$tag."%') )";
        
          }
          else
          { 
            $extra=$extra." and (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
            // $extra=$extra." and ( no_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or  some_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or university_qualification in (select id from tbl_jobs where job_name like '%".$tag."%') or experience_work in (select id from tbl_jobs where job_name like '%".$tag."%') ) ";
       
          }
          }
        

          $sql_search="select * from tbl_jobseeker ".$extra;  
          if($extra=="") 
          {
            $sql_search=$sql_search." where work_status!=2";
          }
          else
          {
            $sql_search=$sql_search." and work_status!=2";
          }
       
  unset($_SESSION['job_keyword']);
  unset($_SESSION['tag']);
  unset($_SESSION['location']);
  unset($_SESSION['age_braket']);
  unset($_SESSION['category']);
}

if(isset($_POST['search']))
{
  $tag=$_POST['tag'];
  if(isset($_POST['checkAll']))
  {
    $category=implode(",",$_POST['checkAll']);
    if($category!="")
    {
      $n_array=explode(',',$category);
    }
  }

  if(isset($_POST['checkAlltag']))
  {
    $location=implode(",",$_POST['checkAlltag']);
    if($location!="")
    {
      $n_array1=explode(',',$location);
    }
  }
  
   $job_keyword=$_POST['job_keyword'];
    if(isset($_POST['age_braket']))
    {
       $age=$_POST['age_braket'];
    }
    $extra="";

    
    if($job_keyword!="")
    {
      if($extra=="")
      {
        // $extra=" where ( no_qualification in (select id from tbl_industry where id like '%".$job_keyword."%')) ";
        $extra=" where (no_qualification like '%".$job_keyword."%') ";
        // $extra=" where (bio like '%".$job_keyword."%' or visa_type like '%".$job_keyword."%'  or qualification like '%".$job_keyword."%' or no_qualification_other like '%".$job_keyword."%' or no_qualifiaction_any like '%".$job_keyword."%' or no_qualification_description like '%".$job_keyword."%' or university_qualification_other like '%".$job_keyword."%' or university_qualifiaction_any like '%".$job_keyword."%' or university_qualification_description like '%".$job_keyword."%' or some_qualification_other like '%".$job_keyword."%' or some_qualifiaction_any like '%".$job_keyword."%' or some_qualification_description like '%".$job_keyword."%')";
      }
      else
      {
        // $extra=$extra." and ( no_qualification in (select id from tbl_industry where id like '%".$job_keyword."%')) ";
        $extra=$extra." and (no_qualification like '%".$job_keyword."%') ";
        // $extra=$extra." and (bio like '%".$job_keyword."%' or visa_type like '%".$job_keyword."%'  or qualification like '%".$job_keyword."%' or no_qualification_other like '%".$job_keyword."%' or no_qualifiaction_any like '%".$job_keyword."%' or no_qualification_description like '%".$job_keyword."%' or university_qualification_other like '%".$job_keyword."%' or university_qualifiaction_any like '%".$job_keyword."%' or university_qualification_description like '%".$job_keyword."%' or some_qualification_other like '%".$job_keyword."%' or some_qualifiaction_any like '%".$job_keyword."%' or some_qualification_description like '%".$job_keyword."%')";
      }
     
    }

    if($age!="")
    {
      if($extra=="")
      {
        $extra=" where (age_braket like '%".$age."%' )";
      }
      else
      {
        $extra=$extra." and (age_braket like '%".$age."%' )";
      }
     
    }

    if($category!="")
    {
       // $extra="";
        if(in_array('NQ',$n_array))
        {
          if($extra=="")
          {
               $extra=" where no_qualification<>''";
          }
          else
          {
            $extra=$extra." or no_qualification<>''";
          }
        }
        if(in_array('SQ',$n_array))
        {
          if($extra=="")
          {
            $extra=" where some_qualification<>''";
          }
          else
          {
            $extra=$extra." or some_qualification<>''";
          }
        }
        if(in_array('UM',$n_array))
        {
          if($extra=="")
          {
            $extra=" where university_qualification<>''";
          }
          else
          {
            $extra=$extra." or university_qualification<>''";
          }
        }
      }

        if($tag!="")
        {
          // if($extra=="")
          // {
           
          //   $extra=" where ( no_qualification in (".$tag.")  or  some_qualification in (".$tag.")  or university_qualification in (".$tag.") (select id from tbl_jobs where job_name like '%".$tag."%'))";
          // }
          // else
          // {  
          //   $extra=$extra." and ( no_qualification in (".$tag.")  or  some_qualification in (".$tag.")  or university_qualification in (".$tag.") (select id from tbl_jobs where job_name like '%".$tag."%')) ";
          // }
          if($extra=="")
          {
            // $extra=" where ( no_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or  some_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or university_qualification in (select id from tbl_jobs where job_name like '%".$tag."%') or experience_work in (select id from tbl_jobs where job_name like '%".$tag."%') )";
          //  $extra=" where ( no_qualification in (".$tag.")  or  some_qualification in (".$tag.")  or university_qualification in (".$tag.") (select id from tbl_jobs where job_name like '%".$tag."%'))";

            $extra=" where (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
          }
          else
          { 
            // $extra=$extra." and ( no_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or  some_qualification in (select id from tbl_jobs where job_name like '%".$tag."%')  or university_qualification in (select id from tbl_jobs where job_name like '%".$tag."%') or experience_work in (select id from tbl_jobs where job_name like '%".$tag."%') ) ";
          //  $extra=$extra." and ( no_qualification in (".$tag.")  or  some_qualification in (".$tag.")  or university_qualification in (".$tag.") (select id from tbl_jobs where job_name like '%".$tag."%')) ";
            $extra=$extra." and (bio like '%".$tag."%' or visa_type like '%".$tag."%')";
          }
          }

          if($location!="")
        {

          // $locarray = array('0');

          // foreach($location as $loc){
          //   $locarray[] = 'name LIKE %'.$loc.'%';
          // }

          // if($extra=="")
          // {
          //   $extra=" where live_in in (select id from tbl_towns where '".implode(" OR ", $locarray)."')";
          // }
          // else
          // {  
          //   $extra=$extra." and live_in in (select id from tbl_towns where '".implode(" OR ", $locarray)."')";
          // }

          if($extra=="")
          {
           
            $extra=" where (live_in in (".$location.")  )";
          }
          else
          {  
            $extra=$extra." and (live_in in (".$location.") ) ";
          }
        }



          $sql_search="select * from tbl_jobseeker ".$extra; 
          if($extra=="") 
          {
            $sql_search=$sql_search." where work_status!=2";
          }
          else
          {
            $sql_search=$sql_search." and work_status!=2";
          }
  }



include_once('../includes/external_header.php');

?>



<style>
  #textBusiness {
    font-size: 18px;
  }
  #textPrimaryBusiness {
    font-size: 15px;
  }
</style>

<!DOCTYPE html>
<html lang="en">

<body>
  <div class="col-md-12" class="login_banner">
    <div class="row">      
      <img src="../img/login.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position: absolute;top:140px;">
            <p>FIND A JOBSEEKER</p>
            <h2>Search Candidate </h2>           
        </div>
      </div>
    </div>
  </div>


  <div class="col-md-12 emp_proflie">
    <div class="container">
      <div class="row">       
      
            <div class="col-md-12">
              <h2 style="margin-top:20px;"><?php echo "WELCOME ".$_SESSION['employer_name']; ?></h2>
              <div class="col-md-12 ">
              <h5>IMPORTANT!</h5>
              <p>Jobseekers set a status in their profiles.</p>
              <p>“Ready to start work” costs 2 token to unlock contact details.</p>
              <p>“Employed but looking for a change” costs 1 token to unlock contact details.</p>
              <p>If their profile is set to “Employed” their profile will be hidden.</p>
              <p>If you contact a Jobseeker and they are employed or unable to take your job, please submit a token refund request. If Jobseekers fail to update their profile may result in a poor rating and/or their account suspended.</p>
              </div>  
            </div>

            </div>

        </div>


<!-- ------------------------- -->
<div class="container">

           <div class="job-search">

              <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <!-- <li class="nav-item">
                  <a class="nav-link active py-3" id="pills-job-tab" data-toggle="pill" href="#pills-job" role="tab" aria-controls="pills-job" aria-selected="true">Find A Job</a>
                </li> -->
       
              </ul>

              <div class="tab-content bg-white p-4 rounded" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-job" role="tabpanel" aria-labelledby="pills-job-tab">
                  <form action="#" method="post">
                  <div class=" find_candidate">
                      <a class="btn-services scrollto abt_btn">FIND A CANDIDATE </a>
                  </div>
                    <div class="row">
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      &nbsp;
                      <label class="reg_label">Industry</label>
<!-- 
                      <input type="text" name="job_keyword" class="form-control" value="<?php if($job_keyword!="") echo $job_keyword; ?>" placeholder="Industry"> -->

                      <select id="primary_business_industry" name="job_keyword" class="search_box">
                      <option value="" selected="selected">Select</option>
                          <?php 
                            $result = mysqli_query($con,"SELECT DISTINCT industry_name,id from tbl_industry ORDER BY industry_name ASC");
                                while($row = mysqli_fetch_array($result))
                                      { $job_keyword= $row['id']  ?>
                                <option value="<?php if($job_keyword!="") echo $job_keyword; ?>"><?php echo $row['industry_name'];?></option>                                                    
                                    <?php }  ?> 
                                <option value="others">Others</option>
                      </select>
                      </div>
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                    
                      <label class="reg_label">Tag</label>
                      <input class="form-control" type="text" name="tag" placeholder="Tag" >
                      <!-- <input type="text" name="tag" class="form-control" value="<?php //if($tag!="") echo $tag; ?>" placeholder="Tag"> -->
                      </div>  

          <!-- tag -->
              <div class="col-md-3">              
                  <label class="reg_label">Location</label>

                   <div class="selectbox1 selectwrap1">
                      <div class="selectbox-title1 form-control">
                        <span class="plzselect1">Location</span>          
                      </div>
                  <div class="selectbox-content1">
                    <div class="input-group">

                      <input class="form-control" type="text"  name="location" placeholder="Search" id="box1">
                    </div>
                      <ul class="ul-list1">
                      <?php
                      $sql_town="select * from tbl_towns";
                      $res_town=$con->query($sql_town);
                      while($row_town=$res_town->fetch_assoc())
                      {            
                      ?>
                                  <li style="padding-left: 10px;"><input class="checkbox-custom1" name="checkAlltag[]" type="checkbox" id="test1<?php echo $row_town['id']; ?>" value="<?php echo $row_town['id']; ?>" /><label for="test1<?php echo $row_town['id']; ?>" class="checkbox-custom-label checkbox"><?php echo $row_town['name']; ?></label></li>
                      <?php
                      }
                      ?>          
                      </ul>
            <!-- <div class="loadbutton"><button class="btn btn-block btn-rounded btn-primary" id="button" onclick="buttonclick()">Load more items...</button></div> -->
                </div>
               
            </div>
              </div>
          <!-- </div> -->
   
                    <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      <label class="reg_label">Age</label>
                      &nbsp;
                      <select id="age_braket" name="age_braket"  class="form-control"  >
                        <option value="" selected="selected" disabled>Age</option>
                        <option value="18-25"<?php  if($age=="18-25"){ echo "selected"; }?>>18 - 25</option>
                        <option value="25-30"<?php  if($age=="25-30"){ echo "selected"; }?>>25 - 30</option>
                        <option value="35-40"<?php  if($age=="35-40"){ echo "selected"; }?>>35 - 40</option>
                        <option value="40-45"<?php  if($age=="40-50"){ echo "selected"; }?>>40 - 50</option>
                        <option value="50+"<?php  if($age=="50+"){ echo "selected"; }?>>50+</option>
                      </select>
                    </div>
          <!-- tag end -->
                   
                      <div class="col-md-6 col-lg-3 mb-3 mb-lg-0">
                      <!-- <br> -->
                        <input type="submit" onclick="set_focus()" name="search" class="btn-services scrollto search_btn" value="Search" style="border: none; background-color:#002d5e;">
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
</div>


         
      </div>
    </div>
  </div>

<?php
 $sql_search;
if($sql_search!="")
{
    $res=$con->query($sql_search);


?>


<div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-start text-left mb-5">
          <div class="col-md-9" data-aos="fade">
            <h2 class="font-weight-bold text-black">Search Results</h2>
          </div>
          <!-- <div class="col-md-3" data-aos="fade" data-aos-delay="200">
            <a href="#" class="btn btn-primary py-3 btn-block"><span class="h5">+</span> Post a Job</a>
          </div> -->
        </div>
        <?php 
        $i=0;
        while($row=$res->fetch_assoc())
        {
          $_SESSION['qry']=$sql_search;
            $loc="";
            $sql_location="select * from tbl_towns where id='".$row['live_in']."'";
            $res_location=$con->query($sql_location);
            if($row_location=$res_location->fetch_assoc())
            {
                $loc=$row_location['name'];

            }

            $job="";
            if($row['no_qualification']!="")
            {
            $sql_job="select * from tbl_industry where id in (".$row['no_qualification'].")";
            $res_job=$con->query($sql_job);
            while($row_job=$res_job->fetch_assoc())
            {
                if($job=="")
                {
                    $job=$row_job['industry_name'];
                }
                else
                {
                $job=$job."<br>".$row_job['industry_name'];
                }

            }
          }



        ?>

        <div class="row" data-aos="fade" id="result_div">
         <div class="col-md-12">

           <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">

              <div class="mb-4 mb-md-0 mr-5">
               <div class="job-post-item-header d-flex align-items-center">
                 <h2 class="mr-3 text-black h4"><?php echo $row['first_name']." ".$row['last_name']; ?></h2>
                 <?php
                 if($row['work_status']=="1")
                 {
                   ?>
               <img src="../img/one-token.png"  height="30" width="30">
                  <?php
                }
              
                 else  
                 {
                   ?>
                     <img src="../img/two-token.png"  height="30" width="30">
                
                 <?php
                 }
                 ?>

                 <?php
                 if($row['gov_support']=="Yes")
                 {
                   ?>
                   <label>
                  
                  <!-- &#9989; MAY COME WITH GOVERNMENT SUPPORT</label> -->
                 &nbsp;
                 <i class="fa fa-university" aria-hidden="true"></i>
                  MAY COME WITH GOVERNMENT SUPPORT

                 <?php
                 }
              
                 else 
                 {
                   ?>
                      <!-- <input type="checkbox" name="gov_app" readonly/> -->
                    <?php
                 }
                 ?>
              
               </div>
               
               <div><span class="fl-bigmug-line-big104"></span> <span><?php echo $loc; ?></span></div>
               <div class="job-post-item-body d-block d-md-flex">
                 <div class="mr-3"><span ></span> <a href="#"><?php echo $job; ?></a></div>
              
            
            
               </div>
               <div class="mr-3" style="margin-top:5px;"><span >Bio:</span> <a href="#"><?php echo $row['bio'];  ?></a></div> 
              <div class="mr-3"> 
           

               <?php
                 if($row['gender']=="male")
                 {
                   ?>
                   <i class="fa fa-male" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                  <?php
                 }
              
                 else 
                 {
                   ?>
                     <i class="fa fa-female" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                
                 <?php
                 }
                 ?>
              </div> 

               <?php 
                 if($row['visa_type']!="")
                 {
                     ?>
                  <div class="badge-wrap">
                
               
               
                
                 </div>
                </br> <span class="bg-primary text-white badge py-2 px-3"><?php echo $row['visa_type']; ?></span>
                 <?php 
                 }
                 ?>
              </div>

              <div class="ml-auto">
                <!-- <a href="#" class="btn btn-secondary rounded-circle btn-favorite text-gray-500"><span class="icon-heart"></span></a> -->
                <a target="_blank" href="candidate.php?id=<?php echo $row['id']; ?> " class="btn btn-primary py-2">View</a>
              </div>
           </div>

         </div>
        </div>
        <?php
        $i++;


        }
        ?>

        <center><?php echo $i." results found"; ?></center>

        

      

      </div>
    </div> 


<?php
  }
    ?>  

  <?php include_once('../includes/external_footer.php'); ?>


<style>
.selectbox-title1.form-control { padding-left: 16px;}

.selectbox1 { width:100%; display:inline-block;}

.selectbox-content1 .input-group {
    padding: 5px;
}

.selectbox-content1 { border:1px solid #d5d5d5; max-height:205px; overflow:auto; display:none; border-top: none;}
.selectbox-content1 ul { list-style:none; margin:0px; padding:0px; cursor: pointer;  position: relative; }
.selectbox-content1 ul li { padding-top:10px; width:100%; display:inline-block;}
.selectbox-content1 ul li:last-child { border-bottom:none;}
.selectbox-content1 ul li:first-child { border-top:none;}
.selectbox-content1 .input-group {padding: 5px;}


.selectwrap1 {
    position: relative;
    float: left;
    width: 100%;
}

.selectwrap1 {
  position: relative;
  float: left;
  width: 100%; }
  .selectwrap1:after {
    content: "\f0d7";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    text-align: center;
    line-height: 36px;
    position: absolute;
    width: 26px;
    height: 36px;
    background: white;
    right: 1px;
    top: 1px;
    pointer-events: none; }

</style>

<script>

  // $('#primary_business_industry').change(function(){
  //   alert($(this).val());
  // })

  // function val(selectObject) {
  //   // d = document.getElementById("primary_business_industry").value;
  //   d = selectObject.value;
  //   alert(d);
  // }
    /****Search Result ****/
   function set_focus()
   {
    $('#result_div').focus();
   }


   /****Dropdown toggle ****/
$(".selectbox-title1").click(function() {
  $(".selectbox-content1").slideToggle("slow");
});

$('#box1').keyup(function () {

        var valThis = $(this).val().toLowerCase();
        
        if (valThis == "") {
            $('.ul-list1 > li').show();
        } else {
            $('.ul-list1 > li').each(function () {
                var text = $(this).text().toLowerCase();
              
                (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
            });
        };
    });
$('.fa-close1').click(function () {
        // Selects all elements of type text and clear all
        $('#box1').val('');
    });

/***Count Length of checkbox***/
var $checkboxes = $('.ul-list1 input[type="checkbox"]');

$checkboxes.change(function() {
  var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
  $('#edit-count-checked-checkboxes1').text(countCheckedCheckboxes);
});

if (countCheckedCheckboxes > 0) {
            $('.plzselect1').css('display', 'none');
            $('#edit-count-checked-checkboxes1').css('display', 'inline-block');
            $('.selected').css('display', 'inline-block');
        }
        else {
            $('.plzselect1').css('display', 'block');
            $('#edit-count-checked-checkboxes1').css('display', 'none');
            $('.selected').css('display', 'none');
        }


   </script>

