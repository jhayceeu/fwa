<?php 
session_start();
include_once('../includes/config.php');



include_once('../includes/external_header.php');
?>

<!DOCTYPE html>
<html lang="en">

<style>
.emp_profile h1,h2,h3,h4,h5,h6{
    
    font-family: 'Comfortaa', cursive;
    font-weight: 600;
}
p {
	font-family: 'Comfortaa', cursive;
    font-weight: 600;
}

.banner2{
  background-image: url(../img/employer-background2.jpg);
  height: 500px;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  top: -20px;
}
</style>

<body>


  <div class="col-md-12 emp_profile" class="banner">
    <div class="row">
      <img src="../img/employer-background.jpg" height="600" width="100%">
        <div class="container">
           <div class="intro-info" style="position:absolute;top:140px;">
              <p>FIND WORK IN AUSTRALIA</p>
              <h2>How it works?</h2>
              <div>
                <a href="#jobseeker-overview" class="btn-get-started scrollto">JOBSEEKERS</a>
                <a href="#employer-overview" class="btn-services scrollto">EMPLOYERS</a>
              </div>
              <h2 style="font-size: 10vw">About</h2>
           </div>
        </div>
    </div>
  </div>



  		<div class="col-md-12 working" id="employer-overview">
          <h2>Allowing businesses to hand pick staff without posting jobs – at a fraction of the price.</h2>
        </div>
        <div class="row about-container" style="margin-top: 60px;"> 
          <div class="col-lg-1">         
          </div>
          <div class="col-lg-6 content wow fadeInUp work_para">
            <p>
              Find Work Australia is the result of years of recruitment. We found that posting jobs that regularly needing filling cost a lot of money and took a great deal of time.
			Once posted, you had to wait for jobseekers to come to you – perhaps even ones that may not be the people you are looking for.
			We resolved to create a simple, fast and effective solution for both Jobseekers and Employers alike. Allowing people to create their profiles for free and giving Employers the ability to search based on any parameter will help ensure you are connected with the people you need.
            </p>
            <div class="col-md-12" style="margin-top: 50px;">
              <div class="row">
                <a href="../employer/register.php" class="btn-services scrollto service_btn">CREATE YOUR FREE ACCOUNT</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 background order-lg-2 order-1 wow fadeInUp">
            <img src="../img/job_recruitment.png" class="img-fluid" alt="">
          </div>
           <!-- <div class="col-lg-2"></div> -->
        </div>


         <div class="row about-extra">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-4 wow fadeInUp">
            <img src="../img/job_recruitment2.png" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0 work_para">
          <h2 style="margin-top: 20px;">Get Started</h2>            
            <p style="margin-top: 20px;">
              GET MY FREE PROFILE
            </p> 
            <div class="col-md-12" style="margin-top: 20px;">
              <div class="row">
                <a href="../jobseeker/register.php" class="btn-get-started scrollto banner_text_btn">Create a Jobseeker Profile</a>
              </div>
            </div>

            <p style="margin-top: 20px;">
              START SEARCHING
            </p> 
            <div class="col-md-12" style="margin-top: 20px;">
              <div class="row">
                <a href="../employer/register.php" class="btn-get-started scrollto banner_text_btn">Create an Employer Account</a>
              </div>
            </div>

            <p style="margin-top: 20px;">
              I NEED HELP WITH SOMETHING
            </p> 
            <div class="col-md-12" style="margin-top: 20px;">
              <div class="row">
                <a href="../fwaticket/index.php" class="btn-get-started scrollto banner_text_btn">Open a Ticket</a>
              </div>
            </div> 

          </div>
        </div>


  <div class="col-md-12" class="login_banner" id="jobseeker-overview">
    <div class="row">
      <img src="../img/circles-v3.jpg" height="500" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <h2 style="font-family: 'Comfortaa', cursive;">
            Benefits for Jobseekers</h2>
            <div>
              <br>
              <a href="../jobseeker/login.php" class="btn-get-started scrollto" style="width:300px;text-align:center;">LOGIN TO SEE ALL BENEFITS</a>

            </div>

         </div>

      </div>
    </div>
  </div>

    <main id="main"> 
    <section id="about">
      <div class="container" id="jobseeker-step">
        <header class="section-header">
            <h3>Create your free profile and get found today!</h3>
            <h5 style="text-align:justify;font-family: 'Comfortaa', cursive;">Find Work Australia are all about getting found quicking and easily. Your free profile makes it easy for employers to see what you want to do, where you are and how long you will be there.<br><br>
            Your free Jobseeker profile comes with benefits! We’ve teamed up with some amazing businesses to provide you with some great deals to get you started.
            <br><br>
            You can easily updated your profile at any time and add a short description, cover letter and image. You can set your status to “Ready to start work”, “Employed but looking for a change” or “Employed”. If you find work, simply set your profile to “Employed” to remove yourself from searches until you are ready.
            <br><br>
            Make sure you have an email and an Australian mobile number to help get yourself into work as soon as possible.
            <br><br>
            Thank you for joining Find Work Australia!
          </h5>
        </header>


        </div>
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;margin-top:30px;">
             <a href="../jobseeker/register.php" class="btn-services scrollto abt_btn">CREATE YOUR FREE PROFILE</a>
           </div>
      </section>

          <!--==========================
      Contact Section
    ============================-->
<?php include_once('../includes/external-pre-footer.php');?>

  </main>


<?php include_once('../includes/external_footer.php'); ?>
</body>
</html>