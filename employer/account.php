<?php 
session_start();
include_once('../includes/config.php');

if(!isset($_SESSION['employer_id'])){
          header("Location: login.php");
  }


// SQL QUERY FOR EMPLOYER PROFILE
date_default_timezone_set("Australia/Sydney");
$company_business_name=$company_trading_name=$other_industry="";
if(isset($_SESSION['employer_id']))
{
$sql="select * from tbl_employer where id='".$_SESSION['employer_id']."'";
  $result=$con->query($sql);
  while($row=$result->fetch_assoc())
  {

   $company_business_name=addslashes($row['company_business_name']);
 
   $logo=addslashes($row['image']);

   $contact_name=addslashes($row['contact_name']);
   $email=addslashes($row['email']);
   $mobile=addslashes($row['mobile']);
   $description=addslashes($row['description_products']);
   $business_address=addslashes($row['business_address']);

   $primary_business_industry=addslashes($row['primary_business_industry']);
   $other_industry=addslashes($row['other_industry']);
   $id=$row['id'];
 
  }
}




if(isset($_POST['submit']))
{
  $business="";
    $name=$_POST['name'];
    $password=$_POST['password'];
    $sql="select * from tbl_employer where email='".$name."' and password='".md5($password)."'";
    $result=$con->query($sql);
    if($row=$result->fetch_assoc())
    {
        
    $_SESSION['employer_id']=$row['id'];
    $_SESSION['employer_name']=$row['contact_name'];
    $business=$row['company_business_name'];

    header('location:home.php');
    }
    else
    {        
      $_SESSION['error']= "invalid email or password";
    }
}


if(isset($_POST['subscribe']))
{
  $token=$_POST['token'];
  // $sql_tk="select * from tbl_package where package_id='".$token."'";
  // $res_tk=$con->query($sql_tk);
  // if($row_tk=$res_tk->fetch_assoc())
  // {
  //   $exp=$row_tk['expiry_days']*30;
  //   $tdate= date("Y-m-d");
  //   $edate=date('Y-m-d', strtotime($tdate. ' + '.$exp.' days'));
  //   $sql_sub="insert into tbl_subscription(employer_id,subscription_date,expiry_date,token_id) values('".$_SESSION['employer_id']."','".$tdate."','".$edate."','".$token."')";
  //   $con->query($sql_sub);
  // }
  header('location:payment.php?pid='.$token.'&eid='.$_SESSION['employer_id']);
}

// Update and Edit Profile Query

$image="";
if(isset($_POST['submitEditProfile']))
 {
 $company_business_name=addslashes($_POST['company_business_name']);

 $image=$_POST['file_location'];
 $image1=basename($image);

 $contact_name=addslashes($_POST['contact_name']);
 $email=addslashes($_POST['email']);
 $mobile=addslashes($_POST['mobile']);

 $description=addslashes($_POST['description']);
 $business_address=addslashes($_POST['business_address']);
//  if(isset($_POST['other']))
//  {

 
 $primary_business_industry=addslashes($_POST['primary_business_industry']);

 if($primary_business_industry=="others")
 {
  $other_industry=addslashes($_POST['other']);
 }
 else
 {
  $other_industry="";
 }
//  }
//  else
//  {
//   $other_industry="";
//  }
//  $other_industry=addslashes($_POST['other']);


  if($image!="")
 {
  $sql="update tbl_employer set company_business_name='".$company_business_name."',image='".$image1."',contact_name='".$contact_name."', email= '".$email."',mobile= '".$mobile."', description_products='".$description."',business_address='".$business_address."',primary_business_industry='".$primary_business_industry."',other_industry='".$other_industry."'  where id='".$_SESSION['employer_id']."'";
 $_SESSION['success']="Your profile updated successfully";
 }
 else
 {
  $sql="update tbl_employer set company_business_name='".$company_business_name."', contact_name='".$contact_name."', email='".$email."',mobile='".$mobile."', description_products='".$description."',business_address='".$business_address."',primary_business_industry='".$primary_business_industry."',other_industry='".$other_industry."'  where id='".$_SESSION['employer_id']."'";

 }
 if($con->query($sql))
 {
   
  $_SESSION['success']="Your profile updated successfully";
   if($image)
   {
    if (!file_exists('../admin/logo/'.$id.'/image'))
                  mkdir('../admin/logo/'.$id.'/image', 0777, true);
              // if(move_uploaded_file($_FILES['upload_image']['tmp_name'], 'admin/uploads/'.$id.'/image' . $_FILES['upload_image']['name']))
              // Your file
              $file = $image;
  
  // Open the file to get existing content
   $data = file_get_contents($file);
   $image1=basename($image);
  // New file
   $new = '../admin/logo/'.$id.'/image/'.$image1;
   $_SESSION['success']="Your profile updated successfully";
  
  // Write the contents back to a new file
  if(file_put_contents($new, $data))
          {
           // $imageName = $targetPath;
          //remove
          unlink($file);
          }
          else
          {
            $_SESSION['error']= "Error ";  
            }
          }
         $_SESSION['success']="Your profile updated successfully";
         header('location:account.php');
         
 }
else{
   $_SESSION['error']= "Error ";
}

}



 if(isset($_POST['update_password']))
 {
   $old_password=$_POST['old_password'];
   $new_password=$_POST['new_password'];
   $confirm_password=$_POST['confirm_password'];
   
  $sql="select * from tbl_employer where password='".md5($old_password)."' and id='".$_SESSION['employer_id']."'";
   $res=$con->query($sql);
   if($row=$res->fetch_assoc())
   {
     $sql_update="update tbl_employer set password='".md5($new_password)."' where id='".$_SESSION['employer_id']."'";
     if($con->query($sql_update))
     {
       $_SESSION['success']="Password updated successfully";
     }
   }
   else
   {
     $_SESSION['error']="Old password is incorrect";
   }
 }

// Request Refund Query
  if(isset($_POST['refund']))
 {
   $company_name=$_POST['company_name'];
   $contact_name=$_POST['contact_name'];
   $contact_number=$_POST['contact_number'];
   $token_contact=$_POST['token_contact'];
   $refund_reason=$_POST['refund_reason'];

   $sql="insert into tbl_refund(company_name,contact_name,contact_number,token_contact,refund_reason,employer_id) values('".$company_name."','".$contact_name."','".$contact_number."','".$token_contact."','".$refund_reason."','".$_SESSION['employer_id']."')";
   if($con->query($sql))
   {
    $_SESSION['success']="Refund request sent successfully!";
   }
   else
   {
    $_SESSION['error']="Failed to request refund";
   }
   
  }

include_once('../includes/external_header.php');

?>
<?php 
$job_keyword=$location=$sql_search=$age=$category="";
if(isset($_POST['search']))
{
    $job_keyword=$_POST['job_keyword'];
    $location=$_POST['location'];
    $age=$_POST['age_braket'];
    $tag=$_POST['tag'];
  
  if(isset($_POST['checkAlltag']))
  {
    $location=implode(",",$_POST['checkAlltag']);
    
  }

     $_SESSION['key']=$job_keyword;
     $_SESSION['category']=$category;
     $_SESSION['tag']=$tag;
     $_SESSION['location']=$location;
     $_SESSION['age_braket']=$age;
    header('location:../search.php');
}
?>

<!-- <style>
  #textBusiness {
    font-size: 18px;
  }
  #textPrimaryBusiness {
    font-size: 15px;
  }
</style> -->

<!DOCTYPE html>
<html lang="en">

<body>
  
  <div class="col-md-12" class="login_banner" >
    <div class="row">      
      <img src="../img/login.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position: absolute;top:140px;">
            <p>MANAGE YOUR PROFILE</p>
            <h2>Employer Account</h2>           
        </div>
      </div>
    </div>
  </div>


  <div class="col-md-12 emp_proflie">
    <div class="container">
      <div class="row">       
      
            <div class="col-md-12">
              <h2 style="margin-top:20px;"><?php echo "WELCOME ".$_SESSION['employer_name']; ?></h2>
              <div class="col-md-12">
              <h5>IMPORTANT!</h5>
              <p>Jobseekers set a status in their profiles.</p>
              <p>“Ready to start work” costs 2 token to unlock contact details.</p>
              <p>“Employed but looking for a change” costs 1 token to unlock contact details.</p>
              <p>If their profile is set to “Employed” their profile will be hidden.</p>
              <p>If you contact a Jobseeker and they are employed or unable to take your job, please submit a token refund request. If Jobseekers fail to update their profile may result in a poor rating and/or their account suspended.</p>
              </div>  
              <input type="button" name="search" data-toggle="modal" data-target="#myModal" class="btn-services scrollto search_btn" value="BUY TOKENS" style="border: none;">

            </div>
            <div class="col-md-10 token_fund">
              <h4>Request a token refund</h4>
               <!-- <input type="submit" name="search"  value="" > -->
               <a href="../fwaticket/index.php" target="_blank" class="btn-services scrollto search_btn" style="border: none;">LODGE A TICKET</a><br><br>
               <a href="../jobseeker/understanding.php" target="_blank" class="btn-services scrollto service_btn">Understanding a jobseeker profile</a><br><br>
            </div>

            </div>

        </div>
      </div>
    </div>



  <div class="site-section bg-light">
    <div class="container">
      <div class="row">
       
        <div class="col-md-12 col-lg-8 mb-5">

                    <?php
          if(isset($_SESSION['no_subscription'])){
            echo "
                <div class='alert alert-danger alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <h4><i class='icon fa fa-warning'></i> Error!</h4>
                ".$_SESSION['no_subscription']."
                </div>
            ";
            unset($_SESSION['no_subscription']);
            }
          if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Error! </h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Success!</h4>
                            ".$_SESSION['success']."
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
?>

          <form style="margin-top:20px;" action="" method="POST" class="p-5 bg-white"  enctype="multipart/form-data">

                    <div>
                        <?php 
                           if($logo!="")
                           { ?>
                        <img src="../admin/logo/<?php echo $id;?>/image/<?php echo $logo;?>" style="height:100px;width:100px;">
                        <?php
                         }
                         else
                         { ?>
                          <img src="../img/no-image.jpg" style="height:100px;width:100px;">
                        <?php } ?>
                    </div>           

            <div class="row form-group-fwa" >
              <div class="col-md-12 mb-3 mb-md-0" style="float:right;">
                <a onclick="editProfile()" value="Edit" id="edit" class="btn btn-primary" style="color: #26baee;float: right;background-color: #fff;"><i class="icon-large icon-pencil"></i> Edit Profile</a> 
              </div>
            </div> 
             
            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="BusinessName">Business name<b style="color:red;"> *</b></label>
                  <input type="text" id="company_business_name" name="company_business_name" value="<?php echo $company_business_name;?>" readonly class="form-control" autocomplete="off" required>
                  &nbsp;
              </div>
            </div>

            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
               <label class="font-weight-bold" for="Logo">Logo </label>
              
                <input type="file" name="upload_image" id="upload_image"  class="form-control" disabled />
                <small>Maximum logo size: 800kb</small>
                <input type="hidden" name="file_location" id="file_location">
                &nbsp;
              </div>
            </div>

            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="ContactName">Contact name<b style="color:red;"> *</b></label>
                <input type="text" id="contact_name" name="contact_name" class="form-control"  value="<?php echo  $contact_name;?>" autocomplete="off" readonly>
                &nbsp;
              </div>
            </div>

            
            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="Email">Email<b style="color:red;"> *</b></label>
                <input type="email" id="email" name="email" class="form-control"  value="<?php echo $email;?>" onchange="check(this.value)" readonly autocomplete="off" required>
                <small id="val" style="color:red;"></small>
                &nbsp;
              </div>
            </div>

            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="Mobile">Mobile<b style="color:red;"> *</b></label>
                  <input type="text" id="mobile" name="mobile" class="form-control"  value="<?php echo $mobile;?>" autocomplete="off" readonly required>
                  <small id="val" style="color:red;"></small>
                  &nbsp;
              </div>
            </div>
            
            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="Description">A brief description of the products and services as part of the profile<b style="color:red;"> *</b></label>
                <textarea id="description" name="description" class="form-control" readonly ><?php echo $description;?></textarea>
                &nbsp;
              </div>
            </div>

            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="Address">Address<b style="color:red;"> *</b></label>
                <input id="autocomplete" placeholder="Enter your address"  name="business_address"  class="form-control" autocomplete="off" type="text" value="<?php echo $business_address;?>" readonly required/>
                &nbsp;
              </div>
            </div>

            <div class="row form-group-fwa">
              <div class="col-md-12 mb-3 mb-md-0">
                <label class="font-weight-bold" for="BusinessIndustry">Primary business industry  <b > </b></label>
                <select id="primary_business_industry" name="primary_business_industry"  class="form-control"    onchange='checkvalue(this.value)'  disabled>
                  <option value="" selected="selected" disabled>Select</option>
                    <?php 
                      $result = mysqli_query($con,"SELECT  DISTINCT industry_name,id from tbl_industry");
                      while($row = mysqli_fetch_array($result))
                     {  ?>
                  <option value="<?php echo $row['industry_name']; ?>"  <?php if($row['industry_name']==$primary_business_industry) echo "selected"; ?>><?php echo $row['industry_name'];?></option>
                  <?php }  ?> 
                  <option value="others" <?php if($primary_business_industry=="others") echo "selected"; ?>>others</option>
                </select>
              </div>
            </div>

           <div id =""  <?php if($other_industry=="") { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
              <input type="text" name="other" value="<?php echo $other_industry;?>" id="other" class="form-control"  disabled />
           </div>

           <div class="row form-group-fwa">
              <div class="col-md-12">
                </br>  <input type="submit" value="Update"  name="submitEditProfile" id="submitEditProfile" class="btn btn-primary  py-2 px-5" hidden>
                <a onclick="closeProfile()" name="closeProfile" id="closeProfile" class="btn btn-warning  py-2 px-5" hidden>Close</a>
                <!-- <button onclick="closeProfile()" type="button" name="closeProfile" id="closeProfile" class="btn btn-services" hidden="">Close</button> -->
              </div>
           </div>

        </form>
      </div>

      <div class="col-lg-4" style="margin-top:20px">
          <div class="p-4 mb-3 bg-white">
            <a style="cursor:pointer;" href="saved_profiles.php" ><span class="rounded bg-success py-2 px-3 text-white"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; View Saved Profiles</span></a>
            </div>
             
            <div class="p-4 mb-3 bg-white">
            <a style="cursor:pointer;" onclick="myFunction()"><span class="rounded bg-primary py-2  text-white" style="padding:0px 1.4em;"><i class="fa fa-file-text-o" aria-hidden="true"></i>
            &nbsp; My Subscriptions</span></a>
            </div>

           <div id="myDIV" style="display:none;">
               
           <div class="p-4 mb-3 bg-white fade show active" id="change_password">
            <?php
            $balance="";
            $subscription_id="";
              $tdate= date("Y-m-d");
              $sql="select * from tbl_subscription where employer_id='".$_SESSION['employer_id']."' and status<>0 and  (expiry_date>'".$tdate."' or expiry_date='unlimited')";
              $res1=$con->query($sql);
              $res=$con->query($sql);
              if($row1=$res1->fetch_assoc())
              {
                ?>
                <!-- <button class="btn btn-warning" style="float:right;" data-toggle="modal" data-target="#myModal1">Refund Token</button> -->
               <?php
                while($row=$res->fetch_assoc())
                {
                $subscription_id=$row['subscription_id'];
                $token_name="";
                $expiry_date="";
                $view_cnt="";
                $sql_pack="select * from tbl_package where package_id='".$row['token_id']."'";
                $res_pack=$con->query($sql_pack);
                if($row_pack=$res_pack->fetch_assoc())
                {
                    $token_name=$row_pack['package_name'];
                    $expiry_date=$row['expiry_date'];
                    if($expiry_date!="unlimited")
                    {
                    $expiry_date = date("d-m-Y", strtotime($expiry_date));
                    }
                    else
                    {
                      $expiry_date ="No expiration";
                    }
                    $view_cnt=$row_pack['view_count'];
                }

                $sql_balance="select sum(used_tokens) as cnt from tbl_employer_view_list where subscription_id='".$subscription_id."'";
                $res_balance=$con->query($sql_balance);
                if($row_balance=$res_balance->fetch_assoc())
                {
                  $balance= $view_cnt-$row_balance['cnt'];
                }
              
            ?>
            
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Token : <?php echo $token_name; ?></label><br>
                  <label class="font-weight-bold" for="old_password">Expired On : <?php echo $expiry_date; ?></label>
                
                  <?php if($view_cnt=="unlimited") { ?>
                  <label class="font-weight-bold" for="old_password">Unlimited Tokens</label>
                  <?php } else { ?>
                    <label class="font-weight-bold" for="old_password"><b style="color:#26baee;"><?php echo $balance; ?></b> Tokens are remaining</label>
                 <br>
                    <?php } ?>
                  <?php
                  // if($row['status']=='1')
                  // {
                  ?>
                <!-- <button class="btn btn-danger" style="width: 80px; height: 38px;" onclick="activate_plan(<?php //echo $row['subscription_id']; ?>,<?php //echo $row['employer_id'] ?>)">Inactive</button> -->
                  <?php
                  // }
                  ?>
                  <?php
                  // if($row['status']=='2')
                  // {
                  ?>
                <!-- <button class="btn btn-success" style="width: 80px; height: 38px;" onclick="deactivate_plan(<?php //echo $row['subscription_id']; ?>)">Active</button> -->
                  <?php
                  // }
                  ?>
                </div>
               
            </div>
            <hr>
            <?php
              }

              $sqle="select extra_tokens from tbl_employer where id='".$_SESSION['employer_id']."'";
              $rese=$con->query($sqle);
              if($rowe=$rese->fetch_assoc())
              {
                if($rowe['extra_tokens']>0)
                {
                  ?>
                  <div class="row form-group-fwa">
                  <div class="col-md-12 mb-3 mb-md-0">
                   
                  <label class="font-weight-bold" for="old_password"><b >  Refunded Tokens/ Extra Tokens</label>
                      <label class="font-weight-bold" for="old_password"><b style="color:#26baee;"><?php echo $rowe['extra_tokens']; ?></b> Tokens are remaining</label>
                   
                  </div>
                 
              </div>
           <?php     
                }
              }
             
              }
              else
              {
              ?>
              <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">You have No Active Subscription plan </label>
                  
                </div>
            </div>
            <?php
              }
              ?>
           <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" style="color:#fff;background-color:#26baee!important;float:right;">Buy Tokens</button>
           
          
          
            </div>
            </div>

            <div class="p-4 mb-3 bg-white">
            <a style="cursor:pointer;"  data-toggle="modal" data-target="#tokenRefundModal"><span class="rounded bg-primary py-2 text-white" style="padding:0px 1.4em;">
              <i class="fa fa-credit-card" aria-hidden="true"></i>  &nbsp;  Request Refund</span></a>
            </div>
           
            <!-- <button class="btn btn-warning" style="float:right;" data-toggle="modal" data-target="#myModal1">Refund Token</button> -->
              
            <div class="p-4 mb-3 bg-white">
            <a style="cursor:pointer;" onclick="changePassword()"><span class="rounded bg-primary py-2  text-white" style="padding:0px 1.4em;">
              <i class="fa fa-key" aria-hidden="true"></i> Change Password</span></a>
            </div>


            <!--Change Password Modal content-->
          <div id="changePasswordDIV" style="display:none;">
            
            <div class="p-4 mb-3 bg-white fade show active" id="change_password">
            <?php
              if(isset($_SESSION['error_pass'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Error!</h4>
                            ".$_SESSION['error_pass']."
                            </div>
                        ";
                        unset($_SESSION['error_pass']);
                        }
                        if(isset($_SESSION['success_pass'])){
                        echo "
                            <div class='alert alert-success alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4>Success!</h4>
                            ".$_SESSION['success_pass']."
                            </div>
                        ";
                        unset($_SESSION['success_pass']);
                        }
    
            ?> 

            <form method="post">
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Old password <b style="color:red;"> *</b></label>
                  <input type="password" id="old_password" name="old_password" value="" class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">New password <b style="color:red;"> *</b></label>
                  <input type="password" id="new_password" name="new_password"  class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Confirm password <b style="color:red;"> *</b></label>
                  <input type="password" id="confirm_password" name="confirm_password"  class="form-control" onchange="check_password()" required >
                  <small id="error" style="color:red;"></small>
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                   </br>
                  <input type="submit" value="Update"  id="update_password" name="update_password" class="btn btn-primary  py-2 px-5">
                </div>
              </div>
            </form>

           </div>
          </div>

            
<!--             <div class="p-4 mb-3 bg-white">
            <a style="cursor:pointer;" data-backdrop="static" data-toggle="modal" data-target="#changePasswordModal"><span class="rounded bg-primary py-2 text-white" style="padding:0px 1.4em;">
              <i class="fa fa-key" aria-hidden="true"></i> Change Password</span></a>
            </div> -->

  

        </div>

      </div>
    </div>
        <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/circles-v3.jpg" height="500" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:80px;">
            <h2 style="font-family: 'Comfortaa', cursive;">
            Do you have something
            <br>you can offer our
            <br>jobseekers?</h2>
            <div>
              <input class="form-control" type="text" name="Email" placeholder="Email" style="width:300px;">
              <br>
              <a href="#" class="btn-get-started scrollto" style="width:300px;text-align:center;">SEND INFO</a>
            </div>
         </div>

      </div>
    </div>
  </div>
  </div>


  <?php include_once('../includes/external_footer.php'); ?>


  <div id="uploadimageModal" class="modal" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Upload & Crop Image</h4>
        </div>
        <div class="modal-body">
          <div class="row">
       <div class="col-md-8 text-center">
        <div id="image_demo" style="width:350px; margin-top:30px"></div>
       </div>
       <div class="col-md-4" style="padding-top:30px;">
        <br/>
        <br/>
        <br/>
     
     </div>
    </div>
        </div>
        <div class="modal-footer">
        <button class="btn btn-success crop_image">Crop & Upload Image</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </div>
    </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form method="post">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Subscribe Token</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
        <label class="font-weight-bold" for="fullname">Select Token</label>
                  <select  id="token" name="token" class="form-control" onchange="toggle(this.value)" >
                  <?php 
                  $tid="";
                  $i=0;
                    $sql="select * from tbl_package where status=1";
                    $res=$con->query($sql);
                    while($row=$res->fetch_assoc())
                    {
                      if($i==0)
                      {
                        $tid=$row['package_id'];
                      }

                  ?>
                      <option value="<?php echo $row['package_id']; ?>"><?php echo $row['package_name'] ;?></option>
                   <?php
                   $i++;
                    }
                    ?>   
                    </select>
        </div>

       <div id="cont" 
        style="
        border:solid 1px #CCC;padding:10px;font-size:15px;    padding-left: 30px;">
        <?php
$sql_pack="select * from tbl_package where package_id='".$tid."'";
$res_pack=$con->query($sql_pack);
if($row_pack=$res_pack->fetch_assoc())
{
 
  echo "<b>Price : </b>".$row_pack['price'];
  echo "<br>";
 
  echo "<b>Description : </b>".$row_pack['description'];
}

        ?>

       
    </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="subscribe">Subscribe</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>



      <!-- Request Token Refund Modal content-->
  <div class="modal fade" id="tokenRefundModal" role="dialog">
    <div class="modal-dialog">

      <form method="post">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Request Token Refund</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
        <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Company Name <b style="color:red;"> *</b></label>
                  <input type="text" id="company_name" name="company_name"  class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Contact Name <b style="color:red;"> *</b></label>
                  <input type="text" id="contact_name" name="contact_name"  class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Contact Number <b style="color:red;"> *</b></label>
                  <input type="text" id="contact_number" name="contact_number"  class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Email <b style="color:red;"> *</b></label>
                  <input type="email" id="contact_email" name="contact_email"  class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Name of Contact Used for Token <b style="color:red;"> *</b></label>
                  <input type="text" id="token_contact" name="token_contact"  class="form-control" required >
                </div>
            </div>
            <div class="row form-group-fwa">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="old_password">Reason for Refund <b style="color:red;"> *</b></label>
                  <input type="text" id="refund_reason" name="refund_reason"  class="form-control" required >
                </div>
            </div>

        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="refund">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>

      
    </div>


  </div>


</body>
</html>


<script>  
$(document).ready(function(){

  $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_emp_image.php",
        type: "POST",
        data:{"image": response},
  
        success:function(data)
        {
         
      
          $('#uploadimageModal').modal('hide');
          // $('#uploaded_image').html(data);
          $('#uploaded_image').attr("src",data);
          document.getElementById('file_location').value=data;
        }
      });
    })
  });

});  
</script>
<script>

function editProfile()
{
  $('#company_business_name').attr('readonly',false);
 
  $('#upload_image').attr('disabled',false);

  $('#details').attr('readonly',false);
  $('#contact_name').attr('readonly',false);
  $('#email').attr('readonly',false);
  $('#mobile').attr('readonly',false);
  $('#password').attr('readonly',false);
  $('#description').attr('readonly',false);
  $('#autocomplete').attr('readonly',false);
 
 
  $('#primary_business_industry').attr('disabled',false);
  $('#other').attr('disabled',false);
  $('#submitEditProfile').attr('hidden',false);
  $('#closeProfile').attr('hidden', false);
  $('#edit').attr('hidden',true);
}

function closeProfile()
{
  window.location.reload();
  // $('#company_business_name').attr('readonly',true);
 
  // $('#upload_image').attr('disabled',true);

  // $('#details').attr('readonly',true);
  // $('#contact_name').attr('readonly',true);
  // $('#email').attr('readonly',true);
  // $('#mobile').attr('readonly',true);
  // $('#password').attr('readonly',true);
  // $('#description').attr('readonly',true);
  // $('#autocomplete').attr('readonly',true);
 
 
  // $('#primary_business_industry').attr('disabled',true);
  // $('#other').attr('disabled',true);
  // $('#submitEditProfile').attr('hidden',true);
  // $('#closeProfile').attr('hidden', true);
  // $('#edit').attr('hidden',false);
}

 function check(email)
  {
    
        $.ajax({url: "../ajax/check_email.php",
          type: 'POST',
          data: {email:email},
          success:function(data)
           {
            if(data==1)
            {
              document.getElementById('val').innerHTML ="Email already in use!";
                  document.getElementById("email").setCustomValidity("Email already in use!");
            } 
  else{
    document.getElementById('val').innerHTML ="";
                  document.getElementById("email").setCustomValidity("");
  }
           }
        });
        
    
  }


//   function checkCurrentPassword(){
//     var currentPassword=document.getElementById('old_password').value;
//     var password="";
//    $query = ("SELECT * FROM tbl_employer WHERE id='".$_SESSION['employer_id']."'");
//    $result=$con->query($sql);
//    if($row= = mysqli_fetch_array($result))
//    {
//       $password = $row['password']
//   }

//   if(currentPassword=.md5($password))
//   {
//       document.getElementById('current_password').setCustomValidity("Current password is incorrect.");
//       document.getElementById('currentPasswordError').innerHTML ="Current password is incorrect. ";
//   }
//   else
//   {
//     document.getElementById('currentPasswordError').innerHTML="";
//     document.get('current_password').setCustomValidity("");
//   }

// }

  function check_password()
  {

    var new_password=document.getElementById('new_password').value;
    var confirm_password=document.getElementById('confirm_password').value;
    if(new_password!=confirm_password)
    {
      document.getElementById('confirm_password').setCustomValidity("New password and confirm password do not match.");
      document.getElementById('error').innerHTML ="New password and confirm password do not match. ";
    }
    else
    {
      document.getElementById('error').innerHTML ="";
      document.getElementById('confirm_password').setCustomValidity("");
    }

  }


</script>
<script>
 document.getElementById("logo").onchange = function (e) {

//var fname = "the file name here.ext";
var fname = e.target.files[0].name; 
var re = /(\.jpg|\.jpeg|\.bmp|\.gif|\.png)$/i;
if(!re.exec(fname))
{
alert("File extension not supported!");
document.getElementById('logo').setCustomValidity("File extension not supported!")
}
else
{
  document.getElementById('logo').setCustomValidity("")
}

  

  

    var reader = new FileReader();

    reader.onload = function (e) {
      
        // get loaded data and render thumbnail.
        document.getElementById("preivew").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};



function activate_plan(id,eid)
{

var val="AE";
  var r = confirm("Are you sure you want to activate this plan?");
    if (r == true) {

  $.ajax({
            type : 'post',
            url : '../ajax/update_row.php', //Here you will fetch records 
            data :  {id:id,eid:eid,val:val}, //Pass $id
            success : function(data){
    //  alert(data);
  if(data=="1")
  {
   
    window.location.replace('my_account.php');


  }
  else
  {
    alert("Error");
  }

            }
        });

}
}

function deactivate_plan(id)
{
var eid=0;
var val="AD";
  var r = confirm("Are you sure you want to deactivate this plan?");
    if (r == true) {

  $.ajax({
            type : 'post',
            url : '../ajax/update_row.php', //Here you will fetch records 
            data :  {id:id,eid:eid,val:val}, //Pass $id
            success : function(data){
        //   $('#price_view').html(data);//Show fetched data from database
  if(data=="1")
  {
   
    window.location.replace('account.php');
    //showSuccessToast_disable();

  }
  else
  {
    alert("Error");
  }
 // window.location.replace('our_works.php');
            }
        });

}
}
</script>

<style>
  .form-control {
    height: 40px; 
}
  </style>
 
 <script>
    function toggle(val) {
   
$.ajax({
          url: "../ajax/check_token.php",
          type: 'POST',
          data: {val:val},
          success:function(data)
           {
             $('#cont').html(data);
           }
        });
}
</script>

<script>
function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function changePassword() {
  var x = document.getElementById("changePasswordDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

</script>

<script>
 function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
      autocomplete.setComponentRestrictions(
            {'country': ['Au']});
    });
  }
}

</script>
 <script>
  function checkvalue(val)
     {
       
      if(val=="others")
      {
        // alert("haiii");
        $('#other').prop('required', true);
        document.getElementById('hiddenDiv').style.display='block';
       document.getElementById('other').style.display='block';
      }
    else
    {
      $('#other').prop('required', false);
      document.getElementById('hiddenDiv').style.display='none';
       document.getElementById('other').style.display='none'; 
    }
     }
              </script>
 <style>

#map {
  height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}
</style>