 <?php 
session_start();
include_once('../includes/config.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['submit']))
{
    $email=$_POST['email'];
    $password=$_POST['password'];
    $sql="select * from tbl_employer where email='".$email."' and password='".md5($password)."'";
    $result=$con->query($sql);
    if($row=$result->fetch_assoc())
    {
      $_SESSION['employer_id']=$row['id'];
      $_SESSION['employer_name']=$row['contact_name'];
      header('location:employer\account.php');
    }
    else{
        $_SESSION['error']= "invalid email or password";
    }
   
}

if(isset($_POST['register_submit']))
{
  $company_business_name=addslashes($_POST['company_business_name']);

  $image=$_POST['file_location'];
  $image1=basename($image);
 
  $contact_name=addslashes($_POST['contact_name']);
  $email=addslashes($_POST['email']);
  $mobile=addslashes($_POST['mobile']);
  $password=addslashes($_POST['password']);
  $description=addslashes($_POST['description']);
  $business_address=addslashes($_POST['business_address']);

  $primary_business_industry=addslashes($_POST['primary_business_industry']);
  $other_industry=addslashes($_POST['other']);
  $sql="insert into tbl_employer(company_business_name,image,contact_name,email,password,mobile,description_products,business_address,primary_business_industry,other_industry) values ('". $company_business_name."','".$image1."','".$contact_name."','".$email."','".md5($password)."','".$mobile."','".$description."','".$business_address."','". $primary_business_industry."','". $other_industry."')";
  if($con->query($sql))
  {
  $id=mysqli_insert_id($con);

    require '../vendor/autoload.php';
    require '../phpmailer/credential.php';

    $url = $_SERVER['REQUEST_URI']; //returns the current URL
    $parts = explode('/',$url);
    $dir = $_SERVER['SERVER_NAME'];
    for ($i = 0; $i < count($parts) - 1; $i++) {
    $dir .= $parts[$i] . "/";
    }
    $project_name="http://".$dir;

    $phpmail = new PHPMailer(true);

    $mail->SMTPDebug = 4;                               // Enable verbose debug output

    $phpmail->isSMTP();                                      // Set mailer to use SMTP
    $phpmail->Host = SMTP_SERVER;  // Specify main and backup SMTP servers
    $phpmail->SMTPAuth = true;                               // Enable SMTP authentication
    $phpmail->Username = EMAIL;                 // SMTP username
    $phpmail->Password = PASS;                           // SMTP password
    $phpmail->SMTPSecure = ENCRYPTION;                            // Enable TLS encryption, `ssl` also accepted
    $phpmail->Port = SMTP_PORT;                                    // TCP port to connect to

    $phpmail->setFrom(EMAIL, 'Find Work Australia');
    $phpmail->addAddress($email);     // Add a recipient

    $phpmail->addReplyTo(EMAIL);
    $phpmail->isHTML(true);                                  // Set email format to HTML

    $phpmail->Subject = 'Welcome to Find Work Australia Employer: '. $company_business_name;
    $phpmail->Body    = '<b>Hi '. $contact_name. ',</b>
        <br>
        <h4><b>Thank you for signing up! Welcome to Find Work Australia.</b></h4>
        <br>
        <p>Your account is now ready. Please click this <a href="'. $project_name .'account.php">link</a> to login and manage your account.</p>
        <br><br>
        <i><p>Thanks for your time,</p>
        <p>Find Work Australia Team</p></i>

        ';
  if(!$phpmail->send()) {
        $_SESSION['error']= 'Message could not be sent. Mailer Error: ' . $phpmail->ErrorInfo;
    } else {
        $_SESSION['success']='Message has been sent';
    }


  header('location:register_success.php');
  $_SESSION['success']= "Employer Details Saved Successfully";

  if($other_industry!="")
  {
    $sql_temp_industry="insert into tbl_temp_industry(employer_id,other_industry,status)values('$id','".$other_industry."','0')";
    $res=$con->query($sql_temp_industry);
  
  }
  
  if($image)
        {
		           
          if (!file_exists('../admin/logo/'.$id.'/image'))
          mkdir('../admin/logo/'.$id.'/image', 0777, true);
      // if(move_uploaded_file($_FILES['upload_image']['tmp_name'], 'admin/uploads/'.$id.'/image' . $_FILES['upload_image']['name']))
      // Your file
      $file = $image;

// Open the file to get existing content
$data = file_get_contents($file);
$image1=basename($image);
// New file
$new = '../admin/logo/'.$id.'/image/'.$image1;

// Write the contents back to a new file
if(file_put_contents($new, $data))
  {
   // $imageName = $targetPath;
  //remove
  unlink($file);
  }
  else
  { 
    $_SESSION['error']= "Error ";  
    }
  }
 


$_SESSION['employer_id']=$id;
$_SESSION['employer_name']=$contact_name;

if($other_industry!="")
{
  $admin_mail_id="";
  $sql_email="select * from tbl_settings where attribute='contact_mail'";
  $res_email=$con->query($sql_email);
  if($row_email=$res_email->fetch_assoc())
  {
    $admin_mail_id=$row_email['value'];
  }
$name="";
$to=$admin_mail_id;
$subject="Industry Request";
$url = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
$dir .= $parts[$i] . "/";
}

$project_name="http://".$dir;
$message="<h4><u>Industry Suggestion</u></h4><br>Hi, <br> New Industry Suggestion from  ".$company_business_name." ";
 $body=  "<div><br><p>$message<br>Log into your account to approve <br> <br><a href='" . $project_name . "admin/index.php'><button class='btn btn-primary'>Continue</button></a><br><br></p>Regards,<br> Admin.</div>";

 $subject = "New Industry Suggestion";
  $txt = "<br>";
  $txt .= "<br>";
  $txt .= "<b> </b><br>".$body;
  $headers= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

  $headers.= "From: admin@jobseeker.com>\r\n";
  $sent_email = mail($to,$subject,$txt,$headers);

}
$name="";
$to=$email;
$subject="Welcome To Find Work Australia";
$url = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
$dir .= $parts[$i] . "/";
}

$project_name="http://".$dir;
$message="<b>Welcome to Find Work Australia</b><br><br><p>Thank you for joining our site and we hope you find what you are looking for.
If you require any additional help, please email help@findworkaustralia.com.au</p>";
 $body=  "<div><br><br><p>$message<br>
  <a href='" . $project_name . "employer_login.php'>" .  $project_name . "/employer_login.php<button class='btn btn-primary'>Continue</button></a>
<br><br></p>Regards,<br> Admin.</div>";

 $subject = "TWelcome To Find Work Australia";
  $txt = "<br>";
  $txt .= "<br>";
  $txt .= "<b> </b><br>".$body;
  $headers= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

  $headers.= "From: admin@jobseeker.com>\r\n";
  $sent_email = mail($to,$subject,$txt,$headers);

 


//header('location:success_message_employer.php');
}
else{
  $_SESSION['error']= "Error!!! Please try again ";
}
}
include_once('../includes/external_header.php'); 
?>

<style type="text/css">
   .progressbar {
      counter-reset: step;
      font-size: 20px;
      font-family: 'Comfortaa', cursive;
    }

    .progressbar li {
      list-style-type: none;
      float: left;
      width: 25%;
      position: relative;
      text-align: center
    }

    .progressbar li:before {
      content: counter(step);
      counter-increment: step;
      color: white;
      width: 50px;
      height: 50px;
      line-height: 50px;
      border: 3px solid #ddd;
      display: block;
      text-align: center;
      margin: 0 auto 10px auto;
      border-radius: 50%;
      background-color: #003E6B;
    }

    .progressbar li:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 1px;
      background-color: #ddd;
      top: 25px;
      left: -50%;
      z-index: -1;
    }

    .progressbar li:first-child:after {
      content:none;
    }

    .progressbar li.active {
      color: green;
    }



  </style>

  <style>
.emp_profile h1,h2,h3,h4,h5,h6{
    
    font-family: 'Comfortaa', cursive;
    font-weight: 600;
}
p {
  font-family: 'Comfortaa', cursive;
    font-weight: 600;
}

.section-header h3 {
  font-family: 'Comfortaa', cursive;
     font-size: 42px!important;
    text-align: center;
    font-weight: 500;
    position: relative;
}

.numberCircle {
    width: 100px;
    line-height: 100px;
    border-radius: 50%;
    text-align: center;
    font-size: 32px;
    border: 3px solid #666;
    background:#666;


}

</style>

<!DOCTYPE html>
<html lang="en">

 



<body>
<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Upload & Crop Image</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 text-center">
              <div id="image_demo" style="width:350px; margin-top:30px"></div>
            </div>
            <div class="col-md-4" style="padding-top:30px;">
                <br/>
                <br/>                    
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success crop_image">Crop & Upload Image</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </div>
  </div>
</div>
  <!--==========================
  Header
  ============================-->

  <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/employer-background.jpg" height="400" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <p>FIND WORK IN AUSTRALIA</p>
            <h2 style="font-family: 'Comfortaa', cursive;">Search for staff your way</h2>
            <div>
              <a href="login.php" class="btn-get-started scrollto">LOGIN</a>
            </div>
         </div>

      </div>
    </div>

      <main id="main"> 
    <section id="about">
      <div class="container">
        <header class="section-header">
            <h3 style="color:red!important;">Overview</h3>
            <p>You may complete as many searches as you like. This will give you an overview of the Jobseekers Profile. Use tokens to unlock their contact information including phone number and email.
            <br><br>Jobseekers are required to keep their profile updated. Failure to update their profile may result in a poor rating and/or their account suspended.
            <br><br> If you use a token to unlock a profile and the Jobseeker is already employed, you may easily request a token refund in your account.</p>
            <div class="col-md-12" style="text-align: center;">
             <a href="#employer-step" class="btn-services scrollto abt_btn">TELL ME MORE ABOUT THIS</a>
           </div>
        </header>
        </div>
      </section>
  </main>

  </div>


  <!--==========================
  REGISTRATION
  ============================-->

<div class="col-md-12 login_back">
  <div class="container">

    <div class="row">
      <div class="col-md-2"></div>

        <div class="col-md-8 login regist_head">
                    <?php
                      if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4> Error!</h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                         
                            ".$_SESSION['success']."
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
                    ?>

          <div class="row">
            <div class="col-md-1"></div>
              <div class="col-md-10">
               
                <div class="row">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <h6>Profile</h6>
                          <label class="reg_label1">Business name <span class="star">*</span></label>
                          <input type="text" id="company_business_name" name="company_business_name" class="reg_box" required>

                          <label  class="reg_label">Logo</label>
                            <div class="custom-file-upload" >                     
                              <input type="file" name="upload_image" id="upload_image"  />
                              <div class="col-md-12" style=" padding: 0px;">
                              <small>Maximum logo size: 800kb</small>
                            </div>

                            <div class="col-md-6" style=" padding: 0px;">
                              <img src="" id="uploaded_image" class="img-thumbnail img_position" />
                            </div>
                              <input type="hidden" name="file_location" id="file_location">
                            </div>

                            <label class="reg_label">Contact name <span class="star">*</span></label>
                            <input type="text" id="contact_name" name="contact_name" class="reg_box">
                     
                            <label class="reg_label">Email <span class="star">*</span></label>
                            <input type="email" id="email" name="email" class="reg_box" onchange="check(this.value)" required>
                            <small id="val" style="color:red;"></small>

                            <label class="reg_label">Password <span class="star">*</span></label>
                            <input type="password" id="password" name="password" class="reg_box" autocomplete="off" required>

                            <label class="reg_label">Phone Number <span class="star">*</span></label>
                            <input type="text" id="mobile" name="mobile" class="reg_box" required>
                            <small id="val" style="color:red;"></small>

                            <label class="reg_label" style="font-size: 13.9px;">A brief description of the products and services as part of the profile <span class="star">*</span></label>
                            <textarea id="description" name="description" class="reg_box"></textarea>

                            <label class="reg_label">Address <span class="star">*</span></label>
                            <input id="autocomplete" placeholder="Enter your address"  name="business_address" class="reg_box" onFocus="geolocate()" type="text" required/>      
             
            
                            <label class="reg_label">Primary business industry</label>
                            <select id="primary_business_industry" name="primary_business_industry" class="reg_box" onchange='checkvalue(this.value)'>
                            <option value="" selected="selected" disabled>Select</option>
                                             <?php 
                                                        $result = mysqli_query($con,"SELECT  DISTINCT industry_name,id from tbl_industry order by industry_name");
                                                        while($row = mysqli_fetch_array($result))
                                                          {  ?>
                                                     <option value="<?php echo $row['id']; ?>"><?php echo $row['industry_name'];?></option>
                                                    
                                                    <?php }  ?> 
                                                    <option value="others">other</option>
                                             </select></br>
                                                          
                            <input type="text" name="other" id="other"   class="reg_box"   style='display:none; margin-top: 12px;'/>
                            <input type="submit" value="Submit" name="register_submit" class="login_btn">


                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>


        </div>


      </div>



</div>


<div class="col-md-12 login_back">
<div class="container">
    <main id="main"> 
    <section id="about">
      <div class="container" id="jobseeker-step">
        <header class="section-header">
            <h3>How does it work?</h3>
            <h4 style="text-align:justify;font-family: 'Comfortaa', cursive;">Find Work Australia lets you create your free profile for employers who are looking for workers.<br>
            Your information will be kept safe and when an employer runs a search, a preview of your profile will be shown to them. It is important to keep your information up to date as the employer will be paying to unlock your details.
            <br><br>
            You can set your profile to “Ready to start work”, “Employed but looking for a change” and “Employed”.
            When you change your status to ‘Employed’ your profile will be hidden until you are ready to be found again.</h4>
            <ul class="progressbar">
              <li><p>Create your profile
              </p>
              <h5>A small preview of this will appear when an employer does a search.</h5>
              </li>

              <li><p>Get a phone number
              <h5>Have your Australian mobile number ready to receive calls from your employer – we can help you with this!</h5></p>
              
              </li>
              <li><p>Keep your profile updated</p>
              <h5>Updating profile will makes you more competent.</h5>
              </li>
              <li><p>Get hired</p>
              <h5>Receive a phone call and/or email from interested employers!</h5>
              </li>
            </ul>



        </header>


        </div>
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
             <a href="#register-top" class="btn-services scrollto abt_btn">CREATE YOUR PROFILE</a>
           </div>
      </section>

  </main>
</div>
</div>





  

<?php include_once('../includes/external_footer.php'); ?>

  <script src="../lib/jquery/jquery.min.js"></script>
  <script src="../lib/jquery/jquery-migrate.min.js"></script>
  <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../lib/easing/easing.min.js"></script>
  <script src="../lib/mobile-nav/mobile-nav.js"></script>
  <script src="../lib/wow/wow.min.js"></script>
  <script src="../lib/waypoints/waypoints.min.js"></script>
  <script src="../lib/counterup/counterup.min.js"></script>
  <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../lib/isotope/isotope.pkgd.min.js"></script>
  <script src="../lib/lightbox/js/lightbox.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="../contactform/contactform.js"></script>
  <!-- <script src="js/jquery.min.js"></script> -->
  <script src="../js/croppie.js"></script>
  
  <!-- Template Main Javascript File -->
  <script src="../js/main.js"></script>

<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_emp_image.php",
        type: "POST",
        data:{"image": response},
  
        success:function(data)
        {
         
       
          $('#uploadimageModal').modal('hide');
          // $('#uploaded_image').html(data);
          $('#uploaded_image').attr("src",data);
          document.getElementById('file_location').value=data;
        }
      });
    })
  });

});  
</script>
   <script>
    function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
      autocomplete.setComponentRestrictions(
            {'country': ['Au']});

    });
  }
}

 </script>
   <script type="text/javascript">
    $(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
    });
</script>   
  <script>
  function checkvalue(val)
     {
      if(val==="others")
      {
      $('#other').prop('required', true);
       document.getElementById('other').style.display='block';
      }
      
    else
    {
      $('#other').prop('required', false);
       document.getElementById('other').style.display='none'; 
    }

     }
</script>
<style>

#map {
  height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}


</style>
<style type="text/css">
  .file-upload-button{
    margin-left: -7.01% !important;
  }
  .regist_head h6{
    font-weight: 500;
    font-size: 18px;
    color: #062fa2;
  }
  /* .img-thumbnail {
    
  
    display: list-item;
} */
/* .img_position{
  display: block;
} */
</style>

 <!-- <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info" style="    margin-top: -75px;">
            <img src="img/LOGO_FOOTER.png">            
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Jobseeker</h4>
            <ul>
              <li><a href="#">Create a Jobseeker Profile</a></li>
              <li><a href="#">Jobseeker Login</a></li>
              <li><a href="#">Benefits</a></li>
              <li><a href="#">Overview</a></li>
             
            </ul>
          </div>
           <div class="col-lg-3 col-md-6 footer-links">
            <h4>Employers</h4>
            <ul>
              <li><a href="#">Create an Employer Account</a></li>
              <li><a href="#">Employer Login</a></li>
              <li><a href="#">Employer Overview</a></li> 
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Agency</h4>
            <ul>
              <li><a href="#">Agency</a></li>          
            </ul>
          </div>
          <div class="col-md-12 footer_job">
            <h1>Jobs</h1>
          </div>

         </div>
      </div>
    </div>
<div class="col-md-12" style="background-color: #002d5e;">
    <div class="container">
      <div class="row">
      <div class="col-md-6 copyright" style="text-align: left;">
       Find Work Australia
      </div>
      <div class="col-md-6 credits" style="text-align: right;">
           <i class="fa fa-facebook" aria-hidden="true"></i>
           <i class="fa fa-twitter" aria-hidden="true"></i>
           <i class="fa fa-google-plus" aria-hidden="true"></i>
           <i class="fa fa-instagram" aria-hidden="true"></i>
           <i class="fa fa-wifi" aria-hidden="true"></i>
      </div>
    </div>
    </div>
  </div>
  </footer>  -->


<script type="text/javascript">

;(function($) {

      // Browser supports HTML5 multiple file?
      var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
          isIE = /msie/i.test( navigator.userAgent );

      $.fn.customFile = function() {

        return this.each(function() {

          var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
              $wrap = $('<div class="file-upload-wrapper">'),
              $input = $('<input type="text" class="file-upload-input" />'),
              // Button that will be used in non-IE browsers
              $button = $('<button type="button" class="file-upload-button">Select a File</button>'),
              // Hack for IE
              $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

      
          $file.css({
            position: 'absolute',
            left: '-9999px'
          });

          $wrap.insertAfter( $file )
            .append( $file, $input, ( isIE ? $label : $button ) );

          // Prevent focus
          $file.attr('tabIndex', -1);
          $button.attr('tabIndex', -1);

          $button.click(function () {
            $file.focus().click(); // Open dialog
          });

          $file.change(function() {

            var files = [], fileArr, filename;

            // If multiple is supported then extract
            // all filenames from the file array
            if ( multipleSupport ) {
              fileArr = $file[0].files;
              for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                files.push( fileArr[i].name );
              }
              filename = files.join(', ');

            // If not supported then just take the value
            // and remove the path to just show the filename
            } else {
              filename = $file.val().split('\\').pop();
            }

            $input.val( filename ) // Set the value
              .attr('title', filename) // Show filename in title tootlip
              .focus(); // Regain focus

          });

          $input.on({
            blur: function() { $file.trigger('blur'); },
            keydown: function( e ) {
              if ( e.which === 13 ) { // Enter
                if ( !isIE ) { $file.trigger('click'); }
              } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                // On some browsers the value is read-only
                // with this trick we remove the old input and add
                // a clean clone with all the original events attached
                $file.replaceWith( $file = $file.clone( true ) );
                $file.trigger('change');
                $input.val('');
              } else if ( e.which === 9 ){ // TAB
                return;
              } else { // All other keys
                return false;
              }
            }
          });

        });

      };

      // Old browser fallback
      if ( !multipleSupport ) {
        $( document ).on('change', 'input.customfile', function() {

          var $this = $(this),
              // Create a unique ID so we
              // can attach the label to the input
              uniqId = 'customfile_'+ (new Date()).getTime(),
              $wrap = $this.parent(),

              // Filter empty input
              $inputs = $wrap.siblings().find('.file-upload-input')
                .filter(function(){ return !this.value }),

              $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

          // 1ms timeout so it runs after all other events
          // that modify the value have triggered
          setTimeout(function() {
            // Add a new input
            if ( $this.val() ) {
              // Check for empty fields to prevent
              // creating new inputs when changing files
              if ( !$inputs.length ) {
                $wrap.after( $file );
                $file.customFile();
              }
            // Remove and reorganize inputs
            } else {
              $inputs.parent().remove();
              // Move the input so it's always last on the list
              $wrap.appendTo( $wrap.parent() );
              $wrap.find('input').focus();
            }
          }, 1);

        });
      }

}(jQuery));

$('input[type=file]').customFile();
</script>

</body>
</html>
