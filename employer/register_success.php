<?php 
session_start();
include_once('../includes/config.php');
include_once('../includes/external_header.php'); 

?>

<div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/login.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <p>FIND WORK IN AUSTRALIA</p>
            <h2>Employer Registration</h2>                
         </div>
      </div>
    </div>
  </div>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
       
  <div class="jumbotron text-xs-center"  style="margin-top:20px;">
  <h4 class="display-4">Thank You!</h4>
  <p class="lead">Your employer account is now ready. <strong></strong> Please click below to continue with your account.</p>
  <hr>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="account.php" role="button">Continue</a>
  </p>
  </div>

         
      </div>
    </div>
  </div>
   
<?php include_once('../includes/external_footer.php'); ?>
