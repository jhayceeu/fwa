<?php 
session_start();
include_once('../includes/config.php');
date_default_timezone_set("Australia/Sydney");
$pid="";
$package_name=$package_amount=$package_desc="";
if(isset($_GET['pid']))
{
$id=$_GET['pid'];
$eid=$_GET['eid'];

$sql_package="select * from tbl_package where package_id='".$id."'";
$res_package=$con->query($sql_package);
if($row_package=$res_package->fetch_assoc())
{
    $package_name=$row_package['package_name'];
    $package_amount=$row_package['price'];
    $package_desc=$row_package['description'];

}
}
else
{
  header('location:package.php');
}



?>

<?php
use \PhpPot\Service\StripePayment;

require_once "../includes/stripe_config.php";

if (!empty($_POST["token"])) {
    require_once '../includes/stripePayment.php';
    $stripePayment = new StripePayment();
    
    $stripeResponse = $stripePayment->chargeAmountFromCard($_POST);
    $package_id=$_POST['package_id'];
    $employer_id=$_POST['employer_id'];
    // require_once "config.php";
    //$dbController = new DBController();
    
    $amount = $stripeResponse["amount"] /100;
    
    $param_type = 'ssdssss';
    $param_value_array = array(
        $_POST['email'],
        $_POST['item_number'],
        $amount,
        $stripeResponse["currency"],
        $stripeResponse["balance_transaction"],
        $stripeResponse["status"],
        json_encode($stripeResponse)
    );
    $query = "insert into tbl_payment (email, item_number, amount, currency_code, txn_id, payment_status, payment_response,user_id,subscription_id) values ('".$_POST['email']."', '".$_POST['item_number']."', '".$amount."', '".$stripeResponse["currency"]."', '".$stripeResponse["balance_transaction"]."', '".$stripeResponse["status"]."', 'd','".$employer_id."','".$package_id."')";
    $con->query($query);
    //$id = $dbController->insert($query, $param_type, $param_value_array);
if($package_id!="")
{
  $sql_tk="select * from tbl_package where package_id='".$package_id."'";
  $res_tk=$con->query($sql_tk);
  if($row_tk=$res_tk->fetch_assoc())
  {
      if($row_tk['expiry_days']!="unlimited")
      {
    $exp=$row_tk['expiry_days']*30;
      }
      else
      {
        $exp="unlimited";
      }
    $tdate= date("Y-m-d");
    if($exp!="unlimited")
    {
    $edate=date('Y-m-d', strtotime($tdate. ' + '.$exp.' days'));
    }
    else
    {
        $edate="unlimited";
    }
    $sql_sub="insert into tbl_subscription(employer_id,subscription_date,expiry_date,token_id) values('".$employer_id."','".$tdate."','".$edate."','".$package_id."')";
    $con->query($sql_sub);
  }

    
    if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded') {
     if(isset($_SESSION['employer_id']))
     {
        $_SESSION['success'] = "Stripe payment is completed successfully. The TXN ID is " . $stripeResponse["balance_transaction"]; 
        header('location:my_account.php'); 
    }
     else
     {
        $_SESSION['success'] = "Stripe payment is completed successfully. The TXN ID is " . $stripeResponse["balance_transaction"]."<br> Please activate the plan from your account before Use";
        header('location:success_message_employer.php');
     }
    }
}
}
include_once('../includes/external_header.php'); 
?>


<!-- <!DOCTYPE html>
<html lang="en"> -->
<link href="../css/style_stripe.css" rel="stylesheet" type="text/css" />


<div class="unit-5 overlay" style="margin-top:100px; margin-bottom: 50px; height:100%; background-image: url('../img/hero_bg_2.jpg');">
      <div class="container text-center">
        <h2 style="margin-top:20px;" class="mb-0">Token Subscription </h2>
        <p class="mb-0 unit-6"><a href="home.php">Home</a> <span class="sep">></span> <span>Token Subscription</span></p>
      </div>
    </div>


<!-- <body>
  <div class="col-md-12" class="login_banner">
    <div class="row">      
      <img src="../img/hero_bg_2.jpg" height="300" width="100%">
       <div class="container">
         <div class="intro-info" style="position: absolute;top:180px;">
            <p>MANAGE YOUR PROFILE</p>
            <h2>Employer Home</h2>           
        </div>
      </div>
    </div>
  </div> -->


    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
        <div class="col-md-12 col-lg-2">
        </div>
          <div class="col-md-12 col-lg-6 mb-5">
   
          <?php if(!empty($successMessage)) { ?>
    <div id="success-message"><?php echo $successMessage; ?></div>
    <?php  } ?>
    <div id="error-message"></div>
<div style="text-align:center;">
    <h5><b style="color:#26baee;">Subscription Plan : </b><?php echo $package_name;  ?></h5>
    <h5><b style="color:#26baee;">Amount :</b> $<?php echo $package_amount; ?></h5>
    <br>
   </div>             
            <form id="frmStripePayment" action=""
                method="post">
                <div class="field-row">
                    <label>Card Holder Name</label> <span
                        id="card-holder-name-info" class="info"></span><br>
                    <input type="text" id="name" name="name"
                        class="demoInputBox">
                </div>
                <div class="field-row">
                    <label>Email</label> <span id="email-info"
                        class="info"></span><br> <input type="text"
                        id="email" name="email" class="demoInputBox">
                </div>
                <div class="field-row">
                    <label>Card Number</label> <span
                        id="card-number-info" class="info"></span><br> <input
                        type="text" id="card-number" name="card-number"
                        class="demoInputBox">
                </div>
                <div class="field-row">
                    <div class="contact-row column-right">
                        <label>Expiry Month / Year</label> <span
                            id="userEmail-info" class="info"></span><br>
                        <select name="month" id="month"
                            class="demoSelectBox">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select> <select name="year" id="year"
                            class="demoSelectBox">
                            <option value="20">2020</option>
                            <option value="21">2021</option>
                            <option value="22">2022</option>
                            <option value="23">2023</option>
                            <option value="24">2024</option>
                            <option value="25">2025</option>
                            <option value="26">2026</option>
                            <option value="27">2027</option>
                            <option value="28">2028</option>
                            <option value="29">2029</option>
                            <option value="30">2030</option>
                            <option value="30">2031</option>
                            <option value="30">2032</option>
                            <option value="30">2033</option>
                            <option value="30">2034</option>
                            <option value="30">2035</option>
                            <option value="30">2036</option>
                            <option value="30">2037</option>
                            <option value="30">2038</option>
                            <option value="30">2039</option>
                            <option value="30">2040</option>
                        </select>
                    </div>
                    <div class="contact-row cvv-box">
                        <label>CVC</label> <span id="cvv-info"
                            class="info"></span><br> <input type="text"
                            name="cvc" id="cvc"
                            class="demoInputBox cvv-input">
                    </div>
                </div>
                <div>
                    <input type="submit" name="pay_now" value="Submit"
                        id="submit-btn" class="btnAction"
                        onClick="stripePay(event);">

                    <div id="loader">
                        <img alt="loader" src="../img/LoaderIcon.gif">
                    </div>
                </div>
                <input type='hidden' name='amount' value='<?php echo $package_amount ?>'> <input
                    type='hidden' name='currency_code' value='USD'> <input
                    type='hidden' name='item_name' value='<?php echo $package_name ?>'>
                    <input type="hidden" name="package_id" value="<?php echo $id; ?>">
                    <input type="hidden" name="employer_id" value="<?php echo $eid; ?>">
                <input type='hidden' name='item_number'
                    value='PHPPOTEG#1'>
            </form>  
          
          </div>

         
        </div>
      </div>
    </div>

<!-- </body>
</html> -->

<?php include_once('../includes/external_footer.php'); ?>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="../vendor/jquery-3.2.1.min.js"
        type="text/javascript"></script>
    <script>
function cardValidation () {
    var valid = true;
    var name = $('#name').val();
    var email = $('#email').val();
    var cardNumber = $('#card-number').val();
    var month = $('#month').val();
    var year = $('#year').val();
    var cvc = $('#cvc').val();

    $("#error-message").html("").hide();

    if (name.trim() == "") {
        valid = false;
    }
    if (email.trim() == "") {
    	   valid = false;
    }
    if (cardNumber.trim() == "") {
    	   valid = false;
    }

    if (month.trim() == "") {
    	    valid = false;
    }
    if (year.trim() == "") {
        valid = false;
    }
    if (cvc.trim() == "") {
        valid = false;
    }

    if(valid == false) {
        $("#error-message").html("All Fields are required").show();
    }

    return valid;
}
//set your publishable key
Stripe.setPublishableKey("<?php echo STRIPE_PUBLISHABLE_KEY; ?>");

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        //enable the submit button
        $("#submit-btn").show();
        $( "#loader" ).css("display", "none");
        //display the errors on the form
        $("#error-message").html(response.error.message).show();
    } else {
        //get token id
        var token = response['id'];
        //insert the token into the form
        $("#frmStripePayment").append("<input type='hidden' name='token' value='" + token + "' />");
        //submit form to the server
        $("#frmStripePayment").submit();
    }
}
function stripePay(e) {
    e.preventDefault();
    var valid = cardValidation();

    if(valid == true) {
        $("#submit-btn").hide();
        $( "#loader" ).css("display", "inline-block");
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#cvc').val(),
            exp_month: $('#month').val(),
            exp_year: $('#year').val()
        }, stripeResponseHandler);

        //submit from callback
        return false;
    }
}
</script>