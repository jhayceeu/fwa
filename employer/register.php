<?php 
session_start();
include_once('../includes/config.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['submit']))
{
    $email=$_POST['email'];
    $password=$_POST['password'];
    $sql="select * from tbl_employer where email='".$email."' and password='".md5($password)."'";
    $result=$con->query($sql);
    if($row=$result->fetch_assoc())
    {
      $_SESSION['employer_id']=$row['id'];
      $_SESSION['employer_name']=$row['contact_name'];
      header('location:employer\account.php');
    }
    else{
        $_SESSION['error']= "invalid email or password";
    }
   
}

if(isset($_POST['register_submit']))
{
  $secretKey = '6LeiFLEZAAAAAPhmVPm907CtOOBxRFwbIh1Cgy9R';
  $token = $_POST['g-token'];
  $ip = $_SERVER['REMOTE_ADDR'];

  $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$token."&remoteip=".$ip;
  $request = file_get_contents($url);
  $response = json_decode($request);

  $company_business_name=addslashes($_POST['company_business_name']);

  $image=$_POST['file_location'];
  $image1=basename($image);
 
  $contact_name=addslashes($_POST['contact_name']);
  $email=addslashes($_POST['email']);
  $mobile=addslashes($_POST['mobile']);
  $password=addslashes($_POST['password']);
  $description=addslashes($_POST['description']);
  $business_address=addslashes($_POST['business_address']);

  $primary_business_industry=addslashes($_POST['primary_business_industry']);
  $other_industry=addslashes($_POST['other']);
  $sql="insert into tbl_employer(company_business_name,image,contact_name,email,password,mobile,description_products,business_address,primary_business_industry,other_industry) values ('". $company_business_name."','".$image1."','".$contact_name."','".$email."','".md5($password)."','".$mobile."','".$description."','".$business_address."','". $primary_business_industry."','". $other_industry."')";
  if($con->query($sql))
  {
  $id=mysqli_insert_id($con);

    require '../vendor/autoload.php';
    require '../phpmailer/credential.php';

    $url = $_SERVER['REQUEST_URI']; //returns the current URL
    $parts = explode('/',$url);
    $dir = $_SERVER['SERVER_NAME'];
    for ($i = 0; $i < count($parts) - 1; $i++) {
    $dir .= $parts[$i] . "/";
    }
    $project_name="http://".$dir;

    $phpmail = new PHPMailer(true);

    $mail->SMTPDebug = 4;                               // Enable verbose debug output

    $phpmail->isSMTP();                                      // Set mailer to use SMTP
    $phpmail->Host = SMTP_SERVER;  // Specify main and backup SMTP servers
    $phpmail->SMTPAuth = true;                               // Enable SMTP authentication
    $phpmail->Username = EMAIL;                 // SMTP username
    $phpmail->Password = PASS;                           // SMTP password
    $phpmail->SMTPSecure = ENCRYPTION;                            // Enable TLS encryption, `ssl` also accepted
    $phpmail->Port = SMTP_PORT;                                    // TCP port to connect to

    $phpmail->setFrom(EMAIL, 'Find Work Australia');
    $phpmail->addAddress($email);     // Add a recipient

    $phpmail->addReplyTo(EMAIL);
    $phpmail->isHTML(true);                                  // Set email format to HTML

    $phpmail->Subject = 'Welcome to Find Work Australia Employer: '. $company_business_name;
    $phpmail->Body    = '<b>Hi '. $contact_name. ',</b>
        <br>
        <h4><b>Thank you for signing up! Welcome to Find Work Australia.</b></h4>
        <br>
        <p>Your account is now ready. Please click this <a href="'. $project_name .'account.php">link</a> to login and manage your account.</p>
        <br><br>
        <i><p>Thanks for your time,</p>
        <p>Find Work Australia Team</p></i>

        ';

  if($response->success){

    if(!$phpmail->send()) {
    $_SESSION['error']= 'Message could not be sent. Mailer Error: ' . $phpmail->ErrorInfo;
    } 
    else {
        $_SESSION['success']='Message has been sent. Someone will contact you shortly.';
    }

  }

  else{
    $_SESSION['error']='Captcha Validation Failed.';
  }


  header('location:register_success.php');
  $_SESSION['success']= "Employer Details Saved Successfully";

  if($other_industry!="")
  {
    $sql_temp_industry="insert into tbl_temp_industry(employer_id,other_industry,status)values('$id','".$other_industry."','0')";
    $res=$con->query($sql_temp_industry);
  
  }
  
  if($image)
        {
               
          if (!file_exists('../admin/logo/'.$id.'/image'))
          mkdir('../admin/logo/'.$id.'/image', 0777, true);
      // if(move_uploaded_file($_FILES['upload_image']['tmp_name'], 'admin/uploads/'.$id.'/image' . $_FILES['upload_image']['name']))
      // Your file
      $file = $image;

// Open the file to get existing content
$data = file_get_contents($file);
$image1=basename($image);
// New file
$new = '../admin/logo/'.$id.'/image/'.$image1;

// Write the contents back to a new file
if(file_put_contents($new, $data))
  {
   // $imageName = $targetPath;
  //remove
  unlink($file);
  }
  else
  { 
    $_SESSION['error']= "Error ";  
    }
  }
 


$_SESSION['employer_id']=$id;
$_SESSION['employer_name']=$contact_name;

if($other_industry!="")
{
  $admin_mail_id="";
  $sql_email="select * from tbl_settings where attribute='contact_mail'";
  $res_email=$con->query($sql_email);
  if($row_email=$res_email->fetch_assoc())
  {
    $admin_mail_id=$row_email['value'];
  }
$name="";
$to=$admin_mail_id;
$subject="Industry Request";
$url = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
$dir .= $parts[$i] . "/";
}

$project_name="http://".$dir;
$message="<h4><u>Industry Suggestion</u></h4><br>Hi, <br> New Industry Suggestion from  ".$company_business_name." ";
 $body=  "<div><br><p>$message<br>Log into your account to approve <br> <br><a href='" . $project_name . "admin/index.php'><button class='btn btn-primary'>Continue</button></a><br><br></p>Regards,<br> Admin.</div>";

 $subject = "New Industry Suggestion";
  $txt = "<br>";
  $txt .= "<br>";
  $txt .= "<b> </b><br>".$body;
  $headers= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

  $headers.= "From: admin@jobseeker.com>\r\n";
  $sent_email = mail($to,$subject,$txt,$headers);

}
$name="";
$to=$email;
$subject="Welcome To Find Work Australia";
$url = $_SERVER['REQUEST_URI']; //returns the current URL
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
$dir .= $parts[$i] . "/";
}

$project_name="http://".$dir;
$message="<b>Welcome to Find Work Australia</b><br><br><p>Thank you for joining our site and we hope you find what you are looking for.
If you require any additional help, please email help@findworkaustralia.com.au</p>";
 $body=  "<div><br><br><p>$message<br>
  <a href='" . $project_name . "employer_login.php'>" .  $project_name . "/employer_login.php<button class='btn btn-primary'>Continue</button></a>
<br><br></p>Regards,<br> Admin.</div>";

 $subject = "TWelcome To Find Work Australia";
  $txt = "<br>";
  $txt .= "<br>";
  $txt .= "<b> </b><br>".$body;
  $headers= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

  $headers.= "From: admin@jobseeker.com>\r\n";
  $sent_email = mail($to,$subject,$txt,$headers);

 


//header('location:success_message_employer.php');
}
else{
  $_SESSION['error']= "Error!!! Please try again ";
}
}include_once('../includes/external_header.php'); 

?>

<style type="text/css">

.industy_tabs {
  margin-top:130px;
}
.industy_tabs h4{
    margin: 0px 0px;
}
.industy_tabs .panel-title a{
  font-size: 13px;
    font-weight: 600;
     color: #fff;
}
.industy_tabs .panel-body {
    background-color: #0c0c0c69;
    border-color: #8696b7;
    padding: 10px 15px;
    text-align: left;
        color: #eee;
    font-size: 14px;
}
.panel-default>.panel-heading {
  color: #fff;
  background-color: #0c0c0c69;
    border-color: #8696b7;
  padding: 0;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.panel-default>.panel-heading a {
  display: block;
  padding: 10px 15px;
  text-align: left;
}

.panel-default>.panel-heading a:after {
  /*content: "";*/
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.panel-default>.panel-heading a[aria-expanded="true"] {
  /*background-color: #eee;*/
}

.panel-default>.panel-heading a[aria-expanded="true"]:after {
  content: "\2212";
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}

.panel-default>.panel-heading a[aria-expanded="false"]:after {
  content: "\002b";
  -webkit-transform: rotate(90deg);
  transform: rotate(90deg);
}
</style>

<style>
.emp_profile h1,h2,h3,h4,h5,h6{
    
    font-family: 'Comfortaa', cursive;
    font-weight: 600;
}
p {
  font-family: 'Comfortaa', cursive;
    font-weight: 600;
}

.section-header h3 {
  font-family: 'Comfortaa', cursive;
     font-size: 42px!important;
    text-align: center;
    font-weight: 500;
    position: relative;
}

.numberCircle {
    width: 100px;
    line-height: 100px;
    border-radius: 50%;
    text-align: center;
    font-size: 32px;
    border: 3px solid #666;
    background:#666;


}

</style>

<!DOCTYPE html>
<html lang="en">

<body>

<!-- jQuery for customized datepicker -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Bootstrap for customized datepicker -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >




<div id="uploadimageModal" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Upload & Crop Image</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 text-center">
              <div id="image_demo" style="width:350px; margin-top:30px"></div>
            </div>
            <div class="col-md-4" style="padding-top:30px;">
                <br/>
                <br/>                    
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success crop_image">Crop & Upload Image</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </div>
  </div>
</div>




<div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/employer-background.jpg" height="400" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <p>FIND WORK IN AUSTRALIA</p>
            <h2 style="font-family: 'Comfortaa', cursive;">Search for staff your way</h2>
            <div>
              <a href="login.php" class="btn-get-started scrollto">LOGIN</a>
            </div>
         </div>

      </div>
    </div>

      <main id="main"> 
    <section id="about">
      <div class="container">
        <header class="section-header">
            <h3 style="color:red!important;">Overview</h3>
            <p>You may complete as many searches as you like. This will give you an overview of the Jobseekers Profile. Use tokens to unlock their contact information including phone number and email.
            <br><br>Jobseekers are required to keep their profile updated. Failure to update their profile may result in a poor rating and/or their account suspended.
            <br><br> If you use a token to unlock a profile and the Jobseeker is already employed, you may easily request a token refund in your account.</p>
            <div class="col-md-12" style="text-align: center;">
             <a href="#employer-step" class="btn-services scrollto abt_btn">TELL ME MORE ABOUT THIS</a>
           </div>
        </header>
        </div>
      </section>
  </main>

  </div>


  <div class="col-md-12 login_back">
  <div class="container">

    <div class="row">
      <div class="col-md-2"></div>

        <div class="col-md-8 login regist_head">
                    <?php
                      if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4> Error!</h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible' style='width: 100%;'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                         
                            ".$_SESSION['success']."
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
                    ?>

          <div class="row" id="register-top">
            <div class="col-md-1"></div>
              <div class="col-md-10">
               
                <div class="row" id="register-top">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-12">
                          <h3>Profile</h3>
                          <input type="hidden" id="g-token" name="g-token"/>
                          <label class="reg_label1">Business name <span class="star">*</span></label>
                          <input type="text" id="company_business_name" name="company_business_name" class="reg_box" autocomplete="off" required>

                          <label  class="reg_label">Logo</label>
                            <div class="custom-file-upload" style="margin-bottom: 50px!important;">                     
                              <input type="file" name="upload_image" id="upload_image"  />
                              <div class="col-md-12" style=" padding: 0px;">
                              <small>Maximum logo size: 800kb</small>
                            </div>

                            <div class="col-md-6" style=" padding: 0px;">
                              <img src="" id="uploaded_image" class="img-thumbnail img_position" />
                            </div>
                              <input type="hidden" name="file_location" id="file_location">
                            </div>

                            <label class="reg_label">Contact name <span class="star">*</span></label>
                            <input type="text" id="contact_name" name="contact_name" class="reg_box" autocomplete="off">
                     
                            <label class="reg_label">Email <span class="star">*</span></label>
                            <input type="email" id="email" name="email" class="reg_box" onchange="check(this.value)" autocomplete="off" required>
                            <small id="val" style="color:red;"></small>

                            <label class="reg_label">Password <span class="star">*</span></label>
                            <input type="password" id="password" name="password" class="reg_box" autocomplete="off" required>

                            <label class="reg_label">Phone Number <span class="star">*</span></label>
                            <input type="text" id="mobile" name="mobile" class="reg_box" autocomplete="off" required>
                            <small id="val" style="color:red;"></small>

                            <label class="reg_label" style="font-size: 13.9px;">A brief description of the products and services as part of the profile <span class="star">*</span></label>
                            <textarea id="description" name="description" class="reg_box"></textarea>

                            <label class="reg_label">Address <span class="star">*</span></label>
                            <input id="autocomplete" placeholder="Enter your address"  name="business_address" class="reg_box" autocomplete="off" type="text" required/>      
             
            
                            <label class="reg_label">Primary business industry</label>
                            <select id="primary_business_industry" name="primary_business_industry" class="reg_box" onchange='checkvalue(this.value)'>
                            <option value="" selected="selected" disabled>Select</option>
                                             <?php 
                                                        $result = mysqli_query($con,"SELECT DISTINCT industry_name,id from tbl_industry order by industry_name");
                                                        while($row = mysqli_fetch_array($result))
                                                          {  ?>
                                                     <option value="<?php echo $row['industry_name']; ?>"><?php echo $row['industry_name'];?></option>
                                                    
                                                    <?php }  ?> 
                                                    <option value="others">other</option>
                                             </select></br>
                                                          
                            <input type="text" name="other" id="other"   class="reg_box"   style='display:none; margin-top: 12px;'/>
                            <input type="submit" value="Submit" name="register_submit" class="login_btn">



                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>

        </div>
              

      </div>

  <main id="main"> 
    <section id="about">
      <div class="container" id="employer-step">
        <header class="section-header">
            <h3>How does it work?</h3>
            <h4 style="text-align:justify;font-family: 'Comfortaa', cursive;">Find Work Australia lets jobseekers create a free profile to be searched by employers.<br><br>
            When you complete search, a preview of the jobseekers profiles will be shown to you. Simply click to see more of the profile including age, gender, languages spoken and more.
            <br>You can also save profiles to compare later.
            <br><br>
            Jobseekers set their profiles to “Ready to start work”, “Employed but looking for a change” and “Employed”.
            <br>“Ready to start work” costs 2 tokens.
            <br>“Employed but looking for a change” costs 1 token.
            <br>If their change your status to ‘Employed’ the profile will be hidden until they are readyto be found again.

            </h4>
            <ul class="progressbar">
              <li><p>Create your account
              </p>
              <h5>There is very little you need to do, simply provide some basic information about who you are and buy some tokens.</h5>
              </li>

              <li><p>Conduct a search
              <h5>Search by pre-defined parameters such as age or gender or use search terms to find the right job ready candidate.</h5></p>
              
              </li>
              <li><p>Save your favourites</p>
              <h5>Easily compare profiles to find the one that looks best for your needs.</h5>
              </li>
              <li><p>Get in touch</p>
              <h5>When you are ready, use a token to unlock contact information including phone number and email.</h5>
              </li>
            </ul>



        </header>


        </div>
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
             <a href="#register-top" class="btn-services scrollto abt_btn">CREATE YOUR ACCOUNT</a>
           </div>
      </section>

  </main>



</div>


<div class="col-md-12 login_back">
<div class="col-md-12 industries">
  <div class="container">
    <h3>Popular Industries</h3>
    <div class="row">
      <div class="col-md-3">
        <img src="../img/icon1.png" style="height: 126px;width: 90px;">
        <h4>BARISTA | CAFE</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon2.png" style="height: 126px;width: 90px;">
         <h4>BAR | RESTAURANT</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon3.png" style="height: 126px;width: 90px;">
         <h4>FRUIT PICKING</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon4.png" style="height: 126px;width: 90px;">
         <h4>RETAIL</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon5.png" style="height: 126px;width: 90px;">
         <h4>CONSTRUCTION</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon6.png" style="height: 126px;width: 90px;">
         <h4>CUSTOMER | CALL CENTRE</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon7.png" style="height: 126px;width: 90px;">
         <h4>CLEANING</h4>
      </div>
      <div class="col-md-3">
        <img src="../img/icon8.png" style="height: 126px;width: 90px;">
         <h4>FACTORY</h4>
      </div>
    </div>

  <!-- <div class="col-md-12 "> -->
      <!-- <div class="container"> -->

  </div>
</div>
</div>

<div class="col-md-12 banner3">
  <div class="row">   
      <div class="col-md-12 banner_text">
        <div class="container">
     

  <div class="industy_tabs panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="row">
        <div class="col-md-6">
          <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:black;">
              CAFE | BARISTA 
              </a>
            </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <p>Working in the hospitality industry in Australia is great way to work and travel. Cafe’s are located in the key tourist locations and many have high and low seasons, so there are plenty of hiring opportunitues.</p>
                <p>You can work as a Barista without formal qualifications. We recommend you know the vocabularly extensively used in cafe’s and restaurants.</p>
              </div>
            </div>
          </div>
          <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo" style="color:black;">
                BAR | RESTAURANT
              </a>
            </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <p>In Australia you need a RSA certificate to server alcohol. Gaining this certificate is easy and can be completed online.</p>

                <p>To get an idea of pay rates, please <a target="_blank" href="https://www.payscale.com/research/AU/Job=Bar_Attendant/Hourly_Rate">click here.</a></p>
              </div>
            </div>
          </div>
           <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo" style="color:black;">
                FRUIT PICKING
              </a>
            </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                  <h5>When & where to go for fruit picking jobs Australia</h5>
                  <p>Before you are going to look for a fruit picking job, it would be useful to determine where you would like to go to. There are many possibilities of different fruit picking jobs as Australia is a huge country with numerous different landscapes and weather conditions.</p>
                  <h5>Queensland</h5>
                  <div class="row">
                    <p class="col-md-8">Queensland grows 1/3 of the nation’s fruit and vegetables. Farmers hire migrant workers, fruit & vegetables pickers and other (experienced) farmhands. Farm work varies across the Queensland region – the farmers grow sugar cane, beans, seasonal fruits and vegetables, also cattle, cotton and wool.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit1.png" height="auto" width="100%">
                    </div>
                  </div>
                  <p>In Tully they grow many bananas. Tully is located 180 kilometres south of Cairns and is known for being the wettest place in Australia, receiving an average of 4134 mm of rain per year. In the Tully area there are lots of beautiful beach towns such as Mission Beach and Bingal Bay. The Tully River is also famous among people who are crazy about rafting!</p>

                  <p>From Stanthorpe in the south to the Atherton Tablelands in the far North; more than 120 types of fruit and vegetable are picked and packed in the state every year all year round so new and motivated workers are needed to fill a variety of fruit picking and harvest jobs.</p>
                  <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Feb – Mar</td>
<td>Pears / Apples</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Rock Melon</td>
<td>St George</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Ginger</td>
<td>Sunshine Coast</td>
</tr>
<tr>
<td>Mar – Dec</td>
<td>Vegetables</td>
<td>Bundaberg</td>
</tr>
<tr>
<td>Apr – Jun</td>
<td>Citrus</td>
<td>Mundubbera</td>
</tr>
<tr>
<td>Apr – Oct</td>
<td>Various</td>
<td>Lockyer Valley</td>
</tr>
<tr>
<td>Apr – Nov</td>
<td>Beans</td>
<td>Mary Valley</td>
</tr>
<tr>
<td>Apr – Dec</td>
<td>Vegetables / Tomatoes</td>
<td>Bowen / Ayr</td>
</tr>
<tr>
<td>May – Oct</td>
<td>Broccoli</td>
<td>Toowoomba</td>
</tr>
<tr>
<td>May – Dec</td>
<td>Sugar Cane</td>
<td>Ayr / Ingham / Innisfail</td>
</tr>
<tr>
<td>Jul – Sep</td>
<td>Ginger</td>
<td>Sunshine Coast</td>
</tr>
<tr>
<td>Jul – Dec</td>
<td>Onions</td>
<td>Lockyer Valley</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Mangoes / Lychees /<br> Avocados / Bananas</td>
<td>Mareeba</td>
</tr>
<tr>
<td>Nov – Jan</td>
<td>Plums</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Nov – Jan</td>
<td>Cotton</td>
<td>Goodiwindi / St George</td>
</tr>
<tr>
<td>Oct – Jan</td>
<td>Peaches</td>
<td>Stanthorpe</td>
</tr>
<tr>
<td>Nov – Mar</td>
<td>Cotton</td>
<td>Toowoomba / Milleran</td>
</tr>
<tr>
<td>Dec – Jan</td>
<td>Bananas / Sugar</td>
<td>Innisfail / Tully</td>
</tr>
<tr>
<td>Dec – Mar</td>
<td>Various</td>
<td>Stanthorpe</td>
</tr>
</tbody>
</table>
              <h5>New South Wales</h5>
                  <div class="row">
                    <p class="col-md-8">New South Wales is one of best places to find seasonal farm jobs in Australia. The main produce in New South Wales include sheep, cattle, pigs, hay, apples, cherries, pears, legumes, maize, nuts, wool, wheat, and rice. Farmhands, migrant workers, fruit pickers, and vegetable pickers may all find work across New South Wales in fields and on ranches. November to April is the busiest harvest period which peaks in February. However, the harvest differs per crop.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit2.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>For example – Griffith is a thriving agricultural and industrial city boasting the third largest population in the Riverina region and many well regarded wineries. Next to grape picking it’s also possible to do orange picking or onion picking at different times of the year. Griffith is located approximately 600 kilometres west of Sydney. This country town is well known for its food, wine and festivals.</p>
                  
                  <table>
                  <tbody>
                  <tr>
                  <td><strong>Harvest</strong></td>
                  <td><strong>Crop</strong></td>
                  <td><strong>Location</strong></td>
                  </tr>
                  <tr>
                  <td>Jan – Mar</td>
                  <td>Stonefruit</td>
                  <td>Young</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Prunes</td>
                  <td>Young</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Pears</td>
                  <td>Orange</td>
                  </tr>
                  <tr>
                  <td>Feb – Mar</td>
                  <td>Grapes</td>
                  <td>Leeton / Hunter Valley</td>
                  </tr>
                  <tr>
                  <td>Feb – Apr</td>
                  <td>Apples</td>
                  <td>Orange</td>
                  </tr>
                  <tr>
                  <td>Mar – Apr</td>
                  <td>Grapes</td>
                  <td>Tumbarumba</td>
                  </tr>
                  <tr>
                  <td>Mar – May</td>
                  <td>Apples</td>
                  <td>Batlow</td>
                  </tr>
                  <tr>
                  <td>Mar – Jun</td>
                  <td>Cotton Picking</td>
                  <td>Narrabri / Mooree</td>
                  </tr>
                  <tr>
                  <td>Sep – Oct</td>
                  <td>Asparagus</td>
                  <td>Gundagai</td>
                  </tr>
                  <tr>
                  <td>Sep – Dec</td>
                  <td>Asparagus</td>
                  <td>Cowra</td>
                  </tr>
                  <tr>
                  <td>Sep – Apr</td>
                  <td>Oranges</td>
                  <td>Griffith</td>
                  </tr>
                  <tr>
                  <td>Nov – Dec</td>
                  <td>Cherries</td>
                  <td>Young / Orange</td>
                  </tr>
                  <tr>
                  <td>Nov – Apr</td>
                  <td>Oranges</td>
                  <td>Lecton</td>
                  </tr>
                  <tr>
                  <td>Dec – Jan</td>
                  <td>Onions</td>
                  <td>Griffith</td>
                  </tr>
                  <tr>
                  <td>Dec – Mar</td>
                  <td>Stone Fruit</td>
                  <td>Tumut / Batlow</td>
                  </tr>
                  <tr>
                  <td>Dec – Apr</td>
                  <td>Blueberries</td>
                  <td>Tumbarumba</td>
                  </tr>
                  </tbody>
                  </table>

                  <h5>Victoria</h5>
                  <div class="row">
                    <p class="col-md-8">November to April (peaking in February) is the main season for fruit and vegetable picking jobs in Victoria. There is plenty of work in the central northern areas around Shepparton. The Murray River area is also a great area to look for work; places such as Mildura and Swan Hill often require fruit picking / harvest workers. Main harvests in these areas include orchard fruits, tomatoes, tobacco, grapes and soft fruits.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit3.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>Fruit picking in Cobram – “Cobram is a large fruit growing area situated on the Murray River just 250 kilometres north of Melbourne. This region is well known for its irrigated fruit and dairy with large areas of stone fruit varieties. The small farm area expanded after WWII , when a significant number of Italian immigrants arrived and established themselves in the farming community.” –<a target="_blank" href="http://workstay.com.au/fruit-picking-cobram-victoria"> http://workstay.com.au/fruit-picking-cobram-victoria</a></p>
                    <p>Fruit picking Yarra Valley in Victoria – “This valley is about 40 kilometres north east of Melbourne. A large variety of cool climate crops have traditionally been grown in the Valley and there are more than 80 vineyards and wineries in the Valley with many different selections of wines.” –<a target="_blank" href="https://jobsearch.gov.au/"> https://jobsearch.gov.au/</a></p>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Tomatoes</td>
<td>Ardmona / Shepparton / Rochester</td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Pears / Peaches / Apples</td>
<td>Ardmona / Shepparton / Cobram</td>
</tr>
<tr>
<td>Jan – Apr</td>
<td>Tobacco</td>
<td>Ovens / King &amp; Kiewa Valleys</td>
</tr>
<tr>
<td>Feb – Mar</td>
<td>Grapes</td>
<td>Lake Boga / Swan Hill / Nyah West</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Ovens / King &amp; Kiewa Valleys</td>
</tr>
<tr>
<td>Mar – Apr</td>
<td>Apples</td>
<td>Buckland Valley / Stanley / Wandilong / Red Hill / Main Range</td>
</tr>
<tr>
<td>Sep – Nov</td>
<td>Asparagus</td>
<td>Dalmore</td>
</tr>
<tr>
<td>Oct – Dec</td>
<td>Strawberries</td>
<td>Silvan</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Cherries</td>
<td>Boweya / Glenrowan / Wagandary</td>
</tr>
<tr>
<td>Nov – Feb</td>
<td>Cherries / Berries</td>
<td>Wandin / Silvan</td>
</tr>
<tr>
<td>Nov – Dec</td>
<td>Tomato Weeding</td>
<td>Echuca / Rochester</td>
</tr>
</tbody>
</table>
              <h5>Tasmania</h5>
                  <div class="row">
                    <p class="col-md-8">Tasmania is all about apples. It grows almost a fifth of the apples in Australia, approximately 55,000 tonnes every year. The main areas are in the Huon district South of Hobart. Tasmania has a relatively short fruit picking season, generally from December to the end of May, fruit picking and harvest jobs are available all around Tasmania. As well as picking apples there are also pears, stone fruits, hops, grapes, berries and much more.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit4.png" height="auto" width="100%">
                    </div>
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Feb</td>
<td>Scallop Splitting</td>
<td>Bicheno</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Huon / Tamar Valley</td>
</tr>
<tr>
<td>Mar – Apr</td>
<td>Hops</td>
<td>Scottsdale / New Norfolk / Devenport</td>
</tr>
<tr>
<td>Mar – May</td>
<td>Apples</td>
<td>Hunter Valley / Tasman Peninsula / West Tamar</td>
</tr>
<tr>
<td>Dec – Jan</td>
<td>Soft Fruit</td>
<td>Channel District / Huon / Kingborough / Derwent Valley</td>
</tr>
</tbody>
</table>
                 <h5>South Australia</h5>
                  <div class="row">
                    <p class="col-md-8">fruit and vegetable picking jobs all year around e.g. picking citrus and soft fruits such as raspberries and strawberries around the Riverland area.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit5.png" height="auto" width="100%">
                    </div>
                    </div>
                    <p>February to April work is available around The Barossa Valley picking and packing the grapes.</p>

<table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Mar</td>
<td>Dried Fruits</td>
<td>Riverland</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes &amp; Peaches</td>
<td>Riverland</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Grapes</td>
<td>Southern Vales / Barossa Valley</td>
</tr>
<tr>
<td>Feb – Apr</td>
<td>Apples &amp; Pears</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Feb – Aug</td>
<td>Brussel Sprouts</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Jun – Aug</td>
<td>Oranges</td>
<td>Riverland</td>
</tr>
<tr>
<td>Jun – Sep</td>
<td>Pruning</td>
<td>Riverland</td>
</tr>
<tr>
<td>Sep – Jan</td>
<td>Oranges</td>
<td>Riverland</td>
</tr>
<tr>
<td>Oct – Feb</td>
<td>Strawberries</td>
<td>Adelaide Hills</td>
</tr>
<tr>
<td>Dec – Feb</td>
<td>Apricots</td>
<td>Riverland</td>
</tr>
</tbody>
</table>
                  <h5>Western Australia</h5>
                  <div class="row">
                    <p class="col-md-8">Fruit picking and harvest jobs are available in the Southwest of the state. From October to June you can harvest grapes and orchard fruits. March to November there is also fishing and processing work available for crayfish, prawns and scallops between Fremantle and Carnarvon. From May to October head northeast, around Kununurra for harvest jobs and fruit picking.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit6.png" height="auto" width="100%">
                    </div>
                    
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Jan – Mar</td>
<td>Grapes</td>
<td>Margaret River / Swan Valley / Mt Barker</td>
</tr>
<tr>
<td>Mar – May</td>
<td>Apples &amp; Pears</td>
<td>Pemberton / Donnybrook / Manjimup</td>
</tr>
<tr>
<td>Mar – Oct</td>
<td>Prawning &amp; Scalloping</td>
<td>Carnavon</td>
</tr>
<tr>
<td>May – Sep</td>
<td>Zucchini &amp; Rock Melons</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Apr – Nov</td>
<td>Melons</td>
<td>Coorow</td>
</tr>
<tr>
<td>Jun – Dec</td>
<td>Melons &amp; Tomatoes</td>
<td>Carnavon</td>
</tr>
<tr>
<td>Jul – Aug</td>
<td>Bananas</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Jul – Dec</td>
<td>Wildflowers</td>
<td>Coorow / Muchea</td>
</tr>
<tr>
<td>Oct – Jan</td>
<td>Mangoes</td>
<td>Kununurra</td>
</tr>
<tr>
<td>Nov – Jun</td>
<td>Rock Lobster</td>
<td>Fremantle / Kalbarri / Dongara / Geraldton / Broome</td>
</tr>
<tr>
<td>All Year</td>
<td>Fishing</td>
<td>Broome</td>
</tr>
</tbody>
</table>
               <h5>Northern Territory</h5>
                  <div class="row">
                    <p class="col-md-8">May to October picking melons around Katherine and Darwin is popular. From October to November head to Katherine and Darwin for mango picking and packing jobs as well as citrus and bananas which are grown all year around there.</p>
                    <div class="col-md-4">
                      <img src="../img/fruit7.png" height="auto" width="100%">
                    </div>
                    
                    </div>
                    <table>
<tbody>
<tr>
<td><strong>Harvest</strong></td>
<td><strong>Crop</strong></td>
<td><strong>Location</strong></td>
</tr>
<tr>
<td>Mar – Oct</td>
<td>Melons</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>Oct – Nov</td>
<td>Mangoes</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>All Year</td>
<td>Citrus / Bananas</td>
<td>Katharine / Darwin</td>
</tr>
<tr>
<td>All Year</td>
<td>Asian Vegetables &amp; Cutting Flowers</td>
<td>Darwin</td>
</tr>
</tbody>
</table>
<p>Information from:<a target="_blank" href="http://www.fruitpicking.org/fruit–picking–essentials/crops–calendar"> http://www.fruitpicking.org/fruit–picking–essentials/crops–calendar</a></p>

              </div>
            </div>
          </div>
           <div class="col-md-12 panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:black;">
                RETAIL
              </a>
            </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <p>Every part of Australia needs people to work in retail.</p>

                <p>Shop work can be physical, such as if you work in a warehouse and require long hours. Don’t forget to put in your profile if you have any physical issues that may be important.</p>

                <p><a target="_blank" href="https://www.payscale.com/research/US/Skill=Retail/Hourly_Rate">Click here</a> to see the Award Rates for retail in Australia.</p>
              </div>
            </div>
          </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="color:black;">
              CONSTRUCTION
            </a>
          </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
             <p>Building and Construction award rates can be found by clicking </p> <a target="_blank" href="http://awardviewer.fwo.gov.au/award/show/MA000020#P551_59778">Click here.</a>
            </div>
          </div>
        </div>
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="heading6">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6" style="color:black;">
              CUSTOMER SERVICE
            </a>
          </h4>
          </div>
          <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Customer Service covers everything from retail to call centres.</p>

                <p>If you are good with people, this could be a great option to get you working quicking with room to move into adminstration or management.</p>
            </div>
          </div>
        </div>

        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapseThree" style="color:black;">
              CLEANING
            </a>
          </h4>
          </div>
          <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Cleaning can be a rewarding job that could include domestic or commercial work.</p>

              <p>Experience will definately pay, so if you have any, put it in your profile!</p>

              <p><a target="_blank" href="https://www.payscale.com/research/US/Skill=Cleaning/Hourly_Rate">Click here</a> to get an idea of the wages you could be paid.</p>
            </div>
          </div>
        </div>
        <div class="col-md-12 panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseThree" style="color:black;">
              FACTORY
            </a>
          </h4>
          </div>
          <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <p>Factory work may not sound glamourous, but you can be assured that the training will be excellent and all legal conditions will be met.</p>

              <p><a target="_blank" href="https://www.payscale.com/research/US/Job=Factory_Worker/Hourly_Rate">Click here</a> to see a pay estimate for this industry.</p>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </div>


        </div>
      </div>

  </div>
</div>

  <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="../img/circles-v3.jpg" height="600" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <h2 style="font-family: 'Comfortaa', cursive;">
            Do you have something
            <br>you can offer our
            <br>jobseekers?</h2>
            <div>
              <input class="form-control" type="text" name="Email" placeholder="Email" style="width:300px;">
              <br>
              <a href="#" class="btn-get-started scrollto" style="width:300px;text-align:center;">SEND INFO</a>
            </div>
         </div>

      </div>
    </div>
    
    <?php include_once('../includes/external-pre-footer.php');?>

    <?php include_once('../includes/register_footer.php');?>
  </div>






  <style type="text/css">
   .progressbar {
      counter-reset: step;
      font-size: 20px;
      font-family: 'Comfortaa', cursive;
    }

    .progressbar li {
      list-style-type: none;
      float: left;
      width: 25%;
      position: relative;
      text-align: center
    }

    .progressbar li:before {
      content: counter(step);
      counter-increment: step;
      color: white;
      width: 50px;
      height: 50px;
      line-height: 50px;
      border: 3px solid #ddd;
      display: block;
      text-align: center;
      margin: 0 auto 10px auto;
      border-radius: 50%;
      background-color: #003E6B;
    }

    .progressbar li:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 1px;
      background-color: #ddd;
      top: 25px;
      left: -50%;
      z-index: -1;
    }

    .progressbar li:first-child:after {
      content:none;
    }

    .progressbar li.active {
      color: green;
    }



  </style>






  


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<!--     <script src="js/jquery.min.js"></script> -->

  <!-- <script src="lib/jquery/jquery.min.js"></script> -->



<!--   <script src="../lib/jquery/jquery-migrate.min.js"></script>
  <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../lib/easing/easing.min.js"></script>
  <script src="../lib/mobile-nav/mobile-nav.js"></script>
  <script src="../lib/wow/wow.min.js"></script>
  <script src="../lib/waypoints/waypoints.min.js"></script>
  <script src="../lib/counterup/counterup.min.js"></script>
  <script src="../lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="../lib/isotope/isotope.pkgd.min.js"></script>
  <script src="../lib/lightbox/js/lightbox.min.js"></script> -->

  <script src="../js/croppie.js"></script>
        <!-- Datepicker -->
 <!--  <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->

 <script src="https://www.google.com/recaptcha/api.js?render=6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <!-- Contact Form JavaScript File -->
  <script src="../contactform/contactform.js"></script>
 

  
  <!-- Template Main Javascript File -->
  <script src="../js/main.js"></script>


<script>
        grecaptcha.ready(function() {
          grecaptcha.execute('6LeiFLEZAAAAAINvKS-WcKAkpAc1COlBDZpWxmCK', {action: 'submit'}).then(function(token) {
              document.getElementById("g-token").value = token;
          });
        });
</script>

  <script>  
$(document).ready(function(){

  $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_emp_image.php",
        type: "POST",
        data:{"image": response},
  
        success:function(data)
        {
         
       
          $('#uploadimageModal').modal('hide');
          // $('#uploaded_image').html(data);
          $('#uploaded_image').attr("src",data);
          document.getElementById('file_location').value=data;
        }
      });
    })
  });

});  
</script>
   <script>
    function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
      autocomplete.setComponentRestrictions(
            {'country': ['Au']});

    });
  }
}

 </script>
   <script type="text/javascript">
    $(function () {
        $("#chkPassport").click(function () {
            if ($(this).is(":checked")) {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
    });
</script>   
  <script>
  function checkvalue(val)
     {
      if(val==="others")
      {
      $('#other').prop('required', true);
       document.getElementById('other').style.display='block';
      }
      
    else
    {
      $('#other').prop('required', false);
       document.getElementById('other').style.display='none'; 
    }

     }
</script>
<style>

#map {
  height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}


</style>
<style type="text/css">
  .file-upload-button{
    margin-left: -7.01% !important;
  }
  .regist_head h6{
    font-weight: 500;
    font-size: 18px;
    color: #062fa2;
  }
  /* .img-thumbnail {
    
  
    display: list-item;
} */
/* .img_position{
  display: block;
} */
</style>

 <!-- <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info" style="    margin-top: -75px;">
            <img src="img/LOGO_FOOTER.png">            
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Jobseeker</h4>
            <ul>
              <li><a href="#">Create a Jobseeker Profile</a></li>
              <li><a href="#">Jobseeker Login</a></li>
              <li><a href="#">Benefits</a></li>
              <li><a href="#">Overview</a></li>
             
            </ul>
          </div>
           <div class="col-lg-3 col-md-6 footer-links">
            <h4>Employers</h4>
            <ul>
              <li><a href="#">Create an Employer Account</a></li>
              <li><a href="#">Employer Login</a></li>
              <li><a href="#">Employer Overview</a></li> 
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Agency</h4>
            <ul>
              <li><a href="#">Agency</a></li>          
            </ul>
          </div>
          <div class="col-md-12 footer_job">
            <h1>Jobs</h1>
          </div>

         </div>
      </div>
    </div>
<div class="col-md-12" style="background-color: #002d5e;">
    <div class="container">
      <div class="row">
      <div class="col-md-6 copyright" style="text-align: left;">
       Find Work Australia
      </div>
      <div class="col-md-6 credits" style="text-align: right;">
           <i class="fa fa-facebook" aria-hidden="true"></i>
           <i class="fa fa-twitter" aria-hidden="true"></i>
           <i class="fa fa-google-plus" aria-hidden="true"></i>
           <i class="fa fa-instagram" aria-hidden="true"></i>
           <i class="fa fa-wifi" aria-hidden="true"></i>
      </div>
    </div>
    </div>
  </div>
  </footer>  -->


<script type="text/javascript">

;(function($) {

      // Browser supports HTML5 multiple file?
      var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
          isIE = /msie/i.test( navigator.userAgent );

      $.fn.customFile = function() {

        return this.each(function() {

          var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
              $wrap = $('<div class="file-upload-wrapper">'),
              $input = $('<input type="text" class="file-upload-input" />'),
              // Button that will be used in non-IE browsers
              $button = $('<button type="button" class="file-upload-button">Select a File</button>'),
              // Hack for IE
              $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

      
          $file.css({
            position: 'absolute',
            left: '-9999px'
          });

          $wrap.insertAfter( $file )
            .append( $file, $input, ( isIE ? $label : $button ) );

          // Prevent focus
          $file.attr('tabIndex', -1);
          $button.attr('tabIndex', -1);

          $button.click(function () {
            $file.focus().click(); // Open dialog
          });

          $file.change(function() {

            var files = [], fileArr, filename;

            // If multiple is supported then extract
            // all filenames from the file array
            if ( multipleSupport ) {
              fileArr = $file[0].files;
              for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                files.push( fileArr[i].name );
              }
              filename = files.join(', ');

            // If not supported then just take the value
            // and remove the path to just show the filename
            } else {
              filename = $file.val().split('\\').pop();
            }

            $input.val( filename ) // Set the value
              .attr('title', filename) // Show filename in title tootlip
              .focus(); // Regain focus

          });

          $input.on({
            blur: function() { $file.trigger('blur'); },
            keydown: function( e ) {
              if ( e.which === 13 ) { // Enter
                if ( !isIE ) { $file.trigger('click'); }
              } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                // On some browsers the value is read-only
                // with this trick we remove the old input and add
                // a clean clone with all the original events attached
                $file.replaceWith( $file = $file.clone( true ) );
                $file.trigger('change');
                $input.val('');
              } else if ( e.which === 9 ){ // TAB
                return;
              } else { // All other keys
                return false;
              }
            }
          });

        });

      };

      // Old browser fallback
      if ( !multipleSupport ) {
        $( document ).on('change', 'input.customfile', function() {

          var $this = $(this),
              // Create a unique ID so we
              // can attach the label to the input
              uniqId = 'customfile_'+ (new Date()).getTime(),
              $wrap = $this.parent(),

              // Filter empty input
              $inputs = $wrap.siblings().find('.file-upload-input')
                .filter(function(){ return !this.value }),

              $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

          // 1ms timeout so it runs after all other events
          // that modify the value have triggered
          setTimeout(function() {
            // Add a new input
            if ( $this.val() ) {
              // Check for empty fields to prevent
              // creating new inputs when changing files
              if ( !$inputs.length ) {
                $wrap.after( $file );
                $file.customFile();
              }
            // Remove and reorganize inputs
            } else {
              $inputs.parent().remove();
              // Move the input so it's always last on the list
              $wrap.appendTo( $wrap.parent() );
              $wrap.find('input').focus();
            }
          }, 1);

        });
      }

}(jQuery));

$('input[type=file]').customFile();
</script>

</body>
</html>