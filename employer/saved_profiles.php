<?php 
session_start(); 
$category=$tag=$location="";
if(!isset($_SESSION['employer_id']))
{
$_SESSION['msg']="Please login to view results";
header('location:../employer/login.php');
}
include_once('../includes/config.php');
include_once('../includes/external_header.php'); 
?>
    <link rel="stylesheet" href="../admin/css/font-awesome.min.css">  
 
    
<?php
$sql_search="select * from tbl_jobseeker where id in (select jobseeker_id from tbl_saved_profile where employer_id='".$_SESSION['employer_id']."')";
if($sql_search!="")
{
    $res=$con->query($sql_search);


?>
    <div class="site-section bg-light">
      <div class="container" style="margin-bottom:50px;" >
        <div class="row justify-content-start text-left mb-5">
          <div class="col-md-9" data-aos="fade">
            <h2 class="font-weight-bold text-black" style="margin-top:100px;">Saved Profiles</h2>
          </div>
          <!-- <div class="col-md-3" data-aos="fade" data-aos-delay="200">
            <a href="#" class="btn btn-primary py-3 btn-block"><span class="h5">+</span> Post a Job</a>
          </div> -->
        </div>
        <?php 
        $i=0;
        while($row=$res->fetch_assoc())
        {
            $loc="";
            $sql_location="select * from tbl_towns where id='".$row['live_in']."'";
            $res_location=$con->query($sql_location);
            if($row_location=$res_location->fetch_assoc())
            {
                $loc=$row_location['name'];

            }

            $job="";
            if($row['no_qualification']!="")
            {
            $sql_job="select * from tbl_industry where id in (".$row['no_qualification'].")";
            $res_job=$con->query($sql_job);
            while($row_job=$res_job->fetch_assoc())
            {
                if($job=="")
                {
                    $job=$row_job['industry_name'];
                }
                else
                {
                $job=$job."<br>".$row_job['industry_name'];
                }

            }
          }



        ?>

        <div class="row" data-aos="fade" id="result_div" >
         <div class="col-md-12">

           <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">

              <div class="mb-4 mb-md-0 mr-5">
            <div class="job-post-item-header d-flex align-items-center">
                 <h2 class="mr-3 text-black h4"><?php echo $row['first_name']." ".$row['last_name']; ?></h2>
                 <?php
                 if($row['work_status']=="1")
                 {
                   ?>
               <img src="../img/one-token.png"  height="30" width="30">
                  <?php
                }
              
                 else  
                 {
                   ?>
                     <img src="../img/two-token.png"  height="30" width="30">
                
                 <?php
                 }
                 ?>

                 <?php
                 if($row['gov_support']=="Yes")
                 {
                   ?>
                   <label>
                  
                  <!-- &#9989; MAY COME WITH GOVERNMENT SUPPORT</label> -->
                 &nbsp;
                 <i class="fa fa-university" aria-hidden="true"></i>
                  MAY COME WITH GOVERNMENT SUPPORT

                 <?php
                 }
              
                 else 
                 {
                   ?>
                      <!-- <input type="checkbox" name="gov_app" readonly/> -->
                    <?php
                 }
                 ?>
              
               </div>
               
               <div><span class="fl-bigmug-line-big104"></span> <span><?php echo $loc; ?></span></div>
               <div class="job-post-item-body d-block d-md-flex">
                 <div class="mr-3"><span ></span> <a href="#"><?php echo $job; ?></a></div>
              
            
            
               </div>
               <div class="mr-3" style="margin-top:5px;"><span >Bio:</span> <a href="#"><?php echo $row['bio'];  ?></a></div> 
              <div class="mr-3"> 
           

               <?php
                 if($row['gender']=="male")
                 {
                   ?>
                   <i class="fa fa-male" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                  <?php
                 }
              
                 else 
                 {
                   ?>
                     <i class="fa fa-female" aria-hidden="true"></i> <?php echo $row['gender'];  ?></a>
                
                 <?php
                 }
                 ?>
              </div> 

               <?php 
                 if($row['visa_type']!="")
                 {
                     ?>
                  <div class="badge-wrap">
                
               
               
                
                 </div>
                </br> <span class="bg-primary text-white badge py-2 px-3"><?php echo $row['visa_type']; ?></span>
                 <?php 
                 }
                 ?>
              </div>

              <div class="ml-auto">
                <!-- <a href="#" class="btn btn-secondary rounded-circle btn-favorite text-gray-500"><span class="icon-heart"></span></a> -->
                <a target="_blank" href="candidate.php?id=<?php echo $row['id']; ?> " class="btn btn-primary py-2">View</a>
              </div>
           </div>

         </div>
        </div>
        <?php
        $i++;
        }
        ?>

      

       


      </div>
    </div>    

    <?php
}
    ?>
    <!-- <div class="site-section bg-light" style="margin-bottom:50px;"></div> -->

   <?php include_once('../includes/external_footer.php'); ?>

  