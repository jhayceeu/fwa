<?php 
session_start();
include_once('includes/config.php');
include_once('includes/header.php');

?>


<!DOCTYPE html>
<html>

<body>

<style>
	.column {
		float: left;
		width: 33.33%;
		padding: 5px;
	}
</style>

  <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="img/Jobseeker-top.jpg" height="500" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:180px;">
            <p>BENEFITS FOR JOBSEEKERS</p>
            <h2 style="font-family: 'Comfortaa', cursive;">Free with your profile!</h2>
            <div>
              <a href="jobseeker/login.php" class="btn-get-started scrollto">LOGIN</a>
              <a href="#" class="btn-services scrollto">LEARN MORE</a>
            </div>
         </div>

      </div>
    </div>
   </div>

  <div class="container">
   <div class="row">
  <div class="column">
    <img src="img/Resume.jpg" style="width:100%">
  </div>
  <div class="column">
    <img src="img/Grocery.jpg" alt="Forest" style="width:100%">
  </div>
  <div class="column">
    <img src="img/Aust-Ph.jpg" alt="Mountains" style="width:100%">
  </div>
</div>

  <main id="main"> 
    <section id="about">
      <div class="container" id="jobseeker-step">
        <header class="section-header">
            <h3>Create a profile to unlock these great benefits.</h3>
            <br>
            <h5 style="text-align:justify;font-family: 'Comfortaa', cursive;">Join the other Jobseekers getting found on Find Work Australia!
            <br>
			Your profile it free and easy to make. You will need an email address and we highly recommend an Australian mobile phone number.</h5>

        </header>


        </div>
            <div class="col-md-12" style="text-align: center; margin-bottom: 20px;">
             <a href="jobseeker/register.php" class="btn-services scrollto abt_btn">CREATE YOUR PROFILE</a>
           </div>
      </section>

  </main>



</div>

  <div class="col-md-12" class="login_banner">
    <div class="row">
      <img src="img/circles-v3.jpg" height="600" width="100%">
       <div class="container">
         <div class="intro-info" style="position:absolute;top:140px;">
            <h2 style="font-family: 'Comfortaa', cursive;">
            Learn more about how a 
            <br>professional CV can help 
            <br>get you the job you want.</h2>
            <div>
              <input class="form-control" type="text" name="Email" placeholder="Email" style="width:300px;" >
              <br>
              <a href="#" class="btn-get-started scrollto" style="width:300px;text-align:center;">SUBSCRIBE</a>
            </div>
         </div>

      </div>
    </div>
  </div>

</body>
</html>
<?php include_once('includes/pre-footer.php'); ?>

<?php include_once('includes/footer.php'); ?>