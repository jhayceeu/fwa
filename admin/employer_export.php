<?php

require_once('../includes/config.php');

if(isset($_POST['export']))
{
	$today = date('Ymd');
	$filename = "employer-export-" . $today . ".csv";

	header('Content-Description: File Transfer');
	header('Content-Type: application/csv');
	header("Content-Disposition: attachment; filename=". $filename);
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	$output = fopen('php://output', 'w');

	$headerRow = ['COMPANY NAME', 'CONTACT NAME', 'CONTACT NUMBER', 'EMAIL', 'DESCRIPTION OF PRODUCTS', 'BUSINESS ADDRESS', 'BUSINESS INDUSTRY', 'TOKEN BALANCE'];

	fputcsv($output, $headerRow);

	$query = "select company_business_name, contact_name, mobile, email, description_products, business_address, primary_business_industry, extra_tokens from tbl_employer";
	$result = $con->query($query);
	while($row=$result->fetch_assoc()){

		fputcsv($output, $row);
	}
	fclose($output);
}
?>