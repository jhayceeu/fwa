<?php
session_start();
include "../includes/config.php";
include "includes/header.php";

$root = $_SERVER['DOCUMENT_ROOT'];

?>
<link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
<body class="materialdesign">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Header top area start-->
    <div class="wrapper-pro">
        <?php include "includes/side_bar.php"; ?>
        <div class="content-inner-all">
            <?php include "includes/top.php"; ?>
            <!-- Header top area end-->
            <!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<div class="breadcome-heading">
											<form role="search" class="">
												<input type="text" placeholder="Search..." class="form-control">
												<a href=""><i class="fa fa-search"></i></a>
											</form>
										</div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->
      <?php include_once('includes/sidebar_mobile.php'); ?>
            <!-- Breadcome start-->
            <div class="breadcome-area des-none">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                            <form role="search" class="">
												<input type="text" placeholder="Search..." class="form-control">
												<a href=""><i class="fa fa-search"></i></a>
											</form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->
            <!-- welcome Project, sale area start-->
            
            <!-- welcome Project, sale area start-->
            <!-- stockprice, feed area start-->
            
            <!-- stockprice, feed area end-->
            <!-- Data table area Start-->
            <div class="admin-dashone-data-table-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd">
                                        <h1>Dashboard</h1>
                                        
                                    </div>
                                </div>
                                <div class="">
                                    <div class="" style="margin-top: 40px;padding-left: 10px;padding-right: 10px;">
                                    <div class="stockprice-feed-project-area">
                <div class="container-fluid">
                    <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="income-dashone-total income-monthly shadow-reset nt-mg-b-30 res-mg-t-30">
                                <div class="income-title">
                                    <div class="main-income-head">
                                        <h2>Employer</h2>
                                        <div class="main-income-phara">
                                            <!-- <p>Monthly</p> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="income-dashone-pro">
                                    <div class="income-rate-total">
                                        <div class="price-adminpro-rate">
                                            <?php
                                            $cnt="0";
                                                $sql="select count(*) as cnt from tbl_employer";
                                                $res=$con->query($sql);
                                                if($row=$res->fetch_assoc())
                                                {
                                                    $cnt=$row['cnt'];
                                                }
                                            ?>
                                            <h3><span>#</span><span class="counter"><?php echo number_format($cnt); ?></span></h3>
                                        </div>
                                        <!-- <div class="price-graph">
                                            <span id="sparkline1"></span>
                                        </div> -->
                                    </div>
                                    <div class="income-range">
                                        <p>Total Employers</p>
                                        <!-- <span class="income-percentange">98% <i class="fa fa-bolt"></i></span> -->
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                          
                           
                        </div>
                        
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                           
                           
                            <div class="income-dashone-total visitor-monthly shadow-reset nt-mg-b-30">
                                <div class="income-title">
                                    <div class="main-income-head">
                                        <h2>Jobseekers</h2>
                                        <div class="main-income-phara visitor-cl">
                                            <!-- <p>Today</p> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="income-dashone-pro">
                                    <div class="income-rate-total">
                                        <div class="price-adminpro-rate">
                                        <?php
                                        $cnt="0";
                                                $sql="select count(*) as cnt from tbl_jobseeker";
                                                $res=$con->query($sql);
                                                if($row=$res->fetch_assoc())
                                                {
                                                    $cnt=$row['cnt'];
                                                }
                                            ?>
                                            <h3><span>#</span><span class="counter"><?php echo number_format($cnt); ?></span></h3>
                                        </div>
                                        <!-- <div class="price-graph">
                                            <span id="sparkline2"></span>
                                        </div> -->
                                    </div>
                                    <div class="income-range visitor-cl">
                                        <p>Total Jobseekers</p>
                                        <!-- <span class="income-percentange">55% <i class="fa fa-level-up"></i></span> -->
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                           
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                           
                           
                            <div class="income-dashone-total user-monthly shadow-reset nt-mg-b-30">
                                <div class="income-title">
                                    <div class="main-income-head">
                                        <h2>Refund Requests</h2>
                                        <div class="main-income-phara visitor-cl">
                                            <!-- <p>Today</p> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="income-dashone-pro">
                                    <div class="income-rate-total">
                                        <div class="price-adminpro-rate">
                                        <?php
                                        $cnt="0";
                                                $sql="select count(*) as cnt from tbl_refund where status=0";
                                                $res=$con->query($sql);
                                                if($row=$res->fetch_assoc())
                                                {
                                                    $cnt=$row['cnt'];
                                                }
                                            ?>
                                            <h3><span>#</span><span class="counter"><?php echo number_format($cnt); ?></span></h3>
                                        </div>
                                        <!-- <div class="price-graph">
                                            <span id="sparkline2"></span>
                                        </div> -->
                                    </div>
                                    <div class="income-range visitor-cl">
                                        <p>New Refund Requests</p>
                                        <!-- <span class="income-percentange">55% <i class="fa fa-level-up"></i></span> -->
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                           
                        </div>


                    </div>
                </div>
            </div>  
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data table area End-->
        </div>
    </div>

    <?php include "includes/footer.php"; ?>
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->

    <!-- <script src="js/jquery-3.3.1.min.js"></script> -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example1').DataTable({
            responsive: true
        });
        //$('#myModal').modal('show');
    });
    </script>
</body>

</html>