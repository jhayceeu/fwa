<?php
session_start();
if(!isset($_SESSION['admin']))
{
    header ('location:index.php');
}
include "../includes/config.php";
$job_name=$category=$edit_job_name=$edit_job_id=$edit_category="";
if(isset($_POST['submit']))
{
    $job_name=addslashes($_POST['job_name']);
    $category=addslashes($_POST['category']);
   
   
    $sql="insert into tbl_jobs (job_name,category) values ('$job_name','$category')";
    if($con->query($sql))
    {
        $_SESSION['success']="Job Saved";
    }
    else
    {
        $_SESSION['error']= $con->error;
    }
}
if(isset($_POST['update']))
{
    $edit_job_name=addslashes($_POST['edit_job_name']);
    $edit_category=addslashes($_POST['edit_category']);
    $edit_job_id=$_POST['edit_job_id'];
   
    $sql="update tbl_jobs set job_name='".$edit_job_name."',category='".$edit_category."' where id='".$edit_job_id."'";
    if($con->query($sql))
    {
        $_SESSION['success']="Job Updated";
    }
    else
    {
        $_SESSION['error']= $con->error;
    }
}
include "includes/header.php";
?>



<link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
<style>
    .abc{
        background-color:bisque ;
         padding:12px;
         margin:2px;
         font-size:13px;
     }
     .def{
        padding:20px;
         background-color:gainsboro;
         text-align:right;
         font-weight:550;
         color:cornflowerblue;
         font-size:13px;
     }
     /* .pagination>.active>a:hover
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%);
     }
     .pagination>.active>a
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%);
     }
     .pagination>li.active>a:hover
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%);
     }
     .pagination>.active>a:focus
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%);  
     }
     .pagination>.active>span
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%);  
     }
     .pagination>.active>span:focus
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%); 
     }
     .pagination>.active>span:hover
     {
        background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%); 
     } */


     
     /* ,  , , .pagination>.active>span:hover{
  background-color: linear-gradient(to bottom, #ff9966 0%, #d75151 100%); */


</style>

<body class="materialdesign">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Header top area start-->
    <div class="wrapper-pro">
        <?php include "includes/side_bar.php"; ?>
        <div class="content-inner-all">
            <?php include "includes/top.php"; ?>
            <!-- Header top area end-->
            <!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                
            </div>
            <!-- Breadcome End-->
            <?php include_once('includes/sidebar_mobile.php'); ?>
            <!-- Breadcome start-->
            <div class="breadcome-area des-none">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                            <form role="search" class="">
												<input type="text" placeholder="Search..." class="form-control">
												<a href=""><i class="fa fa-search"></i></a>
											</form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="dashboard.php">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Jobs</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->
            <!-- welcome Project, sale area start-->
            
            <!-- welcome Project, sale area start-->
            <!-- stockprice, feed area start-->
            
            <!-- stockprice, feed area end-->
            <!-- Data table area Start-->
            <div class="admin-dashone-data-table-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd">
                                        <h1>Jobs</h1>
                                        <div class="sparkline8-outline-icon">
                                            <div class="breadcome-heading">
                                            <form role="search" class="">
												<input type="button" data-toggle="modal" data-target="#modalAddCard"  placeholder="Search..." class="form-control" value="ADD NEW">
                                                <a href=""><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                            </form>
                                            </div>
                                            <!-- <span class="sparkline8-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                            <span><i class="fa fa-wrench"></i></span>
                                            <span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="" style="margin-top: 40px;padding-left: 10px;padding-right: 10px;">
                                      
                                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example1">
                                            <thead>
                                                <tr>
                                                    <!-- <th data-field="state" data-checkbox="true"></th> -->
                                                    <th data-field="id">Sl No</th>
                                                    <th >Job</th>
                                                    <th width="40%">Category</th>
                                                    <th >Action</th>
                                                  
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                           <?php
                                           $i=0;
                                           $sql="select * from tbl_jobs";
                                           $res=$con->query($sql);
                                           while($row=$res->fetch_assoc())
                                           {
                                               $cat="";
                                               if($row['category']!="")
                                               {
                                                   if($row['category']=="SQ")
                                                   {
                                                    $cat="Some Qualification";
                                                   }
                                                   else if($row['category']=="NQ")
                                                   {
                                                    $cat="No Qualification";
                                                   }
                                                   else if($row['category']=="UM")
                                                   {
                                                    $cat="University or Multiple Years of Study";
                                                   }
                                               }
                                                $i++;
                                           

                                           ?>
                                           <tr>
                                           <td><?php echo $i; ?></td>
                                           <td><?php echo $row['job_name']; ?></td>
                                           <td><?php echo $cat; ?></td>
                                           
                                          
                                            <td>
                                           <a onclick="edit_token(<?php echo $row['id'] ?>)" class="btn btn-primary" style="background-color: #fff;color: #2e6da4;"><i class="fa fa-pencil"></i></a>
                                            <a onclick="delete_token(<?php echo $row['id'] ?>)" class="btn btn-danger" style="background-color: #fff;color: red;"><i class="fa fa-trash"></i></a>  </td>
                                           </tr>
                                             <?php
                                             }
                                             ?>   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data table area End-->
        </div>
    </div>
<!--Modal: modalAddCard-->
<div class="modal fade" id="modalAddCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" style="position: absolute;">
  <div class="modal-dialog modal-md modal-notify modal-danger" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center" style="background-color:NavajoWhite;color:cornflowerblue;height:50px">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      <p class="heading" style="font-size:20px;">Add Job</p>
      </div>

      <!--Body-->
	  <form method="POST">
      <div class="modal-body" style="text-align:left;">
      <div class="container col-md-12">
	  	<div class="form-group" >
            <div class="col-md-4 abc def">
                Job Name *
            </div>
            <div class="col-md-7 abc">
                <input type="text" name="job_name" id="job_name" class="form-control" required/>
            </div>
            <div class="col-md-4 abc def">
                Category
            </div>
            <div class="col-md-7 abc">
                <select class="form-control ymmModel" name="category" id="category"  required>
                                    <option value="">--Select One--</option>
                                    <option value="SQ">Some Qualification</option>
                                    <option value="NQ">No Qualification</option>
                                    <option value="UM">University or Multiple Years of Study</option>
                                   
                                    
                                </select>
            </div>
          
            
           
            
           
            
         </div>
        <!-- <i class="fas fa-times fa-4x animated rotateIn"></i> -->
</div>
      </div>
	  
      <!--Footer-->
      <div class="modal-footer flex-center">
        <button type="submit" name="submit" style="margin-top: 30px; margin-right: 50px;width: 120px;" class="btn btn-warning">Submit</button>
        <!-- <a type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</a> -->
     
      </div>
      </form>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalAddCard-->


<!--Modal: modalAddCard-->
<div class="modal fade" id="edit_myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" style="position: absolute;">
  <div class="modal-dialog modal-md modal-notify modal-danger" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center" style="background-color:NavajoWhite;color:cornflowerblue;height:50px">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      <p class="heading" style="font-size:20px;">Add Job</p>
      </div>

      <!--Body-->
	  <form method="POST">
      <div class="modal-body" style="text-align:left;">
      <div class="container col-md-12">
	  	<div class="form-group" >
            <div class="col-md-4 abc def">
                Job Name *
            </div>
            <div class="col-md-7 abc">
            
            <input type="hidden" name="edit_job_id" id="edit_job_id" />
                <input type="text" name="edit_job_name" id="edit_job_name" class="form-control" required/>
            </div>
            <div class="col-md-4 abc def">
               Category
            </div>
            <div class="col-md-7 abc">
                <select class="form-control ymmModel" name="edit_category" id="edit_category"  required>
                                    <option value="">--Select One--</option>
                                    <option value="SQ">Some Qualification</option>
                                    <option value="NQ">No Qualification</option>
                                    <option value="UM">University or Multiple Years of Study</option>
                                    
                                </select>
            </div>
           
         </div>
</div>
      </div>
	  
      <!--Footer-->
      <div class="modal-footer flex-center">
        <button type="submit" name="update" class="btn btn-warning" style="margin-top: 30px; margin-right: 50px;width: 120px;">Submit</button>
        <!-- <a type="button" class="btn  btn-danger waves-effect" data-dismiss="modal">No</a> -->
        </form>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalAddCard-->
<?php include "includes/footer.php"; ?>

    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->

    <!-- <script src="js/jquery-3.3.1.min.js"></script> -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example1').DataTable({
            responsive: true
        });
        //$('#myModal').modal('show');
    });
    function delete_token(id)
    {
        var r = confirm("confirm deletion " );
    if (r == true) {
        $.ajax({
          url: "ajax/delete_job.php",
    			type: 'POST',
    			data: {id:id},
     			success:function(data)
           {
            
            window.location.replace('jobs.php');
           }
				});
    }

    }
    function edit_token(id)
    {
       
        $.ajax({
    type: 'POST',
    url: 'ajax/edit_job.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
    //  alert(response.job_name);
      $('#edit_job_name').val(response.job_name);
      $('#edit_category').val(response.category);
      $('#edit_job_id').val(response.id);
      $('#edit_myModal').modal('show');
    
    }
  });
    }
    </script>
</body>
</html>