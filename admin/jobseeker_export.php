<?php

require_once('../includes/config.php');

if(isset($_POST['export']))
{
	$today = date('Ymd');
	$filename = "jobseeker-export-" . $today . ".csv";

	header('Content-Description: File Transfer');
	header('Content-Type: application/csv');
	header("Content-Disposition: attachment; filename=". $filename);
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	$output = fopen('php://output', 'w');
	$headerRow = ['FIRSTNAME', 'PREFERRED NAME', 'LASTNAME', 'EMAIL', 'AUSTRALIAN NUMBER', 'AGE BRACKET', 'GENDER', 'WORK TYPE', 'INDUSTRIES', 'LOCATION', 'STAY FROM', 'STAY TO', 'VISA TYPE', 'EXPIRE DATE', 'VEHICLE LICENSE', 'IMAGE', 'BIO', 'GOVERNMENT SUPPORT'];

	fputcsv($output, $headerRow);

	$query = "select first_name, prefered_name, last_name, email, australian_mob, age_braket, gender, type_work, industry_name, name, stay_months_from, stay_months_to, visa_type, expire_date, drive_car, image, bio, gov_support from tbl_jobseeker js left join tbl_towns tw on js.live_in = tw.id left join tbl_industry ind on js.no_qualification = ind.id order by last_name, first_name";
	$result = $con->query($query);
	while($row=$result->fetch_assoc()){

		fputcsv($output, $row);
	}
	fclose($output);
}
?>