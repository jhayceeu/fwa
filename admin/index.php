<?php 
session_start();
if(isset($_SESSION['admin']))
{
	header ('location:dashboard.php');
}
include "../includes/config.php";

$admin=$pass=$remember=$error="";
if(isset($_COOKIE['admin_name']))
{
	$admin=$_COOKIE['admin_name'];
	$pass=$_COOKIE['pass_word'];
	$remember="on";
}
if(isset($_POST['login']))
{
	$admin=$_POST['adminname'];
	$pass=$_POST['pass'];
	//$remember
	if(isset($_POST['remember']))
	{
		$remember=$_POST['remember'];
		setcookie("admin_name",$admin,time() + (1200));
		setcookie("pass_word",$pass,time() + (1200));
	}
	else
	{
		setcookie("admin_name", "", time() - 1);
		setcookie("pass_word", "", time() - 1);
		$remember="";
	}
	if($res=$con->query("select * from tbl_admin where email='$admin' and password='".md5($pass)."'"))
	{
		if($res->num_rows>0)
		{
			$_SESSION['admin']=$admin;
			header ('location:dashboard.php');
		}
		else
		{
			$error="Incorrect adminname or password";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">



<head>
	<title>Jobseeker</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter" >
		<div class="container-login100" style="background-color:darkslategrey!important">
			<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
				<form method="POST" class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-32">
						Admin Login
					</span>

					<span class="txt1 p-b-11">
						Admin name
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Admin name is required">
						<input class="input100" type="text" name="adminname" value="<?php echo $admin; ?>" required>
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						Password
					</span>
					<div class="wrap-input100 validate-input m-b-12" data-validate = "Password is required">
						<span class="btn-show-pass" onmousedown="show_pass()" onmouseup="show_pass()">
							<i class="fa fa-eye"></i>
						</span>
						<input class="input100" type="password" name="pass" id="pass" value="<?php echo $pass; ?>" required>
						<span class="focus-input100"></span>
					</div>
					
					<div class="flex-sb-m w-full p-b-48">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember" <?php if($remember) echo "checked"; ?>>
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="#" class="txt3">
								Forgot Password?
							</a>
						</div>
					</div>
					<span style="margin-top:-20px;margin-bottom:10px;color:red"><?php echo $error;?></span>
					<div class="container-login100-form-btn">
						<button type="submit" name="login" class="login100-form-btn">Login</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	
<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>

<script>
function show_pass()
{
	// $('#pass').type="text";
	var pass = document.getElementById('pass');
	if(pass.type=='text')
		pass.type='password';
	else
		pass.type = 'text';
}
</script>

</html>