<?php

//import.php

include '../vendor/autoload.php';

$connect = new PDO("mysql:host=localhost;dbname=jbseeker","jbsusr","P3Ki7915s!8u");

if($_FILES["import_excel"]["name"] != '')
{
	$allowed_extension = array('xls', 'csv', 'xlsx');
	$file_array = explode(".", $_FILES["import_excel"]["name"]);
	$file_extension = end($file_array);

	if(in_array($file_extension, $allowed_extension))
	{
		$file_name = time() . '.' . $file_extension;
		move_uploaded_file($_FILES['import_excel']['tmp_name'], $file_name);
		$file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

		$spreadsheet = $reader->load($file_name);

		unlink($file_name);

		$data = $spreadsheet->getActiveSheet()->toArray();
		array_shift($data);
		foreach($data as $row)
		{
			$insert_data = array(
				':company_business_name'		=>	$row[0],
				':contact_name'		=>	$row[1],
				':mobile'		=>	$row[2],
				':email'		=>	$row[3],
				':password'		=>  md5($row[4]),
				':description_products'	=>	$row[5],
				':business_address'			=>	$row[6],
				':primary_business_industry'		=>	$row[7]
			);

			$query = "
			INSERT INTO tbl_employer
			(company_business_name, contact_name, mobile, email, password, description_products, business_address, primary_business_industry) 
			VALUES (:company_business_name, :contact_name, :mobile, :email, :password, :description_products, :business_address, :primary_business_industry)
			";

			$statement = $connect->prepare($query);
			$statement->execute($insert_data);
		}
		$message = '<div class="alert alert-success">Data Imported Successfully</div>';

	}
	else
	{
		$message = '<div class="alert alert-danger">Only .xls .csv or .xlsx file allowed</div>';
	}
}
else
{
	$message = '<div class="alert alert-danger">Please Select File</div>';
}

echo $message;

?>