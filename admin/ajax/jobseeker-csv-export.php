<?php


$today = date('Ymd');
$filename = "jobseeker-export-" . $today . ".csv";

header('Content-Description: File Transfer');
header('Content-Type: application/csv');
header("Content-Disposition: attachment; filename=". $filename);
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$handle = fopen('php://output', 'w');
ob_clean();

$headerRow = ['ID', 'FIRSTNAME', 'LASTNAME', 'EMAIL', 'AUSTRALIAN NUMBER', 'AGE BRACKET', 'GENDER', 'STATUS', 'WORK TYPE', 'LOCATION', 'STAY FROM', 'STAY TO', 'VISA TYPE', 'EXPIRE DATE', 'VEHICLE LICENSE', 'INDUSTRY', 'IMAGE', 'BIO', 'GOVERNMENT SUPPORT'];
fputcsv($handle, $headerRow);

