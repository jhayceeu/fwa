<?php
session_start();
if(!isset($_SESSION['admin']))
{
    header ('location:index.php');
}
include "../includes/config.php";
if(isset($_POST['refund']))
{
    $count=$_POST['refund_count'];
    $refund_id=$_POST['pass_token_id'];
    $sql="select * from tbl_refund where refund_id='".$refund_id."'";
    $res=$con->query($sql);
    if($row=$res->fetch_assoc())
    {
        $employer_id=$row['employer_id'];
    }

    $sql="update tbl_employer set extra_tokens=extra_tokens+".$count." where  id='".$employer_id."'";
    if($con->query($sql))
    {

     $sql1="update tbl_refund set status=1 where refund_id='".$refund_id."'"    ;
     $con->query($sql1);
$_SESSION['success']="Refunded successfully";
    }
    else
    {
        $_SESSION['error']="Error!!!! please try again later";
    }
}
include "includes/header.php";
?>



<link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
<style>
    .abc{
        background-color:bisque ;
         padding:12px;
         margin:2px;
         font-size:13px;
     }
     .def{
        padding:20px;
         background-color:gainsboro;
         text-align:right;
         font-weight:550;
         color:cornflowerblue;
         font-size:13px;
     }
</style>

<body class="materialdesign">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Header top area start-->
    <div class="wrapper-pro">
        <?php include "includes/side_bar.php"; ?>
        <div class="content-inner-all">
            <?php include "includes/top.php"; ?>
            <!-- Header top area end-->
            <!-- Breadcome start-->
            <div class="breadcome-area mg-b-30 small-dn">
                
            </div>
            <!-- Breadcome End-->
            <?php include_once('includes/sidebar_mobile.php'); ?>
            <!-- Breadcome start-->
            <div class="breadcome-area des-none">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="breadcome-heading">
                                            <form role="search" class="">
												<input type="text" placeholder="Search..." class="form-control">
												<a href=""><i class="fa fa-search"></i></a>
											</form>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="breadcome-menu">
                                            <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                            </li>
                                            <li><span class="bread-blod">Dashboard</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcome End-->
            <!-- welcome Project, sale area start-->
            
            <!-- welcome Project, sale area start-->
            <!-- stockprice, feed area start-->
            
            <!-- stockprice, feed area end-->
            <!-- Data table area Start-->
            <div class="admin-dashone-data-table-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline8-list shadow-reset">
                                <div class="sparkline8-hd">
                                    <div class="main-sparkline8-hd">
                                        <h1>Refund Requests</h1>
                                        <div class="sparkline8-outline-icon">
                                           
                                            <!-- <span class="sparkline8-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                            <span><i class="fa fa-wrench"></i></span>
                                            <span class="sparkline8-collapse-close"><i class="fa fa-times"></i></span> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                <?php
      if(isset($_SESSION['error'])){
                        echo "
                            <div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4><i class='icon fa fa-warning'></i></h4>
                            ".$_SESSION['error']."
                            </div>
                        ";
                        unset($_SESSION['error']);
                        }
                        if(isset($_SESSION['success'])){
                        echo "
                            <div class='alert alert-success alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4><i class='icon fa fa-check'></i> Success!</h4>
                            ".$_SESSION['success']."
                            
                            </div>
                        ";
                        unset($_SESSION['success']);
                        }
    
?>     
                                    <div class="" style="margin-top: 40px;padding-left: 10px;padding-right: 10px;">
                                      
                                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example1">
                                            <thead>
                                                <tr>
                                                    <!-- <th data-field="state" data-checkbox="true"></th> -->
                                                    <th data-field="id">Sl No</th>
                                                    <th >Company Name</th>
                                                    <th width="40%">Contact Name</th>
                                                    <th >Contact Number</th>
                                                    <th >Refund Reason</th>
                                                    <!-- <th >Validity</th> -->
                                                    <th >Action</th>
                                                  
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                           <?php
                                           $i=0;
                                           $sql="select * from tbl_refund order by request_date desc";
                                           $res=$con->query($sql);
                                           while($row=$res->fetch_assoc())
                                           {
                                                $i++;
                                           

                                           ?>
                                           <tr>
                                           <td><?php echo $i; ?></td>
                                           <td><?php echo $row['company_name']; ?></td>
                                           <td><?php echo $row['contact_name']; ?></td>
                                            <td><?php echo $row['contact_number']; ?></td>
                                            <td><?php echo $row['refund_reason']; ?></td>
                                            <!-- <td><?php //echo $row['expiry_days'] ." Months"; ?></td> -->
                                          
                                            <td>
                                            <?php if($row['status']=="0") { ?>
                                           <!-- <button type="button" onclick="approve_refund(<?php //echo $row['refund_id'] ?>)" class="btn btn-primary" style="background-color: #fff;color: #2e6da4;">Approve</button> -->
                                           <button type="button"  class="btn btn-primary" style="background-color: #fff;color: #2e6da4;" data-toggle="modal" data-target="#myModal" onclick="approve_refund(<?php echo $row['refund_id'] ?>)">Approve</button>

                                            <a onclick="reject_refund(<?php echo $row['refund_id'] ?>)" class="btn btn-danger" style="background-color: #fff;color: red;">Reject</button>  
                                            <?php } 
                                            
                                            ?>
                                             <?php if($row['status']=="1") { ?>
                                                <span class="label label-success">Approved</span>        
                                             <?php } ?>
                                             <?php if($row['status']=="2") { ?>
                                                <span class="label label-danger">Rejected</span>
                                            <?php } ?>
                                            </td>

                                           </tr>
                                             <?php
                                             }
                                             ?>   
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Data table area End-->
        </div>
    </div>


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form method="post">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Subscribe Token</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
            <input type="hidden" name="pass_token_id" id="pass_token_id"/>
        <label class="font-weight-bold" for="fullname">Enter Number of tokens</label>
                 <input type="text" name="refund_count" id="refund_count"/>
        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary" name="refund">Refund</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </form>
      
    </div>
  </div>


<!--Modal: modalAddCard-->
<?php include "includes/footer.php"; ?>

    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->

    <!-- <script src="js/jquery-3.3.1.min.js"></script> -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example1').DataTable({
            responsive: true
        });
        //$('#myModal').modal('show');
    });
  
  
    </script>
    <script>
function approve_refund(id)
{
    document.getElementById('pass_token_id').value=id;
}

</script>

  <script>
function reject_refund(id)
{

var val="RD";
  var r = confirm("Are you sure you want to reject this refund request?");
    if (r == true) {

  $.ajax({
            type : 'post',
            url : 'ajax/update_row.php', //Here you will fetch records 
            data :  {id:id,val:val}, //Pass $id
            success : function(data){
        //   $('#price_view').html(data);//Show fetched data from database
  if(data=="1")
  {
   
    window.location.replace('refund.php');
    //showSuccessToast_disable();

  }
  else
  {
    alert("Error");
  }
 // window.location.replace('our_works.php');
            }
        });

}
}

</script>
   
</body>
</html>